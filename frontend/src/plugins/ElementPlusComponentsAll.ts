import { App } from 'vue';

import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';

export default {
  install: (app: App, options?: any) => {
    app.use(ElementPlus);
  }
}