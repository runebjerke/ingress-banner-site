module.exports = {
  "devServer": {
    "disableHostCheck": true,
    "host": "0.0.0.0",
    //"public": "127.0.0.1",
    "port": 8444,
    "proxy": {
      "^/api": {
        "target": "http://localhost:8420/",
        "changeOrigin": true
      },
      "^/auth": {
        "target": "http://localhost:8420/",
        "changeOrigin": true
      },
      "^/static": {
        "target": "https://specops.quest/",
        "changeOrigin": true
      }
    }
  }
}