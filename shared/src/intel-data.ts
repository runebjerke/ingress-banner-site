/*
{
	"result": [
    "6c0f9d3155f84a54aacf4baf67f45637.1c", // GUID
    "Sørenga", // Title
    "Sørenga is a modern neighborhood in  Old Oslo, Norway. It is located east of Bjørvika, south of the street Bispegata  and west and north of the Alna River.", // Description
    "Sorvoja", // Author
    "E", // Author's team
    650000, // 
    831506, // 
    19, //
    2, // 
    [
      // hidden, guid, name, waypoint type, waypoint objective, [waypointdata]
      //   waypoint data
      //     type == 1 -> portaldata:    ["p", team, latE6, lngE6, <int>, <int>, <int>, imageUrl, portalName, [?], <bool>, <bool>, <bool>, timestamp]
      //     type == 2 -> fieldtripdata: ["f", latE6, lngE6]
      [false, "4bcae17bb063424b9cecff64990e2ac0.16", "The Urban Garden Project", 1, 1, ["p", "N", 59902259, 10758937, 1, 0, 0, "http://lh3.googleusercontent.com/PO5FafwJKSb9G8KEG3tfICKZt3aLb9Mf9s23W35m6oU3DBlmnfKG9atQnZFXk7HNUtj8qtQSjnjqrLvMi5Dg2kLQ8zo", "The Urban Garden Project", [], true, true, null, 1617396894847]],
      [false, "6eec4a8a27234487b3f992f5aa6812e7.16", "Bananas", 1, 1, ["p", "E", 59902497, 10759369, 1, 85, 1, "http://lh3.googleusercontent.com/jGuwWmBdbOnkqvswNQbWmgqrvG3GYDfHPFychD1Bv0ja2CNtCL2wz_HE70T0GkqabspeRZzZWp9wEG9MYm6wrf32W6MW", "Bananas", [], true, true, null, 1617533917646]],
      [false, "e42313c3d3b94a1490663806b8a25f3a.16", "Tunnel Air Tower", 1, 1, ["p", "N", 59902787, 10758808, 1, 0, 0, "http://lh3.googleusercontent.com/LmB9KVjvn-5qLF0fhKNyWNWVFLXRQY7eC0bXqHeg1y8sgNzUxVOoNf_fiYyJ0h7Wd3TXdwlBO-_slle_8i3Q71BAWEo", "Tunnel Air Tower", [], false, false, null, 1617536151118]],
      [false, "b840704df16fe37e63c7268b9eefe80c.1d", "Sorenga Byggetrinn 2", 2, 7, ["f", 59903869, 10756669]],
      [false, "0ed1ee2cd50e7d73281e81a299f6c62b.1d", "Sørenga 1", 2, 7, ["f", 59903400, 10756219]],
      [false, "f026a2257aec4c30652088ea1218bb4a.1d", "Sorenga Byggetrinn 1", 2, 7, ["f", 59903251, 10755009]]
	  ],
    "https://lh6.ggpht.com/uavMb-DqInJrbq2857KpN8SzzM0x9Ll8ighGMheWC9So6rCrqVU4R3fS3uicYuKHGFb-VbWuR_-hfhghM7gB"]
}
*/