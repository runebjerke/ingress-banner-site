import { Db } from 'mongodb';
import * as MongoDbQueue from 'mongodb-queue';
import * as Logger from 'bunyan';

export class Queue {

  queue: MongoDbQueue.Queue;
  pollInterval: number = 1000;
  alive: boolean = true;
  log: Logger;

  constructor(logger: Logger, db: Db, name: string, options?: MongoDbQueue.QueueOptions) {
    this.log = logger;
    this.queue = MongoDbQueue(db, name, options);
  }

  async add(payload: MongoDbQueue.Payload): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.queue.add(payload, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
  }

  startMessageLoop(cb: (message: MongoDbQueue.QueueMessage) => Promise<void>): void {
    const { log } = this;
    const delayedGetNext = () => {
      if (this.alive) setTimeout(() => getNext(), this.pollInterval);
    }
    const getNext = () => {
      if (!this.alive) return;
      this.queue.get(async (err, msg) => {
        if (err) {
          log.error("JOB: error = " + err);
          log.error(err.message, err.stack)
          delayedGetNext();
        } else if (msg == null) {
          //log.info("JOB: no message");
          delayedGetNext();
        } else {
          try {
            log.info("JOB: got message", msg);
            await cb(msg);
          } catch (err) {
            log.error("JOB FAILED", msg);
            log.error(err.message, err.stack)
          } finally {
            this.queue.ack(msg.ack, (e,id) => {});
          }
          getNext();
        }
      });
    }
    getNext();
  }
}
