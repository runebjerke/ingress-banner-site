const glyphsText = `Accept | Open
Abandon
Adapt
Advance
After
Again​/Repeat
All
Answer
Attack​/War
Avoid
Balance​/Perfection
Barrier
Before
Begin
Being​/Human
Body
Breathe​/Live
Capture
Change
Chaos
Civilization
Clear
Clear All
Collective​/Shapers
Complex
Conflict
Consequence
Contemplate
Courage
Create
Creativity​/Idea​/Thought
Danger
Data
Defend
Destination
Destiny
Destroy
Deteriorate
Death​/Die
Difficult
Discover
Distance​/Outside
Easy
End
Enlightened​/Enlightenment
Equal
Escape
Evolution​/Progress​/Success
Failure
Fear
Field
Follow
Forget
Future
Gain
Grow
Harm
Harmony​/Peace
Have
Help
Hide
I​/Me
Ignore
Imperfect
Improve
Impure
Individual​/Self
Inside​/Not
Intelligence
Interrupt
Journey
Key
Knowledge
Lead
Legacy
Less
Liberate
Lie
Link
Live Again​/Reincarnate
Lose
Message
Mind
More
Mystery
Nature
New
N'Zeer
Nemesis
Nourish
Now​/Present
Old
Open All
Osiris
Past
Path
Perspective
Portal
Potential
Presence
Pure
Pursue
Question
React
Rebel
Recharge​/Repair
Resistance​/Struggle
Retreat
Safety
Save
Search
See
Separate
Share
Shield
Simple
Soul
Stability​/Stay
Star
Strong
Technology
Them
Together
Truth
Unbounded
Us​/We
Use
Victory
Want
Weak
Worth
XM
You​/Your`;

const adverbGlyphsText = `Open
Perfect
#Before
Complex
Difficult
Easy
Equal
Future
Hidden
Imperfect
Impure
Inside​
Less
More
New
Present
Old
Past
Potential
Pure
Rebel
Separate
Simple
Stable
Strong
Unbounded
Weak`;


const verbGlyphsText = `
Abandon
Adapt
Advance
Answer
Attack​
Avoid
Begin
Breathe​/Live
Capture
Change
Clear
Clear All
Contemplate
Create
Defend
Destroy
Deteriorate
Die
Discover
Distance​
End
Escape
Evolve
Fear
Field
Follow
Forget
Gain
Grow
Harm
Have
Help
Hide
Ignore
Improve
Interrupt
Journey
Lead
Liberate
Lie
Link
Live Again​/Reincarnate
Lose
Nourish
Open All
Pursue
React
Rebel
Recharge​/Repair
Resist
Struggle
Retreat
Save
Search
See
Separate
Share
Stay
Use
Want`;

const nounGlyphsText = `Accept | Open
Advance
All
Answer
Attack​/War
Balance​/Perfection
Barrier
Being​/Human
Body
Change
Chaos
Civilization
Collective​/Shapers
Conflict
Consequence
Courage
Creativity​/Idea​/Thought
Danger
Data
Destination
Destiny
Death​
Distance​/Outside
End
Enlightened​/Enlightenment
Equal
Evolution​/Progress​/Success
Failure
Fear
Field
Future
#Gain
#Grow
Harm
Harmony​/Peace
Help
Me
Individual​/Self
Intelligence
Journey
Key
Knowledge
Legacy
Lie
Link
Message
Mind
Mystery
Nature
N'Zeer
Nemesis
Present
Osiris
Past
Path
Perspective
Portal
Potential
Presence
Question
Resistance​/Struggle
Safety
Shield
Soul
Stability​
Star
Technology
Them/They
Together
Truth
Us​/We
Victory
#Want
#Worth
XM
You`;

function fromList(s: string): string[] {
  return s.split(/[\r\n]+/).map(s => s.trim().toLowerCase()).filter(s => !/^#/.test(s)).map(s => s.replace(/[^a-z\/ ]/g, '').replace(/ /g, '-')).filter(s => s.length);;
}

function pickRandom(strs: string[]): string {
  return strs[Math.floor(Math.random() * strs.length)];  
}

function pickRandom2(strs: string[]): string {
  let g = pickRandom(strs);
  let gg = g.split(/\//g);
  if (gg.length > 0) g = pickRandom(gg);
  return g;
}

const glyphs = {
  all: fromList(glyphsText),
  nouns: fromList(nounGlyphsText),
  verbs: fromList(verbGlyphsText),
  adverbs: fromList(adverbGlyphsText)
};

function generateRandomNumber(): string {
  return ((110 + Math.floor(Math.random() * 90)) % 100).toString();
}

export function generateGlyphSequenceId(num: number = 5): string {
  let seq: string[] = [];
  for (let i = 0; 5 > i; i++) {
    let g = pickRandom(glyphs.all);
    let gg = g.split(/\//g);
    if (gg.length > 0) g = pickRandom(gg);
    seq.push(g);
  }
  seq.push(generateRandomNumber());
  let id = seq.join('-');
  return id;
}

export function generateGlyphSequence2Id(): string {
  let seq = [
    pickRandom2(glyphs.verbs),
    pickRandom2(glyphs.adverbs),
    pickRandom2(glyphs.nouns),
    pickRandom2(glyphs.verbs),
    pickRandom2(glyphs.nouns),
    generateRandomNumber()
  ];
  // let seq = [
  //   pickRandom2(glyphs.adverbs),
  //   pickRandom2(glyphs.nouns),
  //   pickRandom2(glyphs.verbs),
  //   pickRandom2(glyphs.adverbs),
  //   pickRandom2(glyphs.nouns),
  //   generateRandomNumber()
  // ];
  let id = seq.join('-');
  return id;
}

// console.log(generateGlyphSequence2Id());