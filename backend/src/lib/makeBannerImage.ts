import { promises as fsp } from 'fs';
import fs = require('fs');
import path = require('path');
import fetch from 'node-fetch';
import * as fileType from 'file-type';
import { IMissionInfoSummary } from '../../../shared/src/types';
import { createCanvas, loadImage, Image, Canvas } from 'canvas';
import { stripGoogleImageParams } from '../../../shared/src/googleImageParams';
const mkdirp = require('mkdirp');
const sanitize = require('sanitize-filename');
import * as Logger from 'bunyan';

function drawCircleImage(image: Image, width?: number, height?: number): Canvas {
  if (width == undefined) width = image.width;
  if (height == undefined) height = image.height;
  let canvas = createCanvas(width, height);
  let ctx = canvas.getContext("2d");
  ctx!.drawImage(image, 0, 0, width, height);
  ctx!.globalCompositeOperation = "destination-in";
  ctx!.fillStyle = "#000";
  ctx!.beginPath();
  ctx!.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI);
  ctx!.fill();
  ctx!.globalCompositeOperation = "source-over";
  return canvas;
}

interface IBannerOptions {
  rowSize: number;
  tileSize: number;
  circle: boolean;
  outline: number;
  padding: number;
  images?: Image[];
  imageManifest?: IMissionImageManifest;
  imageManifestDataPath?: string;
  limitWidthToRowSize?: boolean;
}

export async function writeCanvasToPngFile(canvas: Canvas, path: string): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    const out = fs.createWriteStream(path);
    const stream = canvas.createPNGStream();
    stream.pipe(out);
    out.on('finish', () => resolve());
    out.on('error', err => reject(err));
  })
}

export async function writeCanvasToJpegFile(canvas: Canvas, path: string, quality: number): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    const out = fs.createWriteStream(path);
    const stream = canvas.createJPEGStream({ quality: quality });
    stream.pipe(out);
    out.on('finish', () => resolve());
    out.on('error', err => reject(err));
  })
}

export async function drawBanner(missions: IMissionInfoSummary[], log: Logger, options: IBannerOptions) {

  const { tileSize, rowSize, padding, outline, circle } = options;

  let width = (options.limitWidthToRowSize === true ? rowSize : 6) * (tileSize + padding * 2);
  let rows = (missions.length % rowSize)
    ? Math.ceil(missions.length / rowSize)
    : (missions.length / rowSize);
  let height = rows * (tileSize + padding * 2)

  let canvas = createCanvas(width, height);

  let ctx = canvas.getContext("2d")!;
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, width, height);

  let manifest = options.imageManifest;

  let images: Image[] | undefined = undefined;
  if (options.images) images = options.images;
  else if (manifest == null) images = await preloadBannerImages(missions, log);

  for (let index = 0; missions.length > index; index++) {

    let dy = Math.floor(index / rowSize);
    let dx = index % rowSize;
    log.info(`drawing image: ${index+1} / ${missions.length} at (${dx},${dy}) ..`)

    let skip = false;
    let image: Image | null = null;
    if (images) {
      image = images[ missions.length - 1 - index ];
    } else if (manifest) {
      let x = manifest.images[ missions.length - 1 - index ];
      if (!x || x.path == null) skip = true;
      else {
        let buf = await fsp.readFile(path.join(options.imageManifestDataPath || ".", x.path!));
        image = await loadImage(buf);
      }
    }

    if (skip || image == null) continue;

    let im: Image | Canvas;

    let x = dx * (tileSize + padding * 2) + padding;
    let y = dy * (tileSize + padding * 2) + padding;

    if (circle === true) {
      im = drawCircleImage(image);
    } else {
      im = image;
    }

    ctx.drawImage(im, 0, 0, im.width, im.height, x, y, tileSize, tileSize);          

    if (outline > 0 && circle) {
      ctx.beginPath();
      ctx.lineWidth = outline;
      ctx.arc(x + tileSize/2, y + tileSize/2, tileSize/2, 0, Math.PI*2);
      ctx.strokeStyle = "#E5A347";
      ctx.stroke();
    }
  }

  return canvas;
}

export async function preloadBannerImages(missions: IMissionInfoSummary[], log: Logger): Promise<Image[]> {
  let imagesUrls = missions.map(m => m.image);
  let images: Image[] = [];
  if (imagesUrls) {
    for (let i = 0; imagesUrls.length > i; i++) {
      let imageUrl = imagesUrls[i];
      imageUrl = stripGoogleImageParams(imageUrl, "=s0");
      log.info("loading image: " + imageUrl);
      let image = await loadImage(imageUrl);
      images.push(image);
    }
  }
  return images;
}

export interface IMissionImage {
  url?: string;
  path?: string;
  size?: number;
  lastModifiedHeader?: string;
  etagHeader?: string;
}

export interface IMissionImageManifest {
  images: Array<IMissionImage | null>;
}

export async function downloadBannerImages(missions: IMissionInfoSummary[], log: Logger, imagesPath: string, manifest?: IMissionImageManifest): Promise<IMissionImageManifest> {
  
  await mkdirp(imagesPath);

  let manifestPath = path.join(imagesPath, "manifest.json");
  if (manifest == null) {
    try {
      log.info("trying to read manifest: " + manifestPath);
      manifest = JSON.parse((await fsp.readFile(manifestPath)).toString()) as IMissionImageManifest;
      log.info("got manifest!");
    } catch (err) {
      log.info("could not read manifest: " + err.message);
      manifest = undefined;
    }
  }

  if (manifest) {
    if (manifest.images.length != missions.length) {
      log.info("manifest length differs, discarding..");
      manifest = undefined;
    }    
  }

  if (manifest == null) {
    log.info("starting with fresh manifest..");
    manifest = {
      images: []
    }
    for (let i=0; missions.length>i; i++)
      manifest.images.push(null);
  }

  const save = async () => {
    await fsp.writeFile(manifestPath, JSON.stringify(manifest, null, '\t'));
  }

  await save();

  for (let i = 0; missions.length > i; i++) {
    
    log.info(`\n\n=== PROCEESING IMAGE ${i+1} OF ${missions.length} ===\n\n`);

    let mission = missions[i];

    // fetch the image
    let imageUrl = mission.image;
    imageUrl = stripGoogleImageParams(imageUrl, "=s0");
    log.info("downloading image: " + imageUrl);

    let im: IMissionImage = manifest.images[i] != null
      ? manifest.images[i]!
      : { };
    manifest.images[i] = im;

    let headers: any = {};
    let forceDl = false;
    let tryDl = true;
    let tmpPath: string | undefined;

    if (im.url && im.url != imageUrl) {
      log.info("changed url, force re-download");
      forceDl = true;
    } else if (im.url == null) {
      log.info("first time seen, force download");
      forceDl = true;
    } else if (im.url == imageUrl) {
      try {
        let imagePath = path.join(imagesPath, im.path!);
        let stat = await fsp.stat(imagePath);
      } catch (err) {
        log.info("local file is gone, clearing..");
        im.path = null!;
      }
      if (im.path) {
        log.info("same as before, checking if modified..");
        if (im.etagHeader != null)
          headers["If-None-Match"] = im.etagHeader;
        if (im.lastModifiedHeader != null)
          headers["If-Modified-Since"] = im.lastModifiedHeader;  
      } else {
        log.info("local copy gone, must re-download..");
        forceDl = true;
      }
    }
    
    if (!forceDl || tryDl) {

      try {
        log.info("downloading: " + imageUrl);
        let res = await fetch(imageUrl, {
          method: 'get',
          headers: headers
        })
        let buf = await res.buffer();

        if (res.status == 304) {

          log.info("not modified, skipping..");

        } else if (Math.floor(res.status / 100) == 2) {

          let ext: string = '';
          try {
            let ft = await fileType.fromBuffer(buf);
            if (ft && ft.ext && ft.ext.length) {
              ext = ft.ext;
            }
          } catch (err) {
            log.warn("error trying to resolve file type: " + err.message);
          }
          if (ext == "") ext = "png";
          let saveName = sanitize(mission.guid + '.' + ext);
          let savePath = path.join(imagesPath, saveName);

          log.info("saving image: " + savePath);
          await fsp.writeFile(savePath, buf);

          // SAVE!

          im.path = saveName;
          im.url = imageUrl;

          let etag = res.headers.get("etag"); // (res.headers["etag"]?.[0] || "").toString();
          log.info("ETAG -> ", etag);
          im.etagHeader = etag!;
          await save();

        } else {

          log.error("OOPS? NOT SURE WHAT TO DO:", res.status, res.statusText);
        }

      } catch (err) {

        log.info("ERROR: " + err);

      }
    }
  }

  return manifest;
}

export function findMissingImages(manifest: IMissionImageManifest): number[] {
  return manifest.images
    .map((im, i) => im == null ? i : null)
    .filter(im => im != null)
    .map(im => im!);
}

export async function loadBannerImageManifest(imagesPath: string): Promise<IMissionImageManifest> {
  let manifestPath = path.join(imagesPath, "manifest.json");
  return JSON.parse((await fsp.readFile(manifestPath)).toString()) as IMissionImageManifest;
}
