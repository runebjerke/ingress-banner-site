
import { IGeoJSONLineString } from '../../../shared/src/types'

export interface IRouteResponsePathInstruction {
  text: string;
  street_name: string;
  distance: number;
  time: number;
  interval: Array<number>, // [0,1]
  sign: number;
  exit_number: number;
  heading?: number;
  turn_angle?: number;
}

// https://graphhopper.stoplight.io/docs/graphhopper-directions-api/openapi.json/components/schemas/RouteResponsePath
export interface RouteResponsePath {
  distance: number;
  weight: number;
  time: number;
  transfers: number;
  points_encoded: boolean;
  bbox: Array<number>; // [ lng,lat,lng,lat ]
  points?: string | IGeoJSONLineString,
  instructions?: Array<IRouteResponsePathInstruction>;
  details?: any;
  legs?: Array<any>,
  ascend: number;
  descend: number;
  snapped_waypoints?: string | IGeoJSONLineString
  points_order?: Array<number>;
}

// https://graphhopper.stoplight.io/docs/graphhopper-directions-api/openapi.json/components/schemas/RouteResponse
export interface IRouteResponse {
  info?: {
    copyrights: Array<string>
    took: number;
  },
  hints?: {
    "visited_nodes.sum": number;
    "visited_nodes.average": number;
  },
  paths?: Array<RouteResponsePath>
}
