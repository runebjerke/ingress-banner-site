import * as url from 'url';
import { URL } from 'url';
import fetch from 'node-fetch';

export interface IGeoCodeJsonFeature {
  type: string; // Feature,
  properties?: {
    geocoding?: {
      place_id?: string | number; // 42700574,
      osm_type?: string; // node,
      osm_id?: string | number; // 3110596255,
      type?: string; // house,
      accuracy?: string | number; // 0,
      label?: string; // 1, Løvenbergvegen, Mogreina, Ullensaker, Akershus, 2054, Norway,
      name?: string; // null,
      housenumber?: string; // 1
      state?: string;
      city?: string;
      district?: string;
      locality?: string;
      street?: string; // Løvenbergvegen,
      postcode?: string; // 2054,
      county?: string; // Akershus,
      country?: string; // Norway,
      admin?: {
        level10?: string;
        level9?: string;
        level8?: string;
        level7?: string;
        level6?: string;
        level5?: string;
        level4?: string;
        level3?: string;
        level2?: string;
        level1?: string;
      }
    }
  },
  geometry?: {
    type: string; // Point,
    coordinates: number[];
  }
};

export interface IGeoCodeJson {
  type: string; // FeatureCollection,
  geocoding?: {
    version: string; // 0.1.0,
    attribution: string; // Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright,
    licence: string; //  ODbL,
    query: string; // 60.229917843587,11.16630979382
  },
  features?: IGeoCodeJsonFeature[]
}

export interface INominatimOptionsBase {
  addressDetails?: boolean;
  email?: string;
  nameDetails?: boolean;
  extraTags?: boolean;
  format?: 'xml' | 'json' | 'jsonv2' | 'geojson' | 'geocodejson';
  acceptLanguage?: string;
  debug?: boolean;
}

export interface INominatimLookupOptions extends INominatimOptionsBase {
  limit?: number;
}

export interface INominatimReverseLookupOptions extends INominatimOptionsBase {
  zoom?: number;
  polygon?: 'geojson' | 'kml' | 'svg' | 'text';
  polygonThreshold?: number;
}

export interface INominatimAddress {
  house_number?: string;
  road?: string;
  city?: string;
  town?: string;
  hamlet?: string;
  neighbourhood?: string;
  suburb?: string;
  village?: string;
  postcode?: string;
  state?: string;
  state_district?: string;
  county?: string;
  municipality?: string;
  country?: string;
  country_code?: string;
}

export interface INominatimResult {
  place_id: number;
  licence: string;
  osm_type: string; // "node"
  osm_id: number; 
  boundingbox: Array<string> // ["60.6462533","60.6463533","10.718818","10.718918"]
  lat: string;
  lon: string; 
  display_name: string;
  class: string; // "place",
  type: string; // "house"
  importance: number; 
  address?: INominatimAddress;
}

const defaultOptions: INominatimOptionsBase = {
  acceptLanguage: 'en-EN',
  format: 'jsonv2'
}

// https://nominatim.org/release-docs/latest/api/Search/

export async function nominatimReverseLookupGeoCodeJson(lat: number, lng: number, options: INominatimReverseLookupOptions = {}): Promise<IGeoCodeJson> {
  return nominatimReverseLookup(lat, lng, { ...options, format: 'geocodejson' })
}

//https://nominatim.org/release-docs/develop/api/Reverse/
export async function nominatimReverseLookup(lat: number, lng: number, options: INominatimReverseLookupOptions = {}): Promise<IGeoCodeJson> {

  options = { ...defaultOptions, ...options };

  let u = url.parse('https://nominatim.openstreetmap.org/reverse');
  let qs: any = {
    lat: lat,
    lon: lng,
    addressdetails: options.addressDetails ? '1' : '0',
    namedetails: options.nameDetails ? '1' : '0',
    extratags: options.extraTags ? '1' : '0'
  };
  if (options.format) qs.format = options.format;
  if (options.acceptLanguage !== undefined) qs["accept-language"] = options.acceptLanguage;
  if (options.email !== undefined) qs.email = options.email;
  if (options.zoom !== undefined) qs.limit = options.zoom;

  u.query = qs;

  let lookupUrl = url.format(u);

  //console.dir(lookupUrl);

  return await ((await fetch(lookupUrl)).json());
}

export async function nominatimLookupLatLng(lat: number, lng: number, options: INominatimLookupOptions = {}): Promise<INominatimResult[]> {
  return nominatimLookup(`${lat},${lng}`, options);
}

export async function nominatimLookup(query: string, options: INominatimLookupOptions = {}): Promise<INominatimResult[]> {
//https://nominatim.openstreetmap.org/search?q=60.6453806,10.7187706&format=json&addressdetails=1&limit=1&email=contact@here.com  

  options = { ...defaultOptions, ...options };

  let u = url.parse('https://nominatim.openstreetmap.org');
  let qs: any = {
    q: query,
    addressdetails: options.addressDetails ? '1' : '0',
    extratags: options.extraTags ? '1' : '0',
    namedetails: options.nameDetails ? '1' : '0'
  };
  if (options.format) qs.format = options.format;
  if (options.acceptLanguage !== undefined) qs["accept-language"] = options.acceptLanguage;
  if (options.email !== undefined) qs.email = options.email;
  if (options.limit !== undefined) qs.limit = options.limit;

  u.query = qs;

  let lookupUrl = url.format(u);

  //console.dir(lookupUrl);

  return <INominatimResult[]><unknown>((await fetch(lookupUrl)).json());
}

// https://wiki.openstreetmap.org/wiki/Tag:boundary=administrative#10_admin_level_values_for_specific_countries
// https://wiki.openstreetmap.org/wiki/Tag:place%3Dsuburb
// 