import { MongoClient } from 'mongodb';
import { RateLimiterMongo } from 'rate-limiter-flexible';
import { Request, Response, NextFunction } from 'express';

export type ExpressMiddlewareFunction = (req: Request, res: Response, next: NextFunction) => void;

export type GetUserIdFromRequestFunction = (req: Request) => string;

export class RateLimiterFactory {

  client: MongoClient;
  dbName: string;
  collectionNamePrefix: string;
  getUserIdFromRequest: GetUserIdFromRequestFunction;

  constructor(client: MongoClient, getUserIdFromRequest: GetUserIdFromRequestFunction, dbName: string, collectionNamePrefix: string) {
    this.client = client;
    this.dbName = dbName;
    this.collectionNamePrefix = collectionNamePrefix;
    this.getUserIdFromRequest = getUserIdFromRequest;
  }

  create(id: string, points: number, duration: number): ExpressMiddlewareFunction {

    const opts = {
      storeClient: this.client,
      dbName: this.dbName,
      tableName: 'rateLimiting_' + id,
      points,
      duration
    };
  
    const rateLimiterMongo = new RateLimiterMongo(opts);
  
    const fun = (req: Request, res: Response, next: NextFunction): void => {
      let userId = this.getUserIdFromRequest(req);
      let id: string = userId != null && userId.trim().length
        ? 'user:' + userId
        : 'ip:' + req.ip;
      rateLimiterMongo.consume(id, 1).then((result: any) => {
        (<any>req).rateLimiting = {
          quota: opts.points,
          result
        };
        res.set({
          "X-RateLimit-Limit": opts.points,
          "X-RateLimit-Remaining": result.remainingPoints,
          "X-RateLimit-Reset": new Date(Date.now() + result.msBeforeNext)      
        });
        next();
      }).catch((result: any) => {
        res.status(429).set({
          "Retry-After": result.msBeforeNext / 1000,
          "RateLimit-Limit": opts.points,
          "RateLimit-Remaining": result.remainingPoints,
          "RateLimit-Reset": Math.round(result.msBeforeNext / 1000) //new Date(Date.now() + result.msBeforeNext)
        }).json({ error: "Rate limited" });
      })
    };
  
    return fun;
  }

}


