import * as path from 'path';
import * as Logger from 'bunyan';
import { createLogger } from 'bunyan';
import * as RotatingFileStream from 'bunyan-rotating-file-stream';
const BunyanDebugStream = require('bunyan-debug-stream');
import * as mkdirp from 'mkdirp';
import * as strftime from 'strftime';

import { IConfig } from "./config";

export interface ILogger {
  error(...args: any): void;
  warn(...args: any): void;
  info(...args: any): void;
  debug(...args: any): void;
}

export async function initializeLogger(config: IConfig, logBaseName: string, logsPath: string): Promise<Logger> {
  
  await mkdirp(logsPath);

  const consoleLogStream = BunyanDebugStream({
    out: process.stdout,
    showPid: false,
    showDate: (t: Date, e: any) => strftime("%Y-%m-%d %H:%M:%S", t),
    processName: false,
    //showMetadata: false, // annoying because 'component' is present in everything - fixed by adding a prefixer! :D
    showLoggerName: false,
    prefixers: {
      'component': (x: string) => x
    }
  })

  const fileLogStream = new RotatingFileStream({
      path: path.join(logsPath, `${logBaseName}-%Y%m%d`),
      period: '1d',
      totalFiles: 30,
      //rotateExisting: true
  })

  const logger = createLogger({
      name: 'app',
      streams: [
          {
              level: 'debug',
              type: 'raw', 
              stream: consoleLogStream
          },
          {
              stream: fileLogStream
          }
      ]
  })

  logger.info("logger initialized");

  return logger;
}
