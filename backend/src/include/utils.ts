import * as escapeRegexp from 'escape-string-regexp';
import { ObjectID } from "mongodb";

import { MissionsPlugin } from '../../../iitc-plugin/src/missionsPluginTypes';

import {
  IMissionInfoDetailed,
  IMissionInfoSummary,
  IMissionWaypointEx,
  findFirstWaypoint,
  ISearchResults,
  IBanner,
  ICompletionReview,
} from '../../../shared/src/types';

export function makeRegexWildcard(text: string): RegExp {

  text = text.trim();

  let prefix = "^";
  let suffix = "$";

  // if no * present, assume substring
  if (text.indexOf('*') < 0) {
    prefix = '';
    suffix = '';
  }

  let expr = escapeRegexp(text.replace(/\*/g, '!_star_!')).replace(/!_star_!/g, '.*?');

  expr = expr.replace(/( +)/g, ' +')

  console.log(expr);

  return new RegExp(prefix + expr + suffix, "i");  
}

export function convertMissionPluginSummary(summary: MissionsPlugin.IMissionSummary, portalGuid?: string): IMissionInfoSummary {
  return <IMissionInfoSummary> {
    guid: summary.guid,
    image: summary.image,
    time: parseInt(<string>summary.medianCompletionTimeMs),
    portalGuid: null,
    rating: parseInt(<string>summary.ratingE6) / 1E4,
    title: summary.title
  }
}

export interface ILatLngE6 {
  latE6: number;
  lngE6: number;
}

export function convertMissionsPluginWaypoint(waypoint: MissionsPlugin.IMissionWaypoint): IMissionWaypointEx {

  const out: IMissionWaypointEx = {
    guid: waypoint.guid,
    hidden: waypoint.hidden,
    objective: waypoint.objectiveNum,
    title: waypoint.title,
    type: waypoint.typeNum,
    point: null!
  }

  if (waypoint.portal) {
    out.point = {
      latE6: waypoint.portal.latE6,
      lngE6: waypoint.portal.lngE6,
      lat: waypoint.portal.latE6 / 1E6,
      lng: waypoint.portal.lngE6 / 1E6
    }
  }

  return out;
}

export function convertMissionPluginDetails(mission: MissionsPlugin.IMissionDetails, portalGuid?: string): IMissionInfoDetailed {

  let m: IMissionInfoDetailed = {
    author: {
      nickname: mission.authorNickname,
      team: mission.authorTeam
    },
    description: mission.description,
    guid: mission.guid,
    image: mission.image,
    time: parseInt(<string>mission.medianCompletionTimeMs),
    numCompleted: parseInt(<string>mission.numUniqueCompletedPlayers),
    rating: parseInt(<string>mission.ratingE6) / 1E4,
    title: mission.title,
    type: mission.typeNum,
    waypoints: mission.waypoints.map(convertMissionsPluginWaypoint),
    numWaypoints: mission.waypoints.length,
    portalGuid: null!
  }

  m.portalGuid = portalGuid == undefined ? findFirstWaypoint(m)?.guid : portalGuid;

  return m;
}

export function ensureString(text: any, trim: boolean = true): string {
  let s = (text || "").toString();
  if (trim) s = s.trim();
  return s;
}

export function ensureBoolean(value: any): boolean | null {
  if (value == null) return null;
  else if (typeof value == 'boolean') return value;
  else return null;
}

export function ensureNumber(value: any): number | null {
  if (value == null) return null;
  else if (typeof value == 'number') return value;
  else if (typeof value == 'string') return parseInt(value);
  else return null;
}

export function wrapSearchResults<T>(list: T[]): ISearchResults<T> {
  return {
    results: list,
    count: list.length,
    numPages: 1,
    page: 1,
    pageSize: list.length
  }
}

export function importReview(review: ICompletionReview): ICompletionReview {
  let rev: ICompletionReview = {
    comment: ensureString(review.comment),
    enjoyed: ensureBoolean(review.enjoyed)!,
    twentyFourSeven: ensureBoolean(review.twentyFourSeven)!,
    artworkRating: ensureNumber(review.artworkRating)!,
    difficultyRating: ensureNumber(review.difficultyRating)!,
    routeRating: ensureNumber(review.routeRating)!,
    passphraseRating: ensureNumber(review.passphraseRating)!,
    completedByFoot: ensureBoolean(review.completedByFoot)!,
    completedByBike: ensureBoolean(review.completedByBike)!,
    completedByCar: ensureBoolean(review.completedByCar)!,
    suitableTransportation: {
      foot: ensureBoolean(review.suitableTransportation?.foot)!,
      bike: ensureBoolean(review.suitableTransportation?.bike)!,
      car: ensureBoolean(review.suitableTransportation?.car)!,
      wheelchair: ensureBoolean(review.suitableTransportation?.wheelchair)!
    }
  };
  return rev;
}

export function importForeignWaypointObject(w0: IMissionWaypointEx): IMissionWaypointEx {

  let w: IMissionWaypointEx = {
    hidden: w0.hidden,
    objective: w0.objective,
    title: w0.title,
    type: w0.type,
    guid: w0.guid,
    point: null!
  }

  // HAVE NEW INPUT FORMAT
  if (w0.point) {
    if (w0.point.latE6 != null) {
      w.point = {
        backfilled: w0.point.backfilled,
        latE6: w0.point.latE6,
        lngE6: w0.point.lngE6,
        lat: w0.point.latE6! / 1E6,
        lng: w0.point.lngE6! / 1E6,
      }
    } else {
      w.point = {
        backfilled: w0.point.backfilled,
        lat: w0.point.lat,
        lng: w0.point.lng,
        latE6: Math.round(w0.point.lat * 1E6),
        lngE6: Math.round(w0.point.lng * 1E6)
      }
    }
  }
  // CONVERT OLD?
  // tslint:disable-next-line
  else if (w0.lat != null && w.lng != null) {
    w.point = {
      // tslint:disable-next-line
      lat: w0.lat!,
      // tslint:disable-next-line
      lng: w0.lng!,
      latE6: Math.round(w0.lat! * 1E6),
      lngE6: Math.round(w0.lng! * 1E6)
    }
  }

  if (w.point) {
    // w.point.geoJSON = {
    //   type: 'Point',
    //   coordinates: [ w.point.lat, w.point.lng ]
    // }
    w.point.geojson = {
      type: 'Point',
      coordinates: [ w.point.lng, w.point.lat ]
    }
  }

  return w;
}

export function stripBannerData(banner: IBanner): IBanner {
  delete banner.missionGuids;
  delete banner.createdAt;
  delete banner.lastModifiedAt;
  delete banner.guid;
  delete banner.addressHierarchy;
  delete banner.addressLookups;
  delete banner.routes;
  delete banner.reviews;
  banner.missions?.forEach(m => {
    let M: any = m;
    delete M.created;
    delete M.lastModified;
    delete M.guid;
    delete M._id;
    delete m.locationPoint;
    delete m.locationBounds;
    delete M.numWaypoints;
    delete M.numCompleted;
    delete m.portalGuid;
    delete M.rating;
    delete m.summaryCreated;
    delete m.summaryUpdated;
    delete m.detailsModified;
    delete m.detailsCreated;
    delete M.time;
    m.waypoints?.forEach(w => {
      delete w.guid;
    })
  })
  return banner;
}

export function resolveObjectID(id: string | ObjectID | undefined): ObjectID {
  if (typeof id == 'object' && id instanceof ObjectID) {
    return id;
  } else if (id == null) {
    return new ObjectID();
  } else {
    return new ObjectID(id);
  }
}
