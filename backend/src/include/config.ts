export interface IConfig {
  paths: {
    data: string;
    temp: string;
    logs: string;
    www: string;
  },
  jobs: {
    enabled: boolean;
  },
  http: {
    port: number;
    publicUrl: string;
    sessionSecret: string;
  },
  db: {
    uri: string;
    name: string;
    collections: {
      missions: string;
      banners: string;
      users: string;
      sessions: string;
      rateLimitingPrefix: string;
      queuePrefix: string;
    }
  }
  apiKeys: {
    telegram: string;
    graphhopper: string;
    googleGeocoding: string;
    googleAuth: {
      clientId: string;
      clientSecret: string;
    }
  },
  telegram: {
    announceChannelId: string | number | null;
  }
}
