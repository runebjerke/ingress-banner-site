import { IRouteResponse } from "../lib/graphhopper";
import { ILatLng } from "../../../shared/src/types";
import { IConfig } from "./config";

const url = require('url');

// http://project-osrm.org/docs/v5.5.1/api/#route-service

// async function queryOSRM(latLngs: ILatLng[], profile: "foot" | "bike" | "car" = "foot"): Promise<IRouteResponse> {
//   // /route/v1/{profile}/{coordinates}?alternatives={true|false}&steps={true|false}&geometries={polyline|polyline6|geojson}&overview={full|simplified|false}&annotations={true|false}
//   let coordinates = latLngs.map(w => `${w.lng},${w.lat}`);
//   let u = url.parse(`http://router.project-osrm.org/route/v1/${profile}/${coordinates}`);
//   u.query = <any>{
//     geometries: 'geojson',
//     continue: 'true',

//     alternatives: 'false'
//   }
//   let routeUrl = url.format(u);

// }

export async function queryGraphhopperRoute(config: IConfig, latLngs: ILatLng[], vehicle: "foot" | "bike" | "car" = "foot"): Promise<IRouteResponse> {
  let u = url.parse("https://graphhopper.com/api/1/route");
  u.query = <any>{
    vehicle: vehicle,
    locale: "en",
    instructions: "true",
    calc_points: "true",
    points_encoded: "false",
    optimize: "false",
    key: config.apiKeys.graphhopper,
    point: latLngs.map(w => `${w.lat},${w.lng}`)
  }    
  let routeUrl = url.format(u);
  let res = await fetch(routeUrl)
  let data = await res.json() as IRouteResponse;
  return data;
}
