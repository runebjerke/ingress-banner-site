// for mongodb map-reduce 
declare function emit(k: string, v: any): void;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  IMPORT
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////
////////  node.js
////////

import url = require('url');
import http = require('http');
import path = require('path');
import fs = require('fs');
import fsp = require('fs/promises');

////////
////////  npm
////////

import escapeHtml = require('escape-html');
import { Telegraf } from 'telegraf';
import * as Passport from 'passport';
import { Strategy as CustomStrategy } from 'passport-custom';
import { BasicStrategy } from 'passport-http';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import * as CookieParser from 'cookie-parser';
// const session = require('express-session');
// const MongoStore = require('connect-mongo');
import Express = require('express');
const haversine = require('haversine-distance');
import * as mkdirp from 'mkdirp';
import { v4 as uuid } from 'uuid';
import CORS = require('cors');
import * as compression from 'compression';
import { MongoClient, Collection, Db, ObjectID, MongoError } from 'mongodb';
import * as Logger from 'bunyan';
const uniqueSlug = require('unique-slug');
const slug = require('limax');

////////
////////  ours
////////

import { IConfig } from './include/config';

import { generateGlyphSequence2Id, } from './lib/glyphSequenceIdGenerator';

import { queryGraphhopperRoute } from './include/routeLookup';

import { Queue } from './lib/queue';

import { MissionsPlugin } from '../../iitc-plugin/src/missionsPluginTypes';

import { IUser } from './include/user'; 

import { nominatimReverseLookupGeoCodeJson } from './lib/nominatim';

import {
  convertMissionPluginDetails,
  convertMissionPluginSummary,
  ensureString,
  makeRegexWildcard,
  resolveObjectID,
  wrapSearchResults,
  importForeignWaypointObject,
  stripBannerData,
  importReview,
  ensureNumber
} from './include/utils';

import {
  formatAddress,
  formatDistance,
  formatTime,
  parseBool
} from '../../frontend/src/lib/utils';

import {
  downloadBannerImages,
  drawBanner,
  writeCanvasToJpegFile,
} from './lib/makeBannerImage';

import {
  IBanner,
  IMissionInfoDetailed,
  IMissionInfoSummary,
  IMissionWaypointEx,
  ISearchResults,
  filterValidWaypoints,
  findFirstWaypoint,
  findLastWaypoint,
  initializeWaypointsObjectivesMap,
  WaypointObjective,
  countUniques,
  IGeoAdminLevel,
  ICountryInfo,
  MissionInfo,
  DefaultBannerUserData,
  ICompletionReview,
  ILatLng
} from '../../shared/src/types';

import {
  ExpressMiddlewareFunction,
  RateLimiterFactory
} from './include/rateLimiterFactory';

import { initializeLogger } from './include/logging';
import escapeStringRegexp = require('escape-string-regexp');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  LOAD CONFIG
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// load config
const config = JSON.parse(fs.readFileSync('config.json').toString()) as IConfig;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  GLOBALS & CONSTANTS
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////
////////  INTERFACES
////////

interface ISessionFeatures {
  logBaseName: string;
  http: boolean;
  httpPort: number;  
  announceQueue: boolean;
  generateBannerImageQueue: boolean;
  updateBannerQueue: boolean;
  updateMissionQueue: boolean;
}

////////
////////  CONSTANTS
////////

const ROLE_CONTRIBUTOR = "contributor";
const ROLE_ADMIN = "admin";

const rootPath = path.resolve(__dirname, '..');

const dataPath = config.paths?.data || "data";
const imagesPath = path.join(dataPath, 'images');
const tempPath = config.paths?.data || 'temp'; // FIXME: typo, should have been .temp, must move files on disk also

const apiPrefix = "/api/";
const staticPrefix = "/static/";
const imageFilenameTemplate = 'banner-{id}-{size}.{ext}';
const defaultPageSize = 50;

const googleStrategyId = "google";
const apikeyStrategyId = "apikey";
const basicApikeyStrategyId = "apikeyBasic";
const logsPath = config.paths?.logs || "logs";

const DefaultMissionsSort: any = { title: 1 }

const sessionModes: {[name: string]: ISessionFeatures} = {
  'ALL': {
    http: true,
    httpPort: 0,
    logBaseName: 'all_',
    announceQueue: true,
    generateBannerImageQueue: true,
    updateBannerQueue: true,
    updateMissionQueue: true
  },
  'web': {
    http: true,
    httpPort: 0,
    logBaseName: 'all_',
    announceQueue: false,
    generateBannerImageQueue: false,
    updateBannerQueue: false,
    updateMissionQueue: false
  },
  'main': {
    http: true,
    httpPort: 0,
    logBaseName: 'main_',
    announceQueue: true,
    generateBannerImageQueue: false,
    updateBannerQueue: true,
    updateMissionQueue: true
  },
  'jobs': {
    http: false,
    httpPort: 0,
    logBaseName: 'jobs_',
    announceQueue: true,
    generateBannerImageQueue: false,
    updateBannerQueue: true,
    updateMissionQueue: true
  },
  'api1': {
    http: true,
    httpPort: 0,
    logBaseName: 'api1_',
    announceQueue: false,
    generateBannerImageQueue: false,
    updateBannerQueue: false,
    updateMissionQueue: false
  },
  'api2': {
    http: true,
    httpPort: +1,
    logBaseName: 'api2_',
    announceQueue: false,
    generateBannerImageQueue: false,
    updateBannerQueue: false,
    updateMissionQueue: false
  },
  'api3': {
    http: true,
    httpPort: +2,
    logBaseName: 'api3_',
    announceQueue: false,
    generateBannerImageQueue: false,
    updateBannerQueue: false,
    updateMissionQueue: false
  },
  'api4': {
    http: true,
    httpPort: +3,
    logBaseName: 'api4_',
    announceQueue: false,
    generateBannerImageQueue: false,
    updateBannerQueue: false,
    updateMissionQueue: false
  },
  'none': {
    http: false,
    httpPort: 0,
    logBaseName: 'none_',
    announceQueue: false,
    generateBannerImageQueue: false,
    updateBannerQueue: false,
    updateMissionQueue: false,
  },
  'mosaic': {
    http: false,
    httpPort: 0,
    logBaseName: 'mosaic_',
    announceQueue: false,
    generateBannerImageQueue: true,
    updateBannerQueue: false,
    updateMissionQueue: false
  }
}

////////
////////  GLOBALS
////////

// express
let app: Express.Application;
let rateLimiterFactory: RateLimiterFactory;

// db
let mongoClient: MongoClient;
let mongoDb: Db;
let usersCollection: Collection;
let missionsCollection: Collection;
let bannersCollection: Collection;
let fieldtripsCollection: Collection;

// queues
let generateBannerImageQueue: Queue;
let updateBannerQueue: Queue;
let updateMissionQueue: Queue;
let announceQueue: Queue;

// log
let log: Logger = null! as Logger;
let loggers = {
  api: <unknown>null as Logger,
  apikeyAuth: <unknown>null as Logger,
  googleAuth: <unknown>null as Logger,
  auth: <unknown>null as Logger
}

// session features
let sessionFeatures: ISessionFeatures = sessionModes.none;

function makeBannerSlug(banner: IBanner): string {
  let s = slug(banner.title);
  if (s == "") s = "banner";
  return s + '-' + uniqueSlug(banner.title + banner.guid);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  INITIALIZE: DB
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function initializeDb() {

  log.info("initializing db..");
  let uri = config.db.uri;
  mongoClient = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  await mongoClient.connect();
  mongoDb = mongoClient.db(config.db.name);
  missionsCollection = mongoDb.collection(config.db.collections.missions);
  bannersCollection = mongoDb.collection(config.db.collections.banners);
  usersCollection = mongoDb.collection(config.db.collections.users);
  fieldtripsCollection = mongoDb.collection('fieldtrips');
  rateLimiterFactory = new RateLimiterFactory(
    mongoClient,
    req => (<any>req.user)?._id,
    config.db.name,
    config.db.collections.rateLimitingPrefix
  );
  log.info("db ready!");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  INITIALIZE: QUEUES
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



async function initializeQueuesAndProcessors(): Promise<void> {
  return new Promise<void>(async (resolve, reject) => {

    log.info("initializing queues..");

    generateBannerImageQueue = new Queue(log, mongoDb, config.db.collections.queuePrefix + 'banner-image', {
      visibility: 60 * 15 // for really huge ones
    });

    updateBannerQueue = new Queue(log, mongoDb, config.db.collections.queuePrefix + 'banner-metrics', {
      visibility: 15
    });

    updateMissionQueue = new Queue(log, mongoDb, config.db.collections.queuePrefix + 'mission-updates', {
      visibility: 15
    });

    announceQueue = new Queue(log, mongoDb, config.db.collections.queuePrefix + 'banner-announce', {
      visibility: 15
    });

    log.info("starting queues..");

    let queueLog = log.child({ component: 'queues' });

    if (sessionFeatures.generateBannerImageQueue) {
      queueLog.info("starting banner generation processor..");
      generateBannerImageQueue.startMessageLoop(async (msg) => {
        queueLog.info({ msg }, "---PROCESS BANNER IMAGE JOB--- ");
        let [ what, id ] = (msg.payload as string).split(':');
        let banner = await bannersCollection.findOne({ _id: new ObjectID(id) }) as IBanner;
        if (banner) {
          try {

            await makeBannerImages(banner, imageFilenameTemplate);

            await bannersCollection.updateOne(
              { _id: new ObjectID(id) },
              {
                $set: {
                  'images': banner.images
                }
              }
            );

            if (what == "add") {
              queueLog.info("adding to announce queue..");
              announceQueue.add("all:" + banner.guid);
            }

          } catch (err) {
            queueLog.error("error while generating banner image for %s: %s", banner.guid, err.message);
          }
        }
        queueLog.info({ msg }, "---PROCESSED BANNER IMAGE JOB---");
      })
    }

    if (sessionFeatures.updateBannerQueue) {
      queueLog.info("starting banner updater processor..");
      updateBannerQueue.startMessageLoop(async (msg) => {
        queueLog.info({ msg }, "---PROCESS BANNER UPDATE JOB--- ");
        let [ what, id ] = (msg.payload as string).split(':');
        let banner = await bannersCollection.findOne({ _id: new ObjectID(id) }) as IBanner;
        if (banner) {
          if (what == "full") {
            await preprocessBanner(banner);
          } else if (what == "metrics") {
            await updateBannerMetrics(banner);
          } else if (what == "route") {
            await updateBannerRoute(banner);
            await updateBannerMetrics(banner);
          }
          //delete banner.comments;
          banner.lastModifiedAt = Date.now();
          await bannersCollection!.updateOne({ guid: banner.guid }, { $set: banner }); // overwrites old? (race condition possible?)
        }
        queueLog.info({ msg }, "---PROCESSED BANNER UPDATE JOB---");
      })
    }

    if (sessionFeatures.updateMissionQueue) {

      queueLog.info("starting missions updater processor..");
      updateMissionQueue.startMessageLoop(async (msg) => {        
        queueLog.info({ msg }, "---PROCESS MISSION UPDATE JOB--- ");
        let [ what, id ] = (msg.payload as string).split(':');
        let mission = await missionsCollection.findOne({ guid: id }) as IMissionInfoDetailed;
        if (mission) {
          if (what == "full") {
            await updateMissionDataFormat(mission);
            await updateMissionMetrics(mission);
          } else if (what == "metrics") {
            await updateMissionMetrics(mission);
          }
          //mission.lastModifiedAt = Date.now();
          await missionsCollection!.updateOne({ guid: mission.guid }, { $set: mission });
        }
        queueLog.info({ msg }, "---PROCESSED MISSION UPDATE JOB---");
      })
    }

    if (sessionFeatures.announceQueue) {


      await initializeTelegramBot();
      queueLog.info("starting banner announce processor..");

      announceQueue.startMessageLoop(async (msg) => {
        let [ what, guid ] = (msg.payload as string).split(':');
        queueLog.info({ what, guid }, "processing banner announce job");
        let banner = await bannersCollection.findOne({ guid: guid }) as IBanner;
        if (banner) {
          // ignore {what}
          try {
            await announceToTelegram(banner);
          } catch (err) {
            queueLog.error({ err, guid }, "error announcing to telegram: %s", err.message)
          }
        }
        queueLog.info({ what, guid }, "processed banner announce job");
      })
    }


    resolve();
  })
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  DATABASE: QUERY
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function queryBanners(query: any, sort: any, projection: any, req: Express.Request, allowOverridePageSize: boolean = false): Promise<ISearchResults<IBanner>> {

  query.deleted = { $ne: true };

  let page = parseInt(<string>req.query.page);
  if (isNaN(page)) page = 1;

  let pageSize = defaultPageSize;
  let ps = parseInt(<string>req.query.pageSize);
  if (!isNaN(ps) && ps >= 0) {
    pageSize = allowOverridePageSize ? ps : Math.min(defaultPageSize, ps)
  }

  //console.dir({ query, sort, projection }, { depth: null });

  let start = Math.max(0, pageSize * (page-1));

  let cursor = await bannersCollection.find(query);

  if (projection != null) cursor = cursor.project(projection);

  if (sort != null) cursor = cursor.sort(sort);

  let count = await cursor.count();

  if (pageSize != 0) {
    cursor = cursor.skip(start).limit(pageSize);
  }

  let banners = await cursor.toArray();

  return <ISearchResults<IBanner>>{
    count,
    page: page,
    numPages: Math.ceil(count / pageSize),
    pageSize: pageSize,
    results: banners.map(banner => personalizeBannerUserData(banner, req))
  };
}

async function queryMissions(query: any, sort: any, options: { req?: Express.Request, includeDetails?: boolean, page?: number, pageSize?: number }): Promise<ISearchResults<IMissionInfoSummary | IMissionInfoDetailed>> {

  let { page, pageSize, includeDetails } = options;

  if (typeof page != 'number') page = 0;
  if (typeof pageSize != 'number') pageSize = 0; 

  let start = Math.max(0, pageSize * (page-1));

  let doSort = sort != null && Object.keys(sort).length;

  let cursor = await missionsCollection.find(query);
  if (doSort) cursor = cursor.sort(sort);
  let count = await cursor.count();

  if (pageSize != 0) {
    cursor = cursor.skip(start).limit(pageSize);
  }

  let missions: IMissionInfoDetailed[] = await cursor.toArray();

  missions.forEach(m => {
    if (m.waypoints) m.numWaypoints = m.waypoints.length;
  })

  if (includeDetails !== true)
    missions = missions.map(m => { delete (<any>m).waypoints; return m; });

  return <ISearchResults<IMissionInfoSummary | IMissionInfoDetailed>>{
    count,
    page: page,
    numPages: pageSize == 0 ? 1 : Math.ceil(count / pageSize),
    pageSize: pageSize == 0 ? count : pageSize,
    results: missions.map(m => personalizeMissionUserData(m, options.req))
  };
}

async function queryUsers(userIds?: string[], forExport: boolean = true): Promise<{[id: string]: IUser}> {
  
  if (!userIds) return {};
  
  let map: {[id: string]: IUser} = {};

  let users = await usersCollection.find({
    _id: { $in: userIds.map(u => new ObjectID(u)) }
  }).toArray() as IUser[];

  if (forExport) {
    users = users.map(u => {
      return <IUser>{
        _id: u._id,
        agentName: u.agentName,
        agentFaction: u.agentFaction
      }
    })
  }

  users.forEach(u => {
    u._id = resolveObjectID(u._id).toHexString()
    map[u._id] = u;
  })

  return map;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  DATABASE: SUBMIT MISSIONS
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function backfillFieldtripWaypoints(mission: IMissionInfoDetailed): Promise<boolean> {
  // backfill fieldtrip waypoint lat lng
  let all = true;
  if (mission.waypoints && mission.waypoints instanceof Array) {
    for (let i = 0; mission.waypoints.length > i; i++) {
      let w = mission.waypoints[i];
      if (w.guid && w.objective == 7 && w.point == null && w.title != "Unavailable") {
        //log.info(`trying to backfill fieldtrip waypoint location: ${w.guid}: ${w.title}`)
        let f = await fieldtripsCollection.findOne({ guid: w.guid! }) as { lat?: number, lng?: number };
        if (f && f.lat != null && f.lng != null) {
          w.point = {
            lat: f.lat,
            lng: f.lng,
            latE6: f.lat * 1E6,
            lngE6: f.lng * 1E6,
            geojson: {
              type: 'Point',
              coordinates: [ f.lng, f.lat ]
            },
            backfilled: true
          }
          log.info(`backfill fieldtrip waypoint location ok for mission ${mission.guid} waypoint #${i+1}: ${w.guid}: ${w.title}`);
        } else {
          all = false;
          log.error(`unable to backfill fieldtrip waypoint location for mission ${mission.guid} waypoint #${i+1}: ${w.guid}: ${w.title}`);
        }
      }
    }
  }
  return all;
}

async function saveMission(mission: IMissionInfoSummary | IMissionInfoDetailed, req?: Express.Request): Promise<boolean> {

  let log = getLogger(req);
  let userId = getUserId(req);

  log.info({ mission, userId }, "saving mission");

  let filterQuery: any = {
    guid: mission.guid
  }

  let updateQuery: any = {
    $set: {
      lastModified: Date.now()
    },
    $setOnInsert: {
      created: Date.now(),
    }
  }

  let isDetails = (mission as IMissionInfoDetailed).waypoints != null;

  // summary fields
  updateQuery.$set = <IMissionInfoSummary>{
    ...updateQuery.$set,
    guid: mission.guid,
    image: mission.image,
    portalGuid: mission.portalGuid,
    rating: mission.rating,
    time: mission.time,
    title: mission.title,
    locationPoint: mission.locationPoint,
  }

  if (isDetails) {

    let details = mission as IMissionInfoDetailed;

    let backfilledAll = await backfillFieldtripWaypoints(details);
    if (!backfilledAll) log.warn("could not backfill all fieldtrip waypoints for mission", mission.guid);
    else log.info("fieldtrip backfill ok")

    updateMissionMetrics(details);

    updateQuery.$set = <IMissionInfoDetailed>{
      ...updateQuery.$set,
      type: details.type,
      author: details.author,      
      description: details.description,
      numCompleted: details.numCompleted,
      numWaypoints: details.waypoints?.length,
      waypoints: details.waypoints?.map(w => importForeignWaypointObject(w)),
      // metrics added data
      numMissions: details.numMissions,
      distance: details.distance,
      numWaypointObjectives: details.numWaypointObjectives,
      numUniques: details.numUniques,
      startPoint: details.startPoint
    }

    updateQuery.$setOnInsert.detailsCreated = Date.now();
    updateQuery.$set.detailsModified = Date.now();

    if (userId) {
      updateQuery.$addToSet = {
        "userData.refreshedBy": userId
      }
      updateQuery.$set["userData.refreshedRecentlyBy." + userId] = Date.now();
    }

  } else {

    updateQuery.$setOnInsert.summaryCreated = Date.now();
    updateQuery.$set.summaryModified = Date.now()

  }

  // narrow in on location
  // if (mission.locationBounds) {
  //   updateQuery.$max = {
  //     'locationBounds.sw.lat': mission.locationBounds.sw.lat,
  //     'locationBounds.sw.lng': mission.locationBounds.sw.lng,
  //   };
  //   updateQuery.$min = {
  //     'locationBounds.ne.lat': mission.locationBounds.ne.lat,
  //     'locationBounds.ne.lng': mission.locationBounds.ne.lng,
  //   };
  // }

  if (mission.portalGuid == null) 
    delete updateQuery.$set.portalGuid; // keep old value if set

  log.debug({ updateQuery }, "---SAVEMISSION---");

  let res = await missionsCollection.updateOne(
    filterQuery,
    updateQuery,
    {
      upsert: true
    }
  );

  // add to last-refreshed collection of a user?
  if (isDetails && userId) {
    if (userId) {
      log.info("adding to user's last-refreshed collection..");
      await usersCollection.updateOne(
        {
          _id: resolveObjectID(userId)
        },
        {
          $addToSet: {
            lastRefreshedMissionIds: mission.guid 
          }
        }
      );
    }
  }

  return res.result.n == 1;
}

async function queryMissionStatuses(missions: MissionInfo[], req?: Express.Request): Promise<ISearchResults<MissionInfo>> {
  let guids = missions.map(m => m.guid);
  //log.info("query guids");  
  //console.dir(guids);
  let query = { 'guid': { $in: guids } };
  //log.info(JSON.stringify(query));
  let known = await queryMissions(query, {}, { req, pageSize: 0, page: 1, includeDetails: false });
  //let known = await missionsCollection.find(query).toArray();

  let resp = missions.map(m => {
    let km = known.results.find(k => k.guid == m.guid) as IMissionInfoSummary;
    if (km) {
      m = km;
      (<any>m).status = {
        isIndexed: (<any>km).waypoints != null
      }
    } else {
      (<any>m).status = {
        isIndexed: false
      }
    }
    delete (<any>m).waypoints;
    return m;
  });

  return wrapSearchResults(resp);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  GEO ZONES
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

interface IGeoAdminLevelTemp {
  name: string;
  level: number;
  children?: {[name: string]: IGeoAdminLevelTemp};
  count: number;
}

async function queryCountryGeoZones(country: string, isMissionsMode: boolean): Promise<IGeoAdminLevel[]> {
  //log.info("country = " + country);

  let query = {
    'addressHierarchy.0': country,
    'deleted': { $ne: true },
    'numMissions': isMissionsMode ? 1 : { '$gt': 1 }
  };
  
  let filter = { 'addressHierarchy': true };

  let banners = await bannersCollection.find(query).project(filter).toArray();

  let hierarchy: IGeoAdminLevelTemp = {
    name: '',
    level: -1,
    children: {},
    count: 0
  };

  banners.forEach((b, i) => {
    let at = hierarchy;
    let parts = b.addressHierarchy as Array<string | null>;
    if (parts) {
      parts.forEach((p, level) => {
        let name = p == null ? "" : p;
        if (!at.children?.[name]) {
          at.children![name] = {
            name: name,
            level: level,
            children: {},
            count: 0
          };
        }
        at = at.children![name];
        if (at) at.count++;
      })
    }
  })

  const reduced: IGeoAdminLevel = {
    name: '',
    level: -1,
    children: [],
    count: 0
  }

  const compact = (at: IGeoAdminLevelTemp, to: IGeoAdminLevel) => {
    let keys = Object.keys(at.children!);
    if (keys.length) {
      keys.forEach(key => {
        if (key == "") {
          let emptyAt = at.children![""];
          compact(emptyAt, to);
        } else {
          let at2 = at.children![key];
          let to2: IGeoAdminLevel = {
            name: at2.name,
            level: at2.level,
            children: [],
            count: at2.count
          }
          to.children?.push(to2);
          compact(at2, to2);
          if (to2.children!.length == 0)
            delete to2.children;
          else
            to2.children?.sort((a,b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));
        }
      })
    }
  }

  compact(hierarchy, reduced);

  return reduced.children!;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  EXPRESS HELPERS
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function queryMissionsAndSendResponse(query: any, sort: any, req: Express.Request, res: Express.Response, allowOverridePageSize: boolean = false, disablePaging: boolean = false): Promise<void> {

  let page = parseInt(<string>req.query.page);
  if (isNaN(page)) page = 1;

  let user = getUser(req);
  if (user && user.roles && user.roles.indexOf("pagesize") >= 0) {
    allowOverridePageSize = true;
  }

  let pageSize = defaultPageSize;
  if (allowOverridePageSize) {
    let ps = parseInt(<string>req.query.pageSize);
    if (!isNaN(ps) && ps >= 0) pageSize = ps;
  }

  if (disablePaging === true) {
    pageSize = 0;
    page = 1;
  }

  let includeDetails = req.query.t == "details";

  let results = await queryMissions(query, sort, { req, page, pageSize, includeDetails });
  res.status(200).json(results);

}

function getUser(req?: Express.Request): IUser | undefined {
  return req ? req.user as IUser : undefined;
}

function getUserId(req?: Express.Request): string | undefined {
  let user = getUser(req);
  return user ? resolveObjectID(user._id).toHexString() : undefined;
}

function getLogger(req?: Express.Request): Logger {
  let logger = req ? (<any>req).log : log;
  return logger || log;
}

function personalizeMissionUserData(mission: MissionInfo, req?: Express.Request): MissionInfo  {
  if (req) {
    let userId = getUserId(req);    
    let userData = mission.userData || {};    
    mission.userData = {
      favoritedBy: (userData.favoritedBy || []).filter(x => x == userId)
    }
  } else {
    mission.userData = {
      favoritedBy: []
    }
  }
  return mission;
}

function personalizeBannerUserData(banner: IBanner, req?: Express.Request): IBanner {
  if (req) {
    let userId = getUserId(req);
    let userData = { ...DefaultBannerUserData, ...banner.userData };
    
    let ud: any = {};

    Object.keys(userData).forEach(k => {
      let v = userData[k];
      if (v instanceof Array) { // array? check if we're in it
        ud[k] = v.indexOf(userId) >= 0;
      } else if (typeof v === 'object') { // object? get value
        ud[k] = v[userId];
      } else {
        ud[k] = v;
      }
    })

    // banner.userData = {
    //   completedBy: (userData.completedBy || []).filter(x => x == userId),
    //   favoritedBy: (userData.favoritedBy || []).filter(x => x == userId),
    //   likedBy: (userData.likedBy || []).filter(x => x == userId),
    //   toDoBy: (userData.toDoBy || []).filter(x => x == userId),
    // }

    banner.userData = ud;

  } else {
    banner.userData = {
      completedBy: [],
      likedBy: [],
      toDoBy: []
    }
  }
  return banner;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  EXPRESS API HANDLERS
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function apiGetAuth(req: Express.Request, res: Express.Response) {
  log.info(`AUTH -> check ${JSON.stringify(req.user)}`);
  if (req.user) {
    res.status(200).json(req.user);
  } else {
    res.status(404).json({});
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const SphericalMercator = require('@mapbox/sphericalmercator');
const merc = new SphericalMercator({ size: 256 });

async function queryBannerGeoData(w: number, s: number, e: number, n: number, req?: Express.Request) {

  let query = {
    'deleted': { $ne: true },
    $and: [
      { 'point.lat': { $lt: n } },
      { 'point.lng': { $lt: e } },
      { 'point.lat': { $gte: s } },
      { 'point.lng': { $gte: w } },
    ]
    // 'point.geoJSON': {
    //   '$geoWithin': {
    //     $box: [ [ w, s ], [ e, n ] ]
    //   }
    // }
  }

  // very minimal set of data
  let projection = {
    guid: true,
    title: true,
    'images.small': true,
    'point.lat': true,
    'point.lng': true,
    rowSize: true,
    numMissions: true,
    numWaypoints: true,
    distance: true,
    userData: true,
    _id: false
  }

  //log.info({ x, y, z, n, e, s, w }, `map query`);
  //log.info(`map query [${x},${y},${z}] -> (${n},${e})-(${s},${w})`);
  let banners = await bannersCollection.find(query).project(projection).toArray();

  banners = banners.map(b => personalizeBannerUserData(b, req));

  return banners;
}

async function queryMissionGeoData(w: number, s: number, e: number, n: number, includeSummaries: boolean, includeUsed: boolean, req?: Express.Request) {

  let query: any = {

    'startPoint.geojson': {
      '$geoWithin': {
        $box: [ [ w, s ], [ e, n ] ]
      }
    }

    // $and: [
    //   { 'locationPoint.lat': { $lt: n } },
    //   { 'locationPoint.lng': { $lt: e } },
    //   { 'locationPoint.lat': { $gte: s } },
    //   { 'locationPoint.lng': { $gte: w } },
    // ]
  }

  if (!includeUsed) {
    query['usedInBanners.0'] = { $exists: false } // require that the 0th element does not exist = no elements in the array   
  }

  if (!includeSummaries) {
    query.detailsModified = { $ne: null };
  }

  // very minimal set of data
  let projection = {
    guid: true,
    title: true,
    image: true,
    portalGuid: true,
    startPoint: true,
    detailsModified: true,
    usedInBanners: true,
    _id: false
  }

  let missions = (await missionsCollection
    .find(query)
    .project(projection)
    .toArray()) as Array<IMissionInfoDetailed>;
  missions.forEach(m => {
    delete m.userData;
    if (m.usedInBanners != null) m.usedInBanners = [];
  });

  return missions;

}

async function apiGetMapTileData(req: Express.Request, res: Express.Response) {

  let x = ensureNumber(req.params.x);
  let y = ensureNumber(req.params.y);
  let z = ensureNumber(req.params.z);

  let bbox = merc.bbox(x, y, z);
  let [ w, s, e, n ]= bbox;

  let bannerFilter = ensureNumber(req.query.banners) || 0;
  let missionFilter = ensureNumber(req.query.missions) || 0;

  let results: Array<any> = [];

  if (bannerFilter != 0) {
    (await queryBannerGeoData(w, s, e, n, req)).forEach(b => {
      results.push({ b });
    })
  }

  if (missionFilter != 0) {
    let includeSummaries = (missionFilter & 2) != 0;
    let includeUsed = (missionFilter & 4) != 0;
    (await queryMissionGeoData(w, s, e, n, includeSummaries, includeUsed, req)).forEach(m => {
      results.push({ m });
    })
  }

  res.status(200).json(wrapSearchResults(results));
}


async function apiGetBannerMapTileData(req: Express.Request, res: Express.Response) {
  let x = ensureNumber(req.params.x);
  let y = ensureNumber(req.params.y);
  let z = ensureNumber(req.params.z);

  let bbox = merc.bbox(x, y, z);
  let [ w, s, e, n ]= bbox;

  let query = {
    
    'deleted': { $ne: true },
    $and: [
      { 'point.lat': { $lt: n } },
      { 'point.lng': { $lt: e } },
      { 'point.lat': { $gte: s } },
      { 'point.lng': { $gte: w } },
    ]

    // 'point.geoJSON': {
    //   '$geoWithin': {
    //     $box: [ [ w, s ], [ e, n ] ]
    //   }
    // }

  }

  // very minimal set of data
  let projection = {
    guid: true,
    title: true,
    'images.small': true,
    'point.lat': true,
    'point.lng': true,
    rowSize: true,
    numMissions: true,
    numWaypoints: true,
    distance: true,
    userData: true,
    _id: false
  }

  //log.info({ x, y, z, n, e, s, w }, `map query`);
  log.info(`map query [${x},${y},${z}] -> (${n},${e})-(${s},${w})`);
  let banners = await bannersCollection.find(query).project(projection).toArray();

  banners = banners.map(b => personalizeBannerUserData(b, req));

  log.info("results = ", banners.length);

  res.status(200).json(wrapSearchResults(banners));
}

async function apiSearchBanners(req: Express.Request, res: Express.Response) {

  let userId = getUserId(req);

  let query: any = {
    numMissions: req.query.mode == "missions" ? 1 : { $gt: 1 }
  };
  let sort: any = null;
  let project: any = {
    _id: false,
    missions: false,
    missionGuids: false,
    reviews: false,
    routes: false,
    addressLookups: false,
    versionHistory: false
  };

  let title = (<string>req.query.title || "").toString().trim();
  if (title != null && title.length) {
    query.title = makeRegexWildcard(title);
  }

  if (userId) {

    if (parseBool(<string>req.query.requireFavorited)) {
      query["userData.favoritedBy"] = { $in: [ userId ] };
    }

    if (parseBool(<string>req.query.requireToDo)) {
      query["userData.toDoBy"] = { $in: [ userId ] };
    }
    
    if (parseBool(<string>req.query.requireCompleted)) {
      query["userData.completedBy"] = { $in: [ userId ] };
    }

    if (parseBool(<string>req.query.excludeCompleted)) {
      query["userData.completedBy"] = { $nin: [ userId ] };
    }

  }

  if (req.query.nesw) {
    let nesw = req.query.nesw.toString().split(',').map(parseFloat).filter(c => !isNaN(c));
    if (nesw.length == 4) {
      let [n,e,s,w] = nesw;
      query = {
        ...query,
        $and: [
          { 'point.lat': { $lte: n } },
          { 'point.lng': { $lte: e } },
          { 'point.lat': { $gte: s } },
          { 'point.lng': { $gte: w } },
        ]
      }
    }
  }

  let pSort = (req.query.sort || "").toString();
  let pSortReverse = (req.query.sortReverse || "").toString() == "true";

  let geo: Array<string | null | undefined> = [];
  try {
    geo = JSON.parse(<string>req.query.geo);
    if (geo instanceof Array) {
      geo.reverse();
      for (let i = 0; geo.length > i; i++) {
        if (geo[i] != null) {
          geo = geo.slice(i).reverse();
          break;
        }
      }
    }
  } catch {
    if (typeof req.query.geo == 'string')
      geo = [ <string>req.query.geo ];
  }
  //console.dir(geo);

  geo.forEach((chunk, index) => {
    query[`addressHierarchy.${index}`] = chunk;
  });

  if (pSort != "") {
    sort = {[pSort]: pSortReverse ? -1 : 1};
  }

  //console.dir(query);

  let results = await queryBanners(query, sort, project, req);
  res.status(200).json(results);
}

async function apiServeOpenGraphBannerHtml(req: Express.Request, res: Express.Response) {

  let bannerId = ensureString(req.params.bannerId);
  let banner = await getBannerByIdOrSlug(bannerId);
  if (banner != null) {

    let html = (await fsp.readFile(path.join(config.paths.www, "index.html"))).toString();

    let opengraphHtml = 
        `<meta property="og:title" content="${escapeHtml(banner.title)}">`
      + `<meta property="og:description" content="${escapeHtml(banner.description)}">`
      + `<meta property="og:image" content="${config.http.publicUrl}static/images/${banner.images!.medium!}">`
      + `<meta property="og:url" content="${config.http.publicUrl}banner/${banner.slug || banner.guid}">`
      + `<meta name="twitter:card" content="summary_large_image">`;

    html = html
    .replace(/<title>.*?<\/title>/, `<title>${escapeHtml(banner.title)}</title>`)
    .replace(`<meta name="og:placeholder">`, opengraphHtml);

    res.header("Content-Type: text/html").send(html);
  } else {
    res.redirect("/");
  }
}

async function getBannerByIdOrSlug(bannerId: string): Promise<IBanner> {

  const isUuid = new RegExp(/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i);

  // get by guid or slug
  let banner = isUuid.test(bannerId)
    ? await bannersCollection.findOne({ guid: bannerId }) as IBanner
    : await bannersCollection.findOne({ slug: bannerId }) as IBanner;

  // if not found, try to match the end of the slug (if any)
  if (banner == null) {
    let m = /-([a-f0-9]{8})$/.exec(bannerId);
    if (m) {
      banner = await bannersCollection.findOne({ slug: new RegExp("-" + m[1] + "$") });
    }
  }

  return banner;

}

async function apiGetBannerById(req: Express.Request, res: Express.Response) {

  let bannerId = ensureString(req.params.bannerId);
  let banner = await getBannerByIdOrSlug(bannerId);

  if (banner == null) {
    res.status(404).end();
    return;
  }

  delete banner?.versionHistory;
  if (banner) {
    if (req.query.full) {
      if (banner.reviews) {
        let users = await queryUsers(banner.reviews.map(r => r.createdBy!), true);
        (<any>banner).$refs = { users }; // HACK!
      }
      res.status(200).json(personalizeBannerUserData(banner, req));
    } else {
      res.status(200).json(stripBannerData(personalizeBannerUserData(banner, req)));
    }
  }
  else res.status(404).json({ error: 'not found' });

}

function ensureArray(x: any, itemConverter?: Function): Array<any> {
  if (x instanceof Array) {
    if (itemConverter) return x.map(y => itemConverter(y));
    else return x;
  } else {
    return [];
  }
}

function importBanner(banner: IBanner) {
  let b: IBanner = {
    guid: ensureString(banner.guid),
    title: ensureString(banner.title),
    description: ensureString(banner.description),
    rowSize: ensureNumber(banner.rowSize)!,
    missionGuids: ensureArray(banner.missionGuids, ensureString),
    missions: ensureArray(banner.missions)
  }
  return b;
}

async function apiUpdateBanner(req: Express.Request, res: Express.Response) {
  try {
    let update: IBanner = req.body as IBanner;
    if (typeof update !== 'object') throw new Error("Invalid banner data");
    update = importBanner(update);

    let guid = ensureString(req.params.bannerId);
    if (guid == "" || guid == null) throw new Error("Missing or invalid banner id");

    delete update.guid;
    let old = await bannersCollection.findOne({ guid: guid }) as IBanner;
    if (!old) throw new Error("Banner not found");

    let userId = getUserId(req);
    let user = getUser(req);
    log.info({ req, userId, roles: user?.roles, guid }, "checking permissions for updating banner..");
    if (userId != old.createdBy) {
      if (!user || !user.roles || user.roles.indexOf(ROLE_ADMIN) < 0) {
        throw new Error("Not admin or user who registered the banner");
      }
    }

    await preprocessBanner(update);
    delete update.guid;
    delete update.userData;
    delete update.reviews;
    delete update.versionHistory;
    log.info({ update }, "Updating banner..");

    //console.dir(update, { depth: null });

    delete old.versionHistory;
    let upd = await bannersCollection.updateOne({
      guid: guid
    }, {
      $set: update,
      $push: {
        versionHistory: old
      }
    })

    log.info(`Matched ${upd.matchedCount}, updated ${upd.modifiedCount}`);

    let banner = await bannersCollection.findOne({ guid: guid });
    await removeUsedInReferences(old);
    await addUsedInReferenced(banner);

    // enqueue image update
    generateBannerImageQueue.add("update:" + resolveObjectID(banner._id).toHexString());

    res.status(200).json(personalizeBannerUserData(banner));

  } catch (err) {
    log.error({ req, err }, "Error while updating banner");
    res.status(500).json({ error: err.message || err })
  }
}

async function addUsedInReferenced(banner: IBanner): Promise<void> {
  let missionGuids = banner.missions!.map(m => m.guid);
  log.info({ guid: banner.guid, missionGuids }, "referencing banner as used-in in all missions..");
  await missionsCollection.updateMany({
    guid: {
      $in: missionGuids
    }
  }, {
    $addToSet: {
      usedInBanners: banner.guid
    }
  })
}

async function removeUsedInReferences(banner: IBanner): Promise<void> {
  
  let missionGuids = banner.missions!.map(m => m.guid);
  log.info({ guid: banner.guid, missionGuids }, "unreferencing banner as not used-in in all missions..");
  await missionsCollection.updateMany({
    guid: {
      $in: missionGuids
    }
  }, {
    $pull: {
      usedInBanners: banner.guid
    }
  })
}

async function apiCreateBanner(req: Express.Request, res: Express.Response) {
  try {
    let banner: IBanner = req.body as IBanner;
    if (typeof banner === 'object') {

      banner = importBanner(banner);

      banner.createdBy = getUserId(req);
      banner.createdAt = Date.now();
      banner.guid = uuid();
      banner.slug = makeBannerSlug(banner);

      await preprocessBanner(banner);

      // TODO: add banner id to all missions' usedIn data
      log.info("saving..");
      let ins = await bannersCollection.insertOne(banner);
      
      log.info("saved -> ", ins.insertedId);

      await addUsedInReferenced(banner);

      // enqueue image update
      generateBannerImageQueue.add("add:" + resolveObjectID(ins.insertedId).toHexString());
      //await makeBannerImages(banner, imageFilenameTemplate);

      res
        .status(201)
        .set({
          'X-Location': `${apiPrefix}banners/${banner.guid}`
        })
        .json(banner);

    } else {
      log.error("Invalid data while saving banner");
      res.status(400).json({ error: 'invalid data' });
    }
  } catch (err) {
    log.error({ err }, "Unhandled error while saving banner");
    res.status(500).json({ error: err.message || err })
  }
}

async function apiDeleteBanner(req: Express.Request, res: Express.Response) {
  let guid = ensureString(req.params.bannerId).trim();
  try {
    if (guid == null || guid == "") throw new Error("missing guid");

    // mark as deleted
    //bannersCollection.deleteOne({ guid: guid });
    await bannersCollection.updateOne({ guid: guid }, { $set: { deleted: true } }); // just mark it as deleted

    // remove mission references
    let banner = await bannersCollection.findOne({ guid: guid }) as IBanner;
    await removeUsedInReferences(banner);

    res.status(204).end();
  } catch (err) {
    log.error({ err, guid }, "error while deleting banner");
    res.status(500).end();
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function apiGetGeoCountries(req: Express.Request, res: Express.Response) {

  let country = (req.params.country || "").toString();
  let isMissionsMode = req.query.mode == "missions";

  if (country == null || country == "") {

    let aggr = isMissionsMode ?
      await bannersCollection.mapReduce(
        function() {
          if (this.deleted !== true && this.addressHierarchy && this.numMissions == 1)
            emit(this.addressHierarchy[0], 1)
        },
        function(key, values) {
          return { count: values.length };
        },
        {
          query: { addressHierarchy: { $exists: true } },
          out: { inline: 1 }
        }
      )
      :
      await bannersCollection.mapReduce(
        function() {
          if (this.deleted !== true && this.addressHierarchy && this.numMissions > 1)
            emit(this.addressHierarchy[0], 1)
        },
        function(key, values) {
          return { count: values.length };
        },
        {
          query: { addressHierarchy: { $exists: true } },
          out: { inline: 1 }
        }
      );

    let countries: ICountryInfo[] = [];
    for (let i = 0; aggr.length > i; i++) {
      let x = aggr[i];
      countries.push({
        name: x._id,
        count: x.value.count,        
        // hierarchy: await queryCountryGeoZones(x._id)
      })
    }

    //aggr.forEach((x: any) => countries[x._id] = { count: x.value.count });
    res.status(200).json(countries);

    // let countries = (await bannersCollection.distinct('addressHierarchy.0')) as string[];
    // res.status(200).json(countries.sort());

  } else {

    res.status(200).json(await queryCountryGeoZones(country, isMissionsMode));
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function apiGetMissionsByGuids(req: Express.Request, res: Express.Response) {
  let guids: string[] = [];
  if (req.method.toLowerCase() == "post") {
    //guids = ensureArray(req.body, ensureString);
    log.info({ body: req.body });
    guids = ensureArray(req.body);
    log.info({ guids }, "checking guids by post body");
  } else {
    guids = req.params.list.split(",").filter(q => q.trim().length);
    log.info({ guids }, "checking guids by path param");  
  }
  queryMissionsAndSendResponse({ guid: { $in: guids }}, {}, req, res, true, true);
}

async function apiSearchMissions(req: Express.Request, res: Express.Response) {

  let sort = DefaultMissionsSort;

  let query: any = {
  };

  let userId = getUserId(req);

  if (userId) {
    if (parseBool(<string>req.query.requireFavorite)) {
      query["userData.favoritedBy"] = { $in: [ userId ] };
    }
    if (parseBool(<string>req.query.requireRecent)) {
      query["userData.refreshedRecentlyBy." + userId] = { $ne: null };
      sort = {
        ["userData.refreshedRecentlyBy." + userId]: -1
      }
    }
  }

  if (parseBool(<string>req.query.requireUnused)) {
    query['usedInBanners.0'] = { $exists: false } // require that the 0th element does not exist = no elements in the array   
  }

  let title = (<string>req.query.title || "").toString().trim();
  if (title.length > 0) {
    query.title = makeRegexWildcard(title);
  }

  let nesw = (<string>req.query.nesw || "").toString().trim();
  if (nesw.length > 0) {
    let [n,e,s,w] = nesw.split(/,/g).map(v => parseFloat(v));
    if (!isNaN(e) && !isNaN(w) && !isNaN(s) && !isNaN(n)) {
      query.$or = [
        {
          $and: [
            { 'locationPoint.lng': { $gte: w } },
            { 'locationPoint.lng': { $lte: e } },
            { 'locationPoint.lat': { $gte: s } },
            { 'locationPoint.lat': { $lte: n } },
          ]
        },
        {
          $and: [
            { 'locationBounds.sw.lng': { $gte: w } },
            { 'locationBounds.ne.lng': { $lte: e } },
            { 'locationBounds.sw.lat': { $gte: s } },
            { 'locationBounds.ne.lat': { $lte: n } },
          ]
        },
        {
          $and: [
            { 'waypoints.0.lng': { $gte: w } },
            { 'waypoints.0.lng': { $lte: e } },
            { 'waypoints.0.lat': { $gte: s } },
            { 'waypoints.0.lat': { $lte: n } },
          ]
        }
      ]
    }
  }

  let requireDetails = <string>req.query.requireDetails == "1";
  if (requireDetails) {
    query.detailsModified = { $ne: null };
  }

  queryMissionsAndSendResponse(query, sort, req, res);
}

async function apiGetMissionById(req: Express.Request, res: Express.Response) {

  let missionId = (req.params.missionId || "").toString();
  let mission = await missionsCollection.findOne<IMissionInfoDetailed>({ guid: missionId });
  if (mission) res.status(200).json(personalizeMissionUserData(mission, req));
  else res.status(404).json({ ok: false, error: 'mission not found' });

}

async function apiListMissions(req: Express.Request, res: Express.Response) {
  queryMissionsAndSendResponse({}, {}, req, res);
}

async function apiSubmitMissionData(req: Express.Request, res: Express.Response) {

  let format = ensureString(req.query.format, true);
  if (format == "") format = "default"
  let data: IMissionInfoDetailed | IMissionInfoSummary[] | IMissionInfoSummary = req.body;

  let log = getLogger(req);
  log.info({ data, format }, "submit mission data");

  if (format == "missionsplugin") {
    if (data instanceof Array) {
      // array of missions summaries
      let summaries = <unknown>data as Array<MissionsPlugin.IMissionSummary>;
      data = summaries.map(summary => convertMissionPluginSummary(summary));
      log.info({ data }, "converted missionplugin summaries");
    } else if (typeof data == 'object') {
      // a mission detail (or summary..hm?)
      if ((<any>data).waypoints) {
        let mission = <unknown>data as MissionsPlugin.IMissionDetails;
        data = convertMissionPluginDetails(mission);
        log.info({ data }, "converted missionplugin detail");
      } else {
        let mission = <unknown>data as MissionsPlugin.IMissionSummary;
        data = convertMissionPluginSummary(mission);
        log.info({ data }, "converted missionplugin summary");
      }
    } else {
      res.status(400).json({ ok: false, error: 'invalid data' });
      return;
    }
  }

  if (data instanceof Array) {

    log.info({ data }, "storing mission summaries");
    let missions: IMissionInfoSummary[] = <IMissionInfoSummary[]><unknown>data;
    await Promise.all(missions.map(m => saveMission(m, req)));
    res.status(200).json(await queryMissionStatuses(missions, req))

  } else if ((<any>data).waypoints) {

    log.info({ data }, "storing mission detail");
    let missionDetails = <IMissionInfoDetailed><unknown>data;
    await saveMission(missionDetails, req);
    res.status(200).json(await queryMissionStatuses([missionDetails], req))

  } else if (data != null && typeof data == 'object') {

    log.info({ data }, "storing mission summary");
    let missionSummary = <IMissionInfoSummary><unknown>data;
    await saveMission(missionSummary, req);
    res.status(200).json(await queryMissionStatuses([missionSummary], req))

  } else {

    log.error({ data, format }, "invalid data");
    res.status(400).json({ ok: false, error: 'invalid data' });

  }
}

async function apiSaveMission(req: Express.Request, res: Express.Response) {
  let data: IMissionInfoSummary | IMissionInfoDetailed = req.body;
  if ((<any>data).waypoints) {
    let missionDetails = <IMissionInfoDetailed><unknown>data;
    await saveMission(missionDetails);
    res.status(201).json({ ok: true })
  } else if (data != null && typeof data == 'object') {
    let missionSummary = <IMissionInfoSummary><unknown>data;
    await saveMission(missionSummary);
    res.status(201).json({ ok: true })
  } else {
    res.status(400).json({ ok: false, error: 'invalid data' });
  }
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function apiClearUserRecentlyRefreshedList(req: Express.Request, res: Express.Response) {
  let userId = getUserId(req);
  if (userId) {

    log.info("clearing recently refreshed in missions: " + userId);
    let upd = await missionsCollection.updateMany({
    }, {
      // $pull: { "userData.refreshedBy": userId },
      $unset: { ["userData.refreshedRecentlyBy." + userId]: "" }     
    });
    log.info(`cleared: ${ upd.modifiedCount }`);

    // clear list in user
    log.info("clearing recently refreshed in user: " + userId);
    let upd2 = await usersCollection.updateOne({
      _id: resolveObjectID(userId)
    }, {
      $set: {
        lastRefreshedMissionIds: []
      }
    })

    res.status(200).json({});
  } else {
    res.status(401).json({ error: 'Not logged in' });
  }
}

async function apiUpdateProfile(req: Express.Request, res: Express.Response) {

  let user = req.user as IUser;
  log.info("update profile for user:", user._id);

  let profile = req.body as IUser;
  if (typeof profile != 'object') {
    res.status(400).json({ error: 'invalid profile data '});
    return;
  }

  let newUser = {} as IUser;

  if (profile.agentFaction != user.agentFaction || profile.agentName != user.agentName) {
    newUser.agentName = profile.agentName;
    newUser.agentFaction = profile.agentFaction;
    newUser.agentVerified = false;
    newUser.agentVerificationChallenge = generateGlyphSequence2Id(); // humanReadableIds.random();
    newUser.roles = user.roles?.filter(r => r != "agentVerified") || [];
  }

  let query = { _id: resolveObjectID(user._id) }
  let update = { $set: newUser };

  log.info("updating user:", user._id);
  //console.dir(query);
  //console.dir(update);

  if (Object.keys(newUser).length == 0) {
    log.info("nothing to update for user:", user._id);
    let user2 = await usersCollection.findOne(query);
    res.status(200).json(user2);
  } else {
    let upd = await usersCollection.updateOne(query, update);
    if (upd.modifiedCount == 1) {
      log.info("successfully updated profile for user:", user._id);
      let user2 = await usersCollection.findOne(query);
      res.status(200).json(user2);
    } else {
      log.error("failed to update profile for user:", user._id);
      res.status(500).json({ error: "profile update failed" });
    }
  }
}

async function apiGenerateNewApiKey(req: Express.Request, res: Express.Response) {
  let user = req.user as IUser;
  log.info("generate new api key for user:", user._id);
  let apiKey = generateGlyphSequence2Id(); //humanReadableIds.random(); // uuid();
  let query = { _id: resolveObjectID(user._id) }
  let update = { $set: { apiKey: apiKey } };
  let upd = await usersCollection.updateOne(query, update);
  if (upd.modifiedCount == 1) {
    log.error("updated api key for user:", user._id);
    res.status(201).json({ apiKey });
  } else {
    log.error("failed to generate new api key for user:", user._id);
    res.status(400).json({ error: 'could not save new api key ??' })
  }
}

async function apiToggleMissionFavorite(req: Express.Request, res: Express.Response, state: boolean) {
  let user = req.user as IUser;
  if (user) {
    let query = { guid: req.params.missionId };
    let update = state
      ? { $addToSet: { "userData.favoritedBy": resolveObjectID(user._id).toHexString() } }
      : { $pull: { "userData.favoritedBy": resolveObjectID(user._id).toHexString() } };
    let upd = await missionsCollection.updateOne(query, update);
    res.status(204).end();
  }
}

async function apiToggleBannerUserData(req: Express.Request, res: Express.Response, propName: string, state: boolean) {
  let userId = getUserId(req);
  if (userId) {
    let bannerId = ensureString(req.params.bannerId);
    if (bannerId != "") {
      let query = { guid: req.params.bannerId };
      let update = state
        ? { $addToSet: { ["userData." + propName]: userId } }
        : { $pull: { ["userData." + propName]: userId } };
      let upd = await bannersCollection.updateOne(query, update);
      res.status(204).end();
    } else {
      res.status(400).end();
    }
  }
}

async function apiToggleBannerCompleted(req: Express.Request, res: Express.Response, state: boolean, review?: ICompletionReview) {
  let userId = getUserId(req);
  if (userId) {
    let bannerId = ensureString(req.params.bannerId);
    if (bannerId != "") {

      // add/remove from userData.completedBy
      log.info(`setting completedBy status for ${bannerId} for user ${userId} ..`);
      let query = { guid: bannerId };
      let update = state
        ? { $addToSet: { "userData.completedBy": userId } }
        : { $pull: { "userData.completedBy": userId } };
      let upd = await bannersCollection.updateOne(query, update);

      if (!state) {

        log.info(`clearing review for ${bannerId} for user ${userId} ..`);

        // remove it
        let rem = await bannersCollection.updateOne(
          { guid: bannerId },
          { $pull: { reviews: { createdBy: userId } } }
        );

        log.info(`- matched: ${rem.matchedCount} modified: ${rem.modifiedCount}`);

      } else if (review && typeof review === 'object') {

        review = importReview(review);

        // first try to update an existing
        review.modifiedAt = Date.now();
        let exSet: any = {};
        for (let key in review) {
          exSet[`reviews.$.` + key] = review[key]
        }
        let updQuery = {
          guid: bannerId,
          'reviews.createdBy': userId
        };
        let updUpdate = { $set: exSet }
        log.info("trying to update comment on ${bannerId} for user ${userId} ..");
        log.info(updQuery);
        log.info(updUpdate);
        let upd = await bannersCollection.updateOne(
          updQuery,
          updUpdate
        );
        log.info(`- matched: ${upd.matchedCount} modified: ${upd.modifiedCount}`);

        if (upd.modifiedCount == 0) {
          log.info("no records matched, trying insert..")
          // try to insert it
          review.createdBy = userId;
          review.createdAt = Date.now();
          let ins = await bannersCollection.updateOne(
            { guid: bannerId },
            { $push: { reviews: review } }
          )
          log.info(`- matched: ${ins.matchedCount} modified: ${ins.modifiedCount}`);
        }

      }

      res.status(204).end();
    } else {
      res.status(400).end();
    }
  }
}

async function apiRequestAgentVerification(req: Express.Request, res: Express.Response) {
  res.status(501).json({ error: 'not yet implemented' })
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function initializeExpress() {

  if (!sessionFeatures.http) return;

  log.info("initializing api..");

  Passport.serializeUser<any, any>((req: Express.Request, user: any, done: Function) => {
    let id: string | null = user ? user._id : null;
    log.info(`AUTH -> serializerUser [${JSON.stringify(user)}] -> [${id}]`);
    done(null, id);
  });
  
  Passport.deserializeUser(async (id, done) => {
    log.info(`AUTH -> deserializerUser [${id}}] ..`);
    let user = await usersCollection.findOne({ _id: new ObjectID((<any>id || "").toString()) }) as IUser;
    if (user) {
      if (typeof user._id === 'object') {
        user._id = (<ObjectID>(user._id)).toHexString();
      }
      // log.info(`AUTH -> deserializerUser [${id}}] -> [${JSON.stringify(user)}]`);
      log.info(`AUTH -> deserializerUser [${id}}] -> [${user.agentName}] [${user.googleProfile?.displayName}]`);
      done(null, user);
    }
    else {
      done(new Error("User not found"));
    }
    // User.findById(id, (err: NativeError, user: UserDocument) => {
    //     done(err, user.id);
    // });
  });

  Passport.use(basicApikeyStrategyId, new BasicStrategy({
      realm: 'authenticate-by-apikey'
    },
    async (username: string, password: string, done: Function) => {
      password = password.trim();
      if (password == "") return done(new Error("Missing password"));
      try {
        let user = await usersCollection.findOne({
          apiKey: password
        }) as IUser | undefined;
        if (user == null) return done(null, false);
        else return done(null, user);
      } catch (err) {
        done(err);
      }
    }
  ));

  Passport.use(apikeyStrategyId, new CustomStrategy(
    async (req: Express.Request, done: Function) => {
      let apiKey = (req.query.apiKey as string || "").toString();
      if (apiKey == "null" || apiKey == "undefined") apiKey = "";
      log.info(`[api-key strategy] checking api key '${apiKey}' ..`)
      if (apiKey == "") {
        log.error(`[api-key strategy] no api key supplied`)
        //done(new Error("missing API key"), null);
        done(null, null); // XXX: is his not pass-through?!
        return;
      }
      let user = await usersCollection.findOne({ apiKey: apiKey }) as IUser | undefined | null;
      if (user) {
        log.info(`[api-key strategy] matched user for api key '${apiKey}: ${user._id}'`);
        done(null, user);
      } else {
        log.error(`[api-key strategy] no match for api key '${apiKey}' ..`)
        //done(new Error("invalid API key"), null);
        done(null, null);
      }
    }
  ))

  Passport.use(googleStrategyId, new GoogleStrategy(
    {
      clientID: config.apiKeys.googleAuth.clientId,
      clientSecret: config.apiKeys.googleAuth.clientSecret,
      callbackURL: url.resolve(config.http.publicUrl, "auth/google/callback")
    },
    async (token: string, tokenSecret: string, profile: any, done: Function) => {
      try {
        log.info(`AUTH -> verify: token: ${token} tokenSecret: ${tokenSecret} profile: ${JSON.stringify(profile)}`);

        let query = {
          googleId: profile.id
        };

        // XXX WHY ISN'T THIS UPSERT WORKING?  INSERTS A NEW DOC EACH TIME!!

        // let update = {
        //   $setOnInsert: {
        //     created: Date.now()
        //   },
        //   $set: {
        //     googleId: profile.id,
        //     googleProfile: profile,
        //     lastSeen: Date.now()
        //   }
        // };

        // // upsert
        // let res = await usersCollection.updateOne(query, update, { upsert: true });

        log.info("\n\n\n--- QUERY USER ---\n\n\n");
        let res = await usersCollection.findOne(query);
        //console.dir(res);

        if (res == null) {
          log.info("\n\n\n--- CREATE USER ---\n\n\n");
          await usersCollection.insertOne({
            googleId: profile.id,
            googleProfile: profile,
            created: Date.now(),
            roles: [ 'contributor' ]
          })
        }

        // reload
        let user = await usersCollection.findOne(query);
        if (!user) throw Error("User not found?!");
        done(null, user);
      } catch (err) {
        done(err, null);
      }
    }
  ));

  const optionalApiKey = (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
    let apiKey = (req.query.apiKey as string || "").toString();
    if (apiKey == "null") apiKey = "";
    if (apiKey != "") {
      // if api key is set, autnenticate it
      log.info("[optional-api-key] have api key, trying: " + apiKey);
      Passport.authenticate(apikeyStrategyId, { session: false })(req, res, next); // one-off, do not create a session
    } else {
      // otherwise: continue as usual without authentication
      log.info("[optional-api-key] no api key, continuing unauthenticated");
      next();
    }
  }

  const requireLoggedIn = (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
    if (req.isAuthenticated() && req.user) {
      return next();
    } else {
      res.status(403).json({ error: 'unauthenticated' });
    }
  }

  const requireApiKey = Passport.authenticate(apikeyStrategyId);

  // const requireLoggedInOrApiKey = (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
  //   allowApiKey(req, res, () => {
  //     requireLoggedIn(req, res, next);
  //   })
  // }

  // const noAuthRequired = (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
  //   next();
  // }

  //const iitcPluginAuth = requireLoggedInOrApiKey;
  //const iitcPluginAuth = noAuthRequired;

  // const allowApiKey = async (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
  //   if (!req.isAuthenticated()) {
  //     let apiKey = (req.params.apiKey as string || "").toString().trim();
  //     if (apiKey != "") {
  //       let user = await usersCollection.find({ apiKey: apiKey });
  //       if (user) {
  //         log.info(`[api-key strategy] matched user for api key '${apiKey}: ${JSON.stringify(user)}'`);
  //         req.user = user;
  //       }
  //     }
  //   }
  //   next();
  // }

  //const requireApiKeyOrLoggedIn = requireLoggedIn; // FIXME: implement this

  function requireRole(roleName: string): ExpressMiddlewareFunction {
    const fun = (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
      let user = req.user as IUser;
      if (req.isAuthenticated() && user && user.roles && user.roles.indexOf(roleName) >= 0) {
        return next();
      } else {
        res.status(403).json({ error: 'unauthenticated/missing role' });
      }
    }
    return fun;
  }

  let apikeyGenerateLimiter = rateLimiterFactory.create('apikey-generate', 1, 60);
  let apikeyTestLimiter = rateLimiterFactory.create('apikey-test', 10, 60);
  let profileEditLimiter = rateLimiterFactory.create('profile-edit', 5, 60);
  
  // const isAuthenticated = (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
  //   if (req.isAuthenticated()) {
  //       log.info(`AUTH -> isAuthenticated! ${JSON.stringify(req.user)}`);
  //       return next();
  //   }
  //   res.redirect("#/login");
  // };

  // const isAuthorized = async (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
  //   const provider = req.path.split("/").slice(-1)[0];
  //   const user = req.user as IUser;
  //   if (user) next();
  //   else res.redirect(`/auth/${provider}`);
  //   // if (_.find(user.tokens, { kind: provider })) {
  //   //     next();
  //   // } else {
  //   //     res.redirect(`/auth/${provider}`);
  //   // }
  // };

  app = Express();

  app.set('trust proxy', true); // trust x-forwarded-for headers

  let apiSerial = 0;
  app.use((req, res, next) => {
    let serial = ++apiSerial;
    let component = `api:${serial}:${req.path}`;
    //let addr = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    let addr = req.socket.remoteAddress || "?";
    let log = loggers.api.child({ component, http: { serial, path: req.path, query: req.query, method: req.method, addr } });
    (<any>req).log = log;
    log.debug(`${req.method} ${req.path} ${JSON.stringify(req.query)}`);
    next();
  })

  app.use(compression());

  app.use(Express.json({ limit: '100MB' }));
  app.use(Express.urlencoded({ extended: true }));

  app.use(CookieParser());

  app.use(require('morgan')('combined'));

  const cookieSession = require('cookie-session');
  app.use(cookieSession({
    // milliseconds of a day
    maxAge: 24*60*60*1000,
    keys: [ config.http.sessionSecret ]
  }));

  app.use(Passport.initialize());
  app.use(Passport.session());

  // app.use(session({
  //   secret: config.http.sessionSecret,
  //   saveUninitialized: true, // don't create session until something stored
  //   resave: true, //don't save session if unmodified
  //   store: MongoStore.create({
  //     //touchAfter: 24 * 3600, // time period in seconds
  //     clientPromise: Promise.resolve(mongoClient),
  //     dbName: config.db.name,
  //     collectionName: config.db.collections.sessions,
  //   })
  // }));

  // app.use(session({
  //   secret: config.http.sessionSecret,
  //   store: MongoStore.create({ clientPromise: Promise.resolve(mongoClient), dbName: config.db.name, collectionName: config.db.collections.sessions })
  // }));  

  app.use(`${staticPrefix}images`, Express.static(path.join(rootPath, imagesPath)));

  let requireCORS = CORS({
    origin(origin, cb) {
       // XXX: temp hack, should have proper CORS headers
      log.info("[cors] check origin: " + origin);
      cb(null, true);
      // https://intel.ingress.com
    }
  })
  
  app.use(requireCORS);

  // see: https://developers.google.com/identity/protocols/oauth2/web-server
  app.get(`/auth/${googleStrategyId}`, (req, res, next) => {
    let log = getLogger(req);
    let auth = Passport.authenticate('google', {
      scope: [ "email", "profile" ],
      prompt: "select_account"
    });
    log.info(`AUTH -> auth google...`);
    auth(req, res, next);
  });

  // see: https://developers.google.com/identity/protocols/oauth2/web-server
  app.get(`/auth/${apikeyStrategyId}`, (req, res, next) => {
    res.redirect(`/auth/${apikeyStrategyId}/basic`);
  });

  app.get(`/auth/${apikeyStrategyId}`, (req, res, next) => {
    res.redirect(`/auth/${apikeyStrategyId}/basic`);
  });

  app.get(`/auth/${apikeyStrategyId}/basic`, Passport.authenticate(basicApikeyStrategyId), (req, res, next) => {
    res.redirect('/');
  })

  app.get(`/auth/${googleStrategyId}/callback`,   
    Passport.authenticate(googleStrategyId, { failureRedirect: url.resolve(config.http.publicUrl, '/#/login?failure') }),
    (req, res) => {
      let log = getLogger(req);
      log.info(`AUTH -> callback after login`);
      res.redirect(config.http.publicUrl);
    }
  );

  app.get("/auth/logout", (req, res) => {
    req.logout();
    res.status(200).json({ ok: 1 });
  });

  app.put(`${apiPrefix}profile`, requireLoggedIn, profileEditLimiter, apiUpdateProfile);
  app.put(`${apiPrefix}profile/api-key`, requireLoggedIn, apikeyGenerateLimiter, apiGenerateNewApiKey);
  app.get(`${apiPrefix}profile/api-key/test`, apikeyTestLimiter, requireApiKey, requireLoggedIn, (req, res) => { res.status(200).json({ ok: true }); })
  app.put(`${apiPrefix}profile/agent-verify`, requireLoggedIn, apiRequestAgentVerification);
  app.delete(`${apiPrefix}profile/recently-refreshed`, requireLoggedIn, apiClearUserRecentlyRefreshedList);

  app.get(`${apiPrefix}geo/country`, apiGetGeoCountries);
  app.get(`${apiPrefix}geo/country/:country`, apiGetGeoCountries);

  app.get(`${apiPrefix}auth`, requireLoggedIn, apiGetAuth);

  // REMOVE, LEGACY
  app.get(`${apiPrefix}apikey/test`, apikeyTestLimiter, requireApiKey, requireLoggedIn, (req, res) => { res.status(200).json({ ok: true }); });

  app.get(`/b/:bannerId`, apiServeOpenGraphBannerHtml);
  app.get(`/b/:bannerId/*`, apiServeOpenGraphBannerHtml);

  app.get(`${apiPrefix}map/:z/:x/:y`, optionalApiKey, apiGetMapTileData);

  [ "b", "banners" ].forEach(bannersPath => { // because "banners/*" can be blocked by some ad-banner systems like ublock

    app.post(`${apiPrefix}${bannersPath}`, requireLoggedIn, requireRole(ROLE_CONTRIBUTOR), apiCreateBanner);
    app.put(`${apiPrefix}${bannersPath}/:bannerId`, requireLoggedIn, requireRole(ROLE_CONTRIBUTOR), apiUpdateBanner);
    app.delete(`${apiPrefix}${bannersPath}/:bannerId`, requireLoggedIn, requireRole(ROLE_ADMIN), apiDeleteBanner);

    app.get(`${apiPrefix}${bannersPath}/map/:z/:x/:y`, optionalApiKey, apiGetBannerMapTileData);
    app.get(`${apiPrefix}${bannersPath}/search`, optionalApiKey, apiSearchBanners);
    app.get(`${apiPrefix}${bannersPath}/:bannerId`, optionalApiKey, apiGetBannerById);
  
    app.put(`${apiPrefix}${bannersPath}/:bannerId/favorite`, requireLoggedIn, (req, res) => apiToggleBannerUserData(req, res, 'favoritedBy', true));
    app.post(`${apiPrefix}${bannersPath}/:bannerId/favorite`, requireLoggedIn, (req, res) => apiToggleBannerUserData(req, res, 'favoritedBy', true));
    app.delete(`${apiPrefix}${bannersPath}/:bannerId/favorite`, requireLoggedIn, (req, res) => apiToggleBannerUserData(req, res, 'favoritedBy', false));
  
    app.put(`${apiPrefix}${bannersPath}/:bannerId/completed`, requireLoggedIn, (req, res) => apiToggleBannerCompleted(req, res, true, req.body));
    app.post(`${apiPrefix}${bannersPath}/:bannerId/completed`, requireLoggedIn, (req, res) => apiToggleBannerCompleted(req, res, true, req.body));
    app.delete(`${apiPrefix}${bannersPath}/:bannerId/completed`, requireLoggedIn, (req, res) => apiToggleBannerCompleted(req, res, false));
  
    app.put(`${apiPrefix}${bannersPath}/:bannerId/todo`, requireLoggedIn, (req, res) => apiToggleBannerUserData(req, res, 'toDoBy', true));
    app.post(`${apiPrefix}${bannersPath}/:bannerId/todo`, requireLoggedIn, (req, res) => apiToggleBannerUserData(req, res, 'toDoBy', true));
    app.delete(`${apiPrefix}${bannersPath}/:bannerId/todo`, requireLoggedIn, (req, res) => apiToggleBannerUserData(req, res, 'toDoBy', false));
  
  });

  [ "m", "missions" ].forEach(missionsPath => {

    app.get(`${apiPrefix}${missionsPath}`, optionalApiKey, apiListMissions);
    app.post(`${apiPrefix}${missionsPath}`, optionalApiKey, apiSubmitMissionData);
    app.get(`${apiPrefix}${missionsPath}/search`, optionalApiKey, apiSearchMissions);
    //app.get(`${apiPrefix}${missionsPath}/bounds/:n/:e/:s/:w`, apiSearchMissionsByBounds);
    app.get(`${apiPrefix}${missionsPath}/guids/:list`, optionalApiKey, apiGetMissionsByGuids);
    app.post(`${apiPrefix}${missionsPath}/guids`, optionalApiKey, apiGetMissionsByGuids);
    app.get(`${apiPrefix}${missionsPath}/:missionId`, optionalApiKey, apiGetMissionById);
    app.put(`${apiPrefix}${missionsPath}/:missionId`, optionalApiKey, apiSaveMission);
  
    app.put(`${apiPrefix}${missionsPath}/:missionId/favorite`, optionalApiKey, requireLoggedIn, (req, res) => apiToggleMissionFavorite(req, res, true));
    app.post(`${apiPrefix}${missionsPath}/:missionId/favorite`, optionalApiKey, requireLoggedIn, (req, res) => apiToggleMissionFavorite(req, res, true));
    app.delete(`${apiPrefix}${missionsPath}/:missionId/favorite`, optionalApiKey, requireLoggedIn, (req, res) => apiToggleMissionFavorite(req, res, false));
  
  })

  // app.use((req, res) => {
  //   res.redirect('/index.html');
  // });

  const server = http.createServer(app);
  const port = config.http.port + sessionFeatures.httpPort;
  server.listen(port, () => {
    log.info(`API ready on port ${port} !`)
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function makeBannerImages(banner: IBanner, filenameTemplate: string): Promise<void> {

  function tpl(size: string, ext: string): string {
    return path.join(imagesPath, filenameTemplate.replace('{id}', banner.guid!).replace('{size}', size).replace('{ext}', ext));
  }

  await mkdirp(imagesPath);

  // log.info("loading images..");
  // let images = await preloadBannerImages(banner.missions!);

  let manifestDataPath = path.join(tempPath, "banner_mission_images", banner.guid!);
  let manifest = await downloadBannerImages(banner.missions!, log, manifestDataPath);

  log.info("TPL = " + tpl('size', 'ext'));

  let largeScale = 2, smallScale = 1, tinyScale = 0.5;

  log.info("create large..");
  let image = await drawBanner(banner.missions!, log, {
    //images,
    imageManifest: manifest,
    imageManifestDataPath: manifestDataPath,
    tileSize: 64*largeScale,
    circle: true,
    padding: 2*largeScale,
    outline: 2*largeScale,
    rowSize: banner.rowSize
  });

  // log.info("write large.png..");
  // await writeCanvasToPngFile(image, tpl('large', 'png'));

  if (banner.images == null) {
    banner.images = {};
  }

  log.info("write large.jpg..");
  let largePath = tpl('large', 'jpg');
  await writeCanvasToJpegFile(image, largePath, 75);
  banner.images.large = path.basename(largePath);

  log.info("create small..");
  image = await drawBanner(banner.missions!, log, {
    // images,
    imageManifest: manifest,
    imageManifestDataPath: manifestDataPath,
    tileSize: 64*smallScale,
    circle: true,
    padding: 2*smallScale,
    outline: 2*smallScale,
    rowSize: banner.rowSize
  });
  // log.info("write small.png..");
  // await writeCanvasToPngFile(image, tpl('small', 'png'));
  log.info("write small.jpg..");
  let mediumPath = tpl('small', 'jpg');
  await writeCanvasToJpegFile(image, mediumPath, 75);
  banner.images.medium = path.basename(mediumPath);

  log.info("create tiny..");
  image = await drawBanner(banner.missions!, log, {
    imageManifest: manifest,
    imageManifestDataPath: manifestDataPath,
    // images,
    tileSize: 64*tinyScale,
    circle: true,
    padding: 2*tinyScale,
    outline: 2*tinyScale,
    rowSize: banner.rowSize
  });

  // log.info("write tiny.png..");
  // await writeCanvasToPngFile(image, tpl('tiny', 'png'));
  log.info("write tiny.jpg..");
  let smallPath = tpl('tiny', 'jpg');
  await writeCanvasToJpegFile(image, smallPath, 75);
  banner.images.small = path.basename(smallPath);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function updateMissionDataFormat(mission: IMissionInfoDetailed): void {
  if (mission.waypoints) {
    // convert old waypoint lat/lng to point
    mission.waypoints.forEach(w => {
      if (w.point == null) {
        if (w.lat != null && w.lng != null) {
          w.point = {
            lat: w.lat!,
            lng: w.lng!,
            latE6: Math.round(w.lat! * 1E6),
            lngE6: Math.round(w.lng! * 1E6),
            geojson: {
              type: 'Point',
              coordinates: [ w.lng!, w.lat! ]
            }
          }
        } else {
          delete (<any>w).point;
        }
      } else {
        if (w.point.latE6! > 90E6) {
          log.info("possible illegal lat/lng: ", w.point.latE6, w.point.lngE6, w.title, w.guid);
          w.point.latE6 = Math.round(w.point.latE6! / 10);
          w.point.lngE6 = Math.round(w.point.lngE6! / 10);
          (<any>w).point.bugged = true;
        }
        w.point.lat = w.point.latE6! / 1E6;
        w.point.lng = w.point.lngE6! / 1E6;
        w.point.geojson = {
          type: 'Point',
          coordinates: [ w.point.lng!, w.point.lat! ]
        }
      }
      // tslint:disable-next-line
      delete w.lat;
      // tslint:disable-next-line
      delete w.lng;
    })
  }
}

function updateMissionMetrics(mission: IMissionInfoDetailed): void {
  if (mission.waypoints != null) {

    mission.numWaypointObjectives = initializeWaypointsObjectivesMap();

    let wps = filterValidWaypoints(mission);

    let distance = 0;
    for (let i = 1; wps.length > i; i++) {
      distance += haversine(wps[i-1].point, wps[i].point);
    }
    mission.distance = distance;

    wps.forEach(wp => {

      // backfill correct geojson?
      if (wp.point && wp.point.geojson == null && wp.point.lat != null && wp.point.lng != null) {        
        wp.point.geojson = {
          type: 'Point',
          coordinates: [ wp.point.lng, wp.point.lat ]
        }
      }
      if (wp.point) delete wp.point.geoJSON;

      mission.numWaypointObjectives![wp.objective]++;
    })

    mission.numUniques = countUniques(wps);

    let wp0 = findFirstWaypoint(mission);
    if (wp0) {
      mission.startPoint = wp0.point;

      mission.locationPoint = wp0.point;
      delete mission.locationBounds;
    }
  }
}

async function updateBannerGeo(banner: IBanner, forceReverseLookups: boolean = false): Promise<IBanner> {
  
  // resolve geo details
  log.info("trying to reverse-lookup address..");
  let missions = banner.missions;
  if (missions && missions.length) {
    let firstWaypoint = findFirstWaypoint(missions[0]);
    if (firstWaypoint) {
      log.info(`first waypoint is: ${firstWaypoint.point.lat},${firstWaypoint.point.lng}`);

      banner.point = firstWaypoint.point;

      if (!banner.addressLookups) banner.addressLookups = {};

      if (!banner.addressLookups["nominatimGeoCode"] || forceReverseLookups) {

        let rev = await nominatimReverseLookupGeoCodeJson(
          firstWaypoint.point.lat!,
          firstWaypoint.point.lng!,
          {
            addressDetails: true,
            acceptLanguage: 'en-EN',
          }
        );
        banner.addressLookups["nominatimGeoCode"] = rev;
      }
    } else {
      log.error("could not find first waypoint?!");
    }

    if (banner.addressLookups) {
      let rev = banner.addressLookups["nominatimGeoCode"];
      let addr: Array<string|undefined> | undefined = undefined;
      let feature = rev?.features?.[0];
      if (feature) {
        let admin = feature?.properties?.geocoding?.admin;
        if (admin) {
          addr = [
            feature!.properties!.geocoding!.country!,
            admin.level1,
            admin.level2,
            admin.level3,
            admin.level4,
            admin.level5,
            admin.level6,
            admin.level7,
            admin.level8,
            admin.level9,
            admin.level10
          ]
        }
      }
      banner.addressHierarchy = addr;
    }
  }
  return banner;
}

async function reloadBannerMissions(banner: IBanner, force: boolean = true): Promise<IBanner> {

  if (force) {
    log.info("forcing re-load of banner missions data..");
    if (banner.missions && !banner.missionGuids) { // copy mission guids from existing missions
      banner.missionGuids = banner.missions.map(m => m.guid);
    }
    delete banner.missions;
  }

  // load missions?
  if (banner.missions == null || banner.missions.length == 0) {
    if (banner.missionGuids && banner.missionGuids instanceof Array) {
      log.info("loading missions:", banner.missionGuids);
      let missions = await missionsCollection.find({ 'guid': { $in: banner.missionGuids.map(guid => (guid || "").toString()) }}).toArray()
      banner.missions = banner.missionGuids.map(guid => missions.find(m => m.guid == guid));
      banner.missions.forEach(m => { if (m == null) throw new Error("could not find all missions?") });
    } else { 
      banner.missions = []; // shrug..
      throw new Error("could not load missions for banner");
    }
  } else{

  }

  return banner;
}

async function updateBannerRoute(banner: IBanner): Promise<IBanner> {
  let providerId = "graphhopper";

  let LOG = log.child({ component: 'updateBannerRoute/' + banner.guid });
  let missions = banner.missions;
  if (missions) {
    for (let i = 0; missions.length > i; i++) {

      let mission = missions[i];

      if (!banner.routes) banner.routes = {};
      let route = banner.routes[mission.guid];
      if (route == null) route = banner.routes[mission.guid] = {};
      if (!route[providerId]) {

        let waypoints = filterValidWaypoints(mission);
        let latLngs = waypoints.map(w => (<ILatLng>{ lat: w.point.lat, lng: w.point.lng }));
        LOG.info({ latLngs }, "looking up route..");
        try {
          let resp = await queryGraphhopperRoute(config, latLngs, "foot");
          LOG.info({ resp }, "got a route response!");
          route[providerId] = resp;
        } catch (err) {
          LOG.error("error looking up route! %s %s", err.message, err.stack);
        }
      }

    }
  }
  return banner;
}

async function updateBannerMetrics(banner: IBanner): Promise<IBanner> {

  let missions = banner.missions;
  if (missions) {

    // re-calculate missions stats
    banner.numMissions = missions.length;
    banner.numWaypoints = 0;
    banner.distance = 0;
    banner.time = 0;
    banner.numWaypointObjectives = initializeWaypointsObjectivesMap();
    
    missions.forEach(mission => {
      updateMissionMetrics(mission);
  
      banner.time! += mission.time;
      banner.distance! += mission.distance!;
      banner.numWaypoints! += mission.numWaypoints!;
      for (let objective in WaypointObjective) {
        if (mission.numWaypointObjectives![objective]) banner.numWaypointObjectives![objective] += mission.numWaypointObjectives![objective];
      }
    })
  
    // add distance of gaps between missions
    for (let i = 1; missions.length > i; i++) {
      let prev = findLastWaypoint(missions[i-1]);
      let next = findFirstWaypoint(missions[i]);
      if (next && prev) banner.distance += haversine(prev.point, next.point);
    }
  
    // get all waypoints
    let allWaypoints = missions.reduce((p,c) => p.concat(filterValidWaypoints(c)), [] as IMissionWaypointEx[]);
    banner.numUniques = countUniques(allWaypoints);

    // update distance from ends-to-begins point
    banner.distanceEndsBegins = haversine(
      allWaypoints[0].point,
      allWaypoints[allWaypoints.length - 1].point
    );

    // calculate efficiency
    banner.timeEfficiency = ((1 / ((banner.time! / 1000) / banner.numWaypoints!)) * 1000);
    banner.distanceEfficiency = ((1 / ((banner.distance! / 1000) / banner.numWaypoints!)) * 1000);
    banner.combinedEfficiency = (banner.timeEfficiency + banner.distanceEfficiency) / 2;
  }

  return banner;
}

async function preprocessBanner(banner: IBanner): Promise<IBanner> {

  //console.dir(banner, { depth: null });

  log.info("+ reload banner missions..");
  await reloadBannerMissions(banner, true);

  log.info("+ update banner metrics..");
  await updateBannerMetrics(banner);

  log.info("+ update banner geo/addr..");
  await updateBannerGeo(banner);

  return banner;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  TELEGRAM
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

let bot: Telegraf | undefined;

async function initializeTelegramBot(testing: boolean = false): Promise<void> {

  bot = new Telegraf(config.apiKeys.telegram);

  // if (testing) {
  //   // we do this to find the channel id
  //   console.dir(await bot.telegram.getUpdates(1000, 5, 0, undefined), { depth: null });
  // }

  if (config.telegram.announceChannelId != '' && config.telegram.announceChannelId != null) {
    log.info("starting telegram bot..");    
    bot.start((ctx) => ctx.reply('Meow, talk prrrrrwy to me 😻'))
    await bot.launch();
  } else {
    bot = undefined;
  }

}

function formatBannerForTelegramMarkdown(banner: IBanner, includeImage: boolean = true): string {

  let imageUrl = `https://specops.quest/static/images/banner-${banner.guid}-tiny.jpg`;
  const escapeChars = [ '_', '*', '[', ']', '(', ')', '~', '`', '<', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!', '\\' ];
  const esc = (s: string) => {
    return s //markdownEscape(s)
      .replace(/&gt;/g, '>')
      .replace(/&lt;/g, '<')
      .split('')
      .map((ch: string) => {
        return (escapeChars.indexOf(ch) >= 0) ? "\\" + ch : ch
      })
      .join('')
      //.replace(/([_\*\[\]\(\)~`\+\-<>=\{\}{}&?#|!.])/g, "\\$1");  // '_', '*', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!' 
  }
  let title = esc(banner.title);
  let addr = esc(formatAddress(banner).slice(0, 2).join(" > "));
  let time = esc(formatTime(banner.time!));
  let distance = esc(formatDistance(banner.distance!));
  let link = esc("https://specops.quest/banner/" + (banner.slug || banner.guid));

  let text = (includeImage ? `\n[${title}](${imageUrl})` : `*${title}*`)
    + `\n\n🧩 ${banner.numMissions} missions\n🚩 ${banner.numWaypoints} waypoints\n🚶‍♂️ ${distance}\n⌚ ${time}\n🌎 ${addr}\n🔗 ${link}`;

  return text;

}

async function announceToTelegram(banner: IBanner): Promise<void> {

  if (!bot) return;

  let text = '';

  try {

    let imageUrl = banner.images?.medium;
    if (!imageUrl) return;

    imageUrl = 'https://specops.quest/static/images/' + imageUrl; // TODO: use http.publibalbla
    //config.http.publicImagesUrl
    
    text = formatBannerForTelegramMarkdown(banner, false);
    log.info("posting to telegram: %s", text);
    await bot.telegram.sendPhoto(config.telegram.announceChannelId!, imageUrl, { caption: text, parse_mode: 'MarkdownV2'});

  } catch (err) {

    log.error({ text }, "error posting to telegram: %s", err.message);

  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  SETUP
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

async function setupLogging(): Promise<void> {
  log = await initializeLogger(config, sessionFeatures.logBaseName, logsPath);
  loggers.api = log.child({ component: 'api' });
  loggers.auth = log.child({ component: 'auth' });
  loggers.googleAuth = log.child({ component: 'auth/google' });
  loggers.apikeyAuth = log.child({ component: 'auth/apikey' });
  log.info(sessionFeatures, "session features");
}

async function setup(): Promise<void> {
  if (!sessionFeatures) throw new Error("invalid session mode: " + process.argv[2]);
  await setupLogging();
  await initializeDb();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
////////  MAIN()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

(async () => {

  if (process.argv[2] == '--fix-mission-geojson') {

    sessionFeatures = sessionModes.none;
    await setup();

    log.info("fixing mission geojson data..")

    let curs = missionsCollection!.find({
      startPoint: null
    });

    log.info("found # missions = " + (await curs.count()));

    let count = 0;

    while (true) {

      let mission = await curs.next() as IMissionInfoDetailed;
      if (!mission) break;

      count++;
      if (count % 500 == 0) console.log(count);

      await updateMissionMetrics(mission);

      delete (<any>mission)._id;
      await missionsCollection!.updateOne({
        guid: mission.guid
      }, {
        $set: {
          ...mission // update all
        }
      })

      //process.exit(1);

    }

  } else if (process.argv[2] == '--backfill-fieldtrips') {

    sessionFeatures = sessionModes.none;
    await setup();

    log.info("finding missions with missing fieldtrip data..")

    let curs = missionsCollection!.find({
      // guid: "3bc8d49ba4dd40a8bb589a10118bae03.1c",
      waypoints: {
        $elemMatch: {
          type: 2,
          point: null,
          title: { $ne: "Unavailable" }
        }
      }
    });

    log.info("found # missions = " + (await curs.count()));

    while (true) {

      let mission = await curs.next() as IMissionInfoDetailed;
      if (!mission) break;

      log.info(`${mission.guid}: ${mission.title}`);

      let all = await backfillFieldtripWaypoints(mission);
      if (!all) log.error("COULD NOT BACKFILL ALL");
      
      await updateMissionMetrics(mission);

      delete (<any>mission)._id;
      await missionsCollection!.updateOne({
        guid: mission.guid
      }, {
        $set: {
          ...mission // update all
        }
      })

      //process.exit();

    }

  } else if (process.argv[2] == '--telegram-test') {

    sessionFeatures = sessionModes.none;
    await setup();

    log.info("telegram test..");
    let cursor = bannersCollection!.find({}).sort({ _id: -1 }).limit(1);
    let banner = (await cursor.toArray())[0];
    await initializeTelegramBot(true);
    await announceToTelegram(banner);

  } else if (process.argv[2] == '--update-banner-full') {

    sessionFeatures = sessionModes.none;
    await setup();

    await initializeQueuesAndProcessors();
    let banners: IBanner[] = process.argv[3]
      ? await bannersCollection!.find({ guid: process.argv[3] }).toArray()
      : await bannersCollection!.find({}).toArray();
    for (let i=0; banners.length>i; i++) {
      let b = banners[i];
      updateBannerQueue!.add('full:' + resolveObjectID(b._id).toHexString());
      // await preprocessBanner(b, false);
      // await bannersCollection!.updateOne({ guid: b.guid }, { $set: b });
    }
    log.info("DONE");
  }
  else if (process.argv[2] == '--update-banner-route') {

    sessionFeatures = sessionModes.none;
    await setup();

    await initializeQueuesAndProcessors();
    let banners: IBanner[] = process.argv[3]
      ? await bannersCollection!.find({ guid: process.argv[3] }).toArray()
      : await bannersCollection!.find({}).toArray();
    for (let i=0; banners.length>i; i++) {
      let b = banners[i];    
      updateBannerQueue!.add('route:' + resolveObjectID(b._id).toHexString());
      // await updateBannerMetrics(b);
      // await bannersCollection!.updateOne({ guid: b.guid }, { $set: b });
    }
    log.info("DONE");
  }
  else if (process.argv[2] == '--update-banner-metrics') {

    sessionFeatures = sessionModes.none;
    await setup();

    await initializeQueuesAndProcessors();
    let banners: IBanner[] = process.argv[3]
      ? await bannersCollection!.find({ guid: process.argv[3] }).toArray()
      : await bannersCollection!.find({}).toArray();
    for (let i=0; banners.length>i; i++) {
      let b = banners[i];    
      updateBannerQueue!.add('metrics:' + resolveObjectID(b._id).toHexString());
      // await updateBannerMetrics(b);
      // await bannersCollection!.updateOne({ guid: b.guid }, { $set: b });
    }
    log.info("DONE");
  }
  else if (process.argv[2] == '--update-mission-errors') {

    sessionFeatures = sessionModes.none;
    await setup();

    await initializeQueuesAndProcessors();
    let missions: MissionInfo[] = process.argv[3]
      ? await missionsCollection!.find({ guid: process.argv[3] }).toArray()
      : await missionsCollection!.find({
        waypoints: {
          $elemMatch: {
            'point.lat': { $gte: 180 }
          }
        }  
      }).project({ _id: true, guid: true }).toArray();
    log.info("matched " + missions.length)
    for (let i = 0; missions.length>i; i++) {
      let m = missions[i];    
      updateMissionQueue!.add('full:' + m.guid);
    }
    log.info("DONE");
  }
  else if (process.argv[2] == '--update-mission-data') {

    sessionFeatures = sessionModes.none;
    await setup();

    await initializeQueuesAndProcessors();
    let missions: MissionInfo[] = process.argv[3]
      ? await missionsCollection!.find({ guid: process.argv[3] }).toArray()
      : await missionsCollection!.find({}).project({ _id: true, guid: true }).toArray();
    for (let i = 0; missions.length>i; i++) {
      let m = missions[i];    
      updateMissionQueue!.add('full:' + m.guid);
    }
    log.info("DONE");
  }
  else if (process.argv[2] == '--update-mission-metrics') {

    sessionFeatures = sessionModes.none;
    await setup();

    await initializeQueuesAndProcessors();
    let missions: MissionInfo[] = await missionsCollection!.find({}).toArray();
    for (let i = 0; missions.length>i; i++) {
      let m = missions[i];    
      updateMissionQueue!.add('metrics:' + m.guid);
    }
    log.info("DONE");
  }
  else if (process.argv[2] == '--set-banner-slugs') {

    sessionFeatures = sessionModes.none;
    await setup();

    let all = await bannersCollection!.find({}).toArray() as IBanner[];
    for (let i = 0; all.length > i; i++) {
      let banner = all[i];
      console.log(`processing ${i+1}/${all.length}: ${banner.title} ..`);
      let s = makeBannerSlug(banner);
      console.log(s);
      await bannersCollection!.updateOne({
        guid: banner.guid
      },
      {
        $set: {
          slug: s
        }
      });
    }
    console.log("DONE");

  } else if (process.argv[2] == '--set-used-missions') {

    sessionFeatures = sessionModes.none;
    await setup();

    let all = await bannersCollection!.find({}).toArray() as IBanner[];
    for (let i = 0; all.length > i; i++) {
      let banner = all[i];
      console.log(`processing ${i+1}/${all.length}: ${banner.title} ..`);
      await removeUsedInReferences(banner);
      await addUsedInReferenced(banner);
    }
    console.log("DONE");

  } else if (process.argv[2] == '--fix-banner-images') {

    sessionFeatures = sessionModes.none;
    await setup();

    await initializeQueuesAndProcessors();
    log.info("UPDATING BANNER IMAGES");
    let banners: IBanner[] = process.argv[3]
      ? await bannersCollection!.find({ guid: process.argv[3], images: null }).toArray()
      : await bannersCollection!.find({ images: null }).limit(10).toArray();
    for (let i=0; banners.length>i; i++) {
      let b = banners[i];
      let id = resolveObjectID(b._id).toHexString();
      log.info("enqueue banner image refresh: " + id);
      generateBannerImageQueue!.add("update:"+id);
    }
    log.info("DONE");
  }
  else if (process.argv[2] == '--update-banner-images') {

    sessionFeatures = sessionModes.none;
    await setup();

    await initializeQueuesAndProcessors();
    log.info("UPDATING BANNER IMAGES");
    let banners: IBanner[] = process.argv[3]
      ? await bannersCollection!.find({ guid: process.argv[3] }).toArray()
      : await bannersCollection!.find({}).toArray();
    for (let i=0; banners.length>i; i++) {
      let b = banners[i];
      let id = resolveObjectID(b._id).toHexString();
      log.info("enqueue banner image refresh: " + id);
      generateBannerImageQueue!.add("update:"+id);
    }
    log.info("DONE");
  }
  else
  {
    sessionFeatures = sessionModes[process.argv[2]];
    await setup();

    log.info("STARTING NORMAL MODE")
    await initializeQueuesAndProcessors();
    await initializeExpress();
  }

})();


