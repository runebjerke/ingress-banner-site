import { filterValidWaypoint, filterValidWaypoints, IBanner } from '../../shared/src/types';
const { MongoClient, Collection, Db, ObjectID, MongoError } = require('mongodb');
const readline = require('readline');
import fetch from 'node-fetch';
const url = require('url');
const fs = require('fs');

const config = JSON.parse(fs.readFileSync('config.json').toString());

(async() => {

  let uri = 'mongodb://localhost/?retryWrites=true&writeConcern=majority';
  let mongoClient = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  await mongoClient.connect();
  let mongoDb = mongoClient.db('ingress-banners');
  let missionsCollection = mongoDb.collection('missions2');
  let bannersCollection = mongoDb.collection('banners');

  let bannerId = process.argv[2];
  let banner = await bannersCollection.findOne({ guid: bannerId }) as IBanner;
  if (banner) {
    let mission = banner.missions![0];
    console.dir(mission.waypoints);

    let u = url.parse("https://graphhopper.com/api/1/route");
    u.query = {
      vehicle: "foot",
      locale: "en",
      instructions: "true",
      calc_points: "true",
      points_encoded: "false",
      optimize: "false",
      key: config.apiKeys.graphhopper,
      point: filterValidWaypoints(mission).map(w => `${w.point.lat},${w.point.lng}`)
    }    
    let routeUrl = url.format(u);
    let res = await fetch(routeUrl)
    let data = await res.json() as IRouteResponse;
    
  }

  await mongoClient.close();

})();

