/*

new york
40.7055537,-74.0134436

geocoding: {
          place_id: 304309168,
          osm_type: 'way',
          osm_id: 907570434,
          type: 'platform',
          accuracy: 0,
          label: 'Bowling Green, State Street, Financial District, Manhattan Community Board 1, Manhattan, New York County, New York, 10275, United States',
          name: 'Bowling Green',
          country: 'United States',
          postcode: '10275',
          state: 'New York',
          city: 'New York',
          district: 'Manhattan',
          locality: 'Financial District',
          street: 'State Street',
          admin: {
            level4: 'New York',
            level5: 'New York',
            level6: 'New York County',
            level7: 'Manhattan',
            level8: 'Manhattan Community Board 1',
            level10: 'Financial District'
          }
        }



pakistan

31.4645834,74.268186

geocoding: {
          place_id: 218904241,
          osm_type: 'way',
          osm_id: 620739085,
          type: 'tertiary',
          accuracy: 0,
          label: 'Johar Town, Lahore District, Punjab, 54782, Pakistan',
          country: 'Pakistan',
          postcode: '54782',
          state: 'Punjab',
          county: 'Lahore District',
          city: 'Johar Town',
          locality: 'Johar Town',
          admin: { level3: 'Punjab' }
        }


59.9420143,10.7681074

geocoding: {
          place_id: 123068753,
          osm_type: 'way',
          osm_id: 146856850,
          type: 'parking',
          accuracy: 0,
          label: 'Sandakerveien 64, Lillogata, Sandaker, Sagene, Oslo, 0480, Norway',
          name: 'Sandakerveien 64',
          country: 'Norway',
          postcode: '0480',
          city: 'Oslo',
          district: 'Sagene',
          locality: 'Sandaker',
          street: 'Lillogata',
          admin: { level4: 'Oslo', level7: 'Oslo', level9: 'Sagene' }
        }
      },



tokyo:

35.5079383,139.2080462

geocoding: {
          place_id: 258827262,
          osm_type: 'relation',
          osm_id: 2689435,
          type: 'administrative',
          accuracy: 0,
          label: 'Kiyokawa, Aiko County, Kanagawa Prefecture, Japan',
          name: 'Kiyokawa',
          country: 'Japan',
          state: 'Kanagawa Prefecture',
          county: 'Aiko County',
          admin: {
            level4: 'Kanagawa Prefecture',
            level6: 'Aiko County',
            level7: 'Kiyokawa'
          }
        }

35.7094497,139.8074826

geocoding: {
          place_id: 119275108,
          osm_type: 'way',
          osm_id: 126859160,
          type: 'tertiary',
          accuracy: 0,
          label: 'Azumabashi 3-chome, Sumida, Tokyo, 〒130-0001, Japan',
          country: 'Japan',
          postcode: '〒130-0001',
          city: 'Tokyo',
          district: 'Sumida',
          locality: 'Azumabashi 3-chome',
          admin: { level4: 'Tokyo', level7: 'Sumida' }
        }

55.3976545,10.384252

geocoding: {
          place_id: 63657586,
          osm_type: 'node',
          osm_id: 5657228046,
          type: 'house',
          accuracy: 0,
          label: '11, Slotsgade, Odense, Odense Kommune, Region of Southern Denmark, 5000, Denmark',
          country: 'Denmark',
          postcode: '5000',
          state: 'Region of Southern Denmark',
          city: 'Odense',
          street: 'Slotsgade',
          housenumber: '11',
          admin: {
            level4: 'Region of Southern Denmark',
            level7: 'Odense Kommune'
          }
        }




*/


import fs = require('fs');
import fetch from 'node-fetch';
import url = require('url');
import querystring = require('querystring');

import { IGeoCodeJson, nominatimReverseLookupGeoCodeJson } from '../src/lib/nominatim';

const config: any = JSON.parse(fs.readFileSync('config.json').toString());
const latLng = process.argv[2]; // '40.7055537,-74.0134436';

(async () => {
    
  let [lat, lng] = latLng.split(/,/g).map(n => parseFloat(n));
  console.log(lat, lng)
  let res = await nominatimReverseLookupGeoCodeJson(lat, lng, { addressDetails: true });
  console.dir(res, { depth: null });

  // let res: IGeoCodeJson = {
  //   type: 'FeatureCollection',
  //   features: [
  //     {
  //       type: 'Feature',
  //       properties: {
  //         geocoding: {
  //           place_id: 304309168,
  //           osm_type: 'way',
  //           osm_id: 907570434,
  //           type: 'platform',
  //           accuracy: 0,
  //           label: 'Bowling Green, State Street, Financial District, Manhattan Community Board 1, Manhattan, New York County, New York, 10275, United States',
  //           name: 'Bowling Green',
  //           country: 'United States',
  //           postcode: '10275',
  //           state: 'New York',
  //           city: 'New York',
  //           district: 'Manhattan',
  //           locality: 'Financial District',
  //           street: 'State Street',
  //           admin: {
  //             level4: 'New York',
  //             level5: 'New York',
  //             level6: 'New York County',
  //             level7: 'Manhattan',
  //             level8: 'Manhattan Community Board 1',
  //             level10: 'Financial District'
  //           }
  //         }
  //       },        
  //       geometry: {
  //         type: 'Point',
  //         coordinates: [ -74.01384176297859, 40.704829000000004 ]
  //       }      
  //     }
  //   ]
  // };

  let feature = res?.features?.[0];
  if (feature) {
    let admin = feature?.properties?.geocoding?.admin;
    if (admin) {
      let components = [
        feature!.properties!.geocoding!.country!,
        admin.level1,
        admin.level2,
        admin.level3,
        admin.level4,
        admin.level5,
        admin.level6,
        admin.level7,
        admin.level8,
        admin.level9,
        admin.level10
      ]
      console.dir(components);
    }
  }

})();