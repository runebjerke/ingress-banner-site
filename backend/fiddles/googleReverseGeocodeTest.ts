
import fs = require('fs');
import fetch from 'node-fetch';
import url = require('url');
import querystring = require('querystring');

const config: any = JSON.parse(fs.readFileSync('config.json').toString());
const latLng = process.argv[2]; // '40.7055537,-74.0134436';



(async () => {
    
  let u = url.parse('https://maps.googleapis.com/maps/api/geocode/json');
  u.search = querystring.encode({
    latlng: latLng,
    language: 'en',
    key: config.apiKeys.googleGeocoding
    //result_type: 'country|administrative_area_level_1|administrative_area_level_2|administrative_area_level_3|administrative_area_level_4|administrative_area_level_5'
  });
  let qu = url.format(u);

  console.log(qu);

  let res = await (await fetch(qu)).json();

  fs.writeFileSync('geotest-' + Date.now() + '.json', JSON.stringify(res));

  let ok = res.results[0].address_components.reverse().filter((x: any) => {
    let types = x.types.filter((t: string) => /^administrative_area_level_\d+|country|locality|sublocality|neighborhood$/.test(t))
    return types.length > 0;
  });
  
  console.dir(ok);  

})();

/*

let res: any = {
  plus_code: {
    compound_code: 'PX4P+6J New York, NY, USA',
    global_code: '87G7PX4P+6J'
  },
  results: [
    {
      address_components: [
        {
          long_name: 'New York County',
          short_name: 'New York County',
          types: [ 'administrative_area_level_2', 'political' ]
        },
        {
          long_name: 'Manhattan',
          short_name: 'Manhattan',
          types: [ 'political', 'sublocality', 'sublocality_level_1' ]
        },
        {
          long_name: 'New York',
          short_name: 'New York',
          types: [ 'locality', 'political' ]
        },
        {
          long_name: 'New York',
          short_name: 'NY',
          types: [ 'administrative_area_level_1', 'political' ]
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [ 'country', 'political' ]
        }
      ],
      formatted_address: 'New York County, New York, NY, USA',
      geometry: {
        bounds: {
          northeast: { lat: 40.882214, lng: -73.907 },
          southwest: { lat: 40.6803955, lng: -74.047285 }
        },
        location: { lat: 40.7830603, lng: -73.9712488 },
        location_type: 'APPROXIMATE',
        viewport: {
          northeast: { lat: 40.882214, lng: -73.907 },
          southwest: { lat: 40.6803955, lng: -74.047285 }
        }
      },
      place_id: 'ChIJOwE7_GTtwokRFq0uOwLSE9g',
      types: [ 'administrative_area_level_2', 'political' ]
    },
    {
      address_components: [
        {
          long_name: 'New York',
          short_name: 'NY',
          types: [ 'administrative_area_level_1', 'political' ]
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [ 'country', 'political' ]
        }
      ],
      formatted_address: 'New York, USA',
      geometry: {
        bounds: {
          northeast: { lat: 45.015861, lng: -71.777491 },
          southwest: { lat: 40.4773991, lng: -79.7625901 }
        },
        location: { lat: 43.2994285, lng: -74.21793260000001 },
        location_type: 'APPROXIMATE',
        viewport: {
          northeast: { lat: 45.015861, lng: -71.777491 },
          southwest: { lat: 40.4773991, lng: -79.7625901 }
        }
      },
      place_id: 'ChIJqaUj8fBLzEwRZ5UY3sHGz90',
      types: [ 'administrative_area_level_1', 'political' ]
    },
    {
      address_components: [
        {
          long_name: 'United States',
          short_name: 'US',
          types: [ 'country', 'political' ]
        }
      ],
      formatted_address: 'United States',
      geometry: {
        bounds: {
          northeast: { lat: 71.5388001, lng: -66.885417 },
          southwest: { lat: 18.7763, lng: 170.5957 }
        },
        location: { lat: 37.09024, lng: -95.712891 },
        location_type: 'APPROXIMATE',
        viewport: {
          northeast: { lat: 71.5388001, lng: -66.885417 },
          southwest: { lat: 18.7763, lng: 170.5957 }
        }
      },
      place_id: 'ChIJCzYy5IS16lQRQrfeQ5K5Oxw',
      types: [ 'country', 'political' ]
    }
  ],
  status: 'OK'
};

*/