const { MongoClient } = require('mongodb');
const LBL = require('line-by-line');

(async() => {

  let uri = 'mongodb://localhost/?retryWrites=true&writeConcern=majority';
  let mongoClient = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  await mongoClient.connect();
  let mongoDb = mongoClient.db('other');
  let coll = mongoDb.collection('fieldtrips');

  await coll.deleteMany({});

  let ln = 0;
  let reader = new LBL('c:/dl/o/missions.json');

  reader.on('line', async line => {
    reader.pause();
    ln++;

    try {
      let data = JSON.parse(line);
      let fieldtrips = data.waypoints.filter(w => w.objective == 7);
      for (let i=0; fieldtrips.length>i; i++) {
        let f = fieldtrips[i];
        let ff = { guid: f.pguid, title: f.name };
        if (f.location) {
          ff.lat = f.location.coordinates[1];
          ff.lng = f.location.coordinates[0];
        }
        console.dir(ff);
        await coll.updateOne({ guid: ff.guid }, { $setOnInsert: ff }, { upsert: true })
        //await coll.insertOne(ff);
      }       
    } catch (err) {
      console.log("ERROR: " + err);
      console.error(line);
    }

    reader.resume();
  })

  reader.on('end', () => {

  })


  await reader.read(json => {
    return new Promise(async (resolve, reject) => {
      ln++;
      let b = Buffer.from(json.split('').map(s => s.charCodeAt(0)));
      let json2 = decoder.write(b);
      try {
        let data = JSON.parse(json2);
        delete data._id;
        delete data.updated;
        await coll.insertOne(data);
        console.log(ln, data.name);
      } catch (err) {
        console.log("ERROR: " + err);
        console.error(json2);
      } finally {
        resolve();
      }
    });
  }, async totalObjects => {
    console.log("totalObjects", totalObjects);
    await mongoClient.close();
  });


})();

