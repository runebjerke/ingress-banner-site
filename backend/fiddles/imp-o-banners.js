const { MongoClient, Collection, Db, ObjectID, MongoError } = require('mongodb');
const readline = require('readline');
const fs = require('fs');
const JsonReader  = require('big-json-reader');
const { StringDecoder } = require('string_decoder');

const decoder = new StringDecoder('utf8');

(async() => {

  let uri = 'mongodb://localhost/?retryWrites=true&writeConcern=majority';
  let mongoClient = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  await mongoClient.connect();
  let mongoDb = mongoClient.db('other');
  let missionsCollection = mongoDb.collection('missions');
  let bannersCollection = mongoDb.collection('banners');

  await bannersCollection.deleteMany({});

  let ln = 0;
  let reader = new JsonReader(require.resolve('e:/dump/banners.json'));
  await reader.read(json => {
    return new Promise(async (resolve, reject) => {
      ln++;
      let b = Buffer.from(json.split('').map(s => s.charCodeAt(0)));
      let json2 = decoder.write(b);
      let data = JSON.parse(json2);
      delete data._id;
      delete data.updated;
      await bannersCollection.insertOne(data);
      console.log(ln, data.name);
      resolve();
    });
  }, async totalObjects => {
    console.log("totalObjects", totalObjects);
    await mongoClient.close();
  });


})();

