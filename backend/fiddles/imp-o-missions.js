const { MongoClient, Collection, Db, ObjectID, MongoError } = require('mongodb');
const readline = require('readline');
const fs = require('fs');
const JsonReader  = require('big-json-reader');
const { StringDecoder } = require('string_decoder');

const LBL = require('line-by-line');

(async() => {

  let uri = 'mongodb://localhost/?retryWrites=true&writeConcern=majority';
  let mongoClient = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  await mongoClient.connect();
  let mongoDb = mongoClient.db('other');
  let missionsCollection = mongoDb.collection('missions');

  await missionsCollection.deleteMany({});

  let ln = 0;
  let reader = new LBL('e:/dump/missions.json');

  reader.on('line', async line => {
    reader.pause();
    ln++;

    try {
      let data = JSON.parse(line);
      delete data._id;
      delete data.updated;
      await missionsCollection.insertOne(data);
      console.log(ln, data.name);
    } catch (err) {
      console.log("ERROR: " + err);
      console.error(line);
    }

    reader.resume();
  })

  reader.on('end', () => {

  })


  await reader.read(json => {
    return new Promise(async (resolve, reject) => {
      ln++;
      let b = Buffer.from(json.split('').map(s => s.charCodeAt(0)));
      let json2 = decoder.write(b);
      try {
        let data = JSON.parse(json2);
        delete data._id;
        delete data.updated;
        await missionsCollection.insertOne(data);
        console.log(ln, data.name);
      } catch (err) {
        console.log("ERROR: " + err);
        console.error(json2);
      } finally {
        resolve();
      }
    });
  }, async totalObjects => {
    console.log("totalObjects", totalObjects);
    await mongoClient.close();
  });


})();

