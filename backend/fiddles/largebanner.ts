import { drawBanner, downloadBannerImages, findMissingImages, loadBannerImageManifest, IMissionImageManifest, writeCanvasToJpegFile } from '../src/lib/makeBannerImage';
const fs = require('fs');
import { IBanner } from '../../shared/src/types';

let banner = JSON.parse(fs.readFileSync('c:/dl/test.json').toString()) as IBanner ;

let imagesPath = "temp/images/banner/" + banner.guid;

loadBannerImageManifest(imagesPath)
//downloadBannerImages(banner.missions!, imagesPath)
  .then(async (manifest: IMissionImageManifest) => {
    let missingIndices = findMissingImages(manifest);
    if (missingIndices.length > 0) {
      console.warn("WARNING! missing indices:", missingIndices);
    }

    let im = await drawBanner(banner.missions!, {
      circle: true,
      outline: 2,
      rowSize: 6,
      tileSize: 24,
      padding: 2,
      limitWidthToRowSize: false,
      imageManifest: manifest,
      imageManifestDataPath: imagesPath
    })

    await writeCanvasToJpegFile(im, 'banner.jpg', 75);

  })
  .catch(err => {

  })
