// ==UserScript==
// @id             iitc-plugin-specops.quest-helper-dev-loader 
// @name           IITC plugin: specops.quest helper (dev build loader)
// @category       Misc
// @version        0.2.0
// @match          https://intel.ingress.com/*
// @require        file:///C:/source/js/ingress-banner-site/iitc-plugin/dist/iitc_plugin_ingressbannerhelper.dev.user.js
// @grant none
// ==/UserScript==