// this is because this lib is included from the frontend also, and it needs to resolve the IITC.PortalData type
/// <reference path="../node_modules/iitcpluginkit/src/types/iitc/types.d.ts" />

export module MissionsPlugin {

  export interface IMissionSummary {
    guid: string;
    title: string;
    image: string;
    ratingE6?: string;
    medianCompletionTimeMs: string;
  }

  export interface IMissionDetails extends IMissionSummary {
    description: string;
    authorNickname: string;
    authorTeam: 'E' | 'R' | 'N';
    numUniqueCompletedPlayers: number | string;
    typeNum: number;
    type: string | null;
    waypoints: Array<IMissionWaypoint>;
  }

  export interface IMissionWaypoint {
    hidden: boolean;
    lat?: number;
    lng?: number;
    guid: string;
    title: string;
    typeNum: number;
    type?: string | null;
    objectiveNum: number;
    objective?: string | null;
    portal?: IITC.PortalData;
  }

}