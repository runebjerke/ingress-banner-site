declare const BUILD_TYPE: string;

import * as Plugin from "iitcpluginkit";

import { MissionsPlugin } from './missionsPlugin';

const Vue = require("vue/dist/vue");

const SETTINGS_PREFIX = "ingressbannerhelper";

import {
  IMissionWaypointEx,
  IMissionInfoSummary,
  ILatLngBounds,
  ILatLng,
  IMissionInfoDetailed,
  ISearchResults,
  MissionInfo,
} from '../../shared/src/types';

import { stripGoogleImageParams } from '../../shared/src/googleImageParams';

const missionsListHtml: string = require('./missions-list.html').default;

// PROBLEM: window.plugin.missions does not store the lat/lng of fieldtrip waypoints in decodeWaypoint()

interface IMissionsListDialogEntry {
  guid: string;
  elem: JQuery<HTMLElement>;
  status: string;
  info?: IMissionInfoDetailed;
}

class IngressBannerHelperPlugin implements Plugin.Class {

  // baseUrl: string = BUILD_TYPE == "prod" 
  //   ? "https://specops.quest/api/"
  //   : 
    baseUrl: string = "http://localhost:8421/api/";

    apiKey: string = '';

  selectedPortalGuid: string | null = null;

  // suppressHooks: number = 0;
  suppressHooks: boolean = false;

  endSuppressHooks() {
    setTimeout(() => {
      this.suppressHooks = false;
    }, 500);
  }

  get missionsPlugin(): MissionsPlugin.IPlugin {
    let p: MissionsPlugin.IPlugin = window.plugin.missions;
    if (p != null) {
      p = {
        ...p,
        loadMission: MissionsPlugin.loadMission // override with patched decode
      }
    }
    return p;
  }

  statusTexts: any = {
    known: {
      text: '📙 Indexed',
      title: 'Indexed - click to refresh'
    },
    'known-locked': {
      text: '🔒 Indexed',
      title: 'Recently indexed (locked)',
      locked: true
    },
    pending: {
      text: '⏳ Updating',
      title: "Please wait..",
      locked: true
    },
    unknown: {
      text: '✨ New',
      title: 'Not indexed - click to refresh'
    }
  }

  currentDialog: Array<IMissionsListDialogEntry> = [];

  private iu: string;

  async waitForMissionsPlugin(): Promise<MissionsPlugin.IPlugin> {
    return new Promise<MissionsPlugin.IPlugin>((resolve, reject) => {
      let pollForPluginInterval: NodeJS.Timeout | null = null;
      pollForPluginInterval = setInterval(() => {
        if (window.plugin.missions) {
          clearInterval(pollForPluginInterval!);
          console.log("MISSIONS PLUGIN READY");
          resolve(window.plugin.missions);
        } else {
          //console.log("NOT READY");
        }
      }, 250);
    })
  }

  async init() {
    console.log("specops.quest banner helper " + VERSION + "/" + BUILD_TYPE);

    require("./styles.css");
    require("./missions-list.css");

    this.iu = btoa(PLAYER.nickname + ':' + PLAYER.team);

    this.apiKey = localStorage.getItem(SETTINGS_PREFIX + ".apikey") as string;
    if (this.apiKey == "null") this.apiKey = null!;

    //console.log("loading vue script..");
    //await $.getScript("https://unpkg.com/vue@2.6.12/dist/vue.js");

    $("#toolbox").append(
      $("<a>", {
        text: "🚩specops.quest",
        click: () => this.showDialog()
      })
    );
        
    window.addHook('iitcLoaded', () => {
      console.log("IITC loaded!");
    });

    this.waitForMissionsPlugin().then(mp => {

      // set up for capturing dialogs, so that we can find the dialog created by window.plugin.missions.showMissionListDialog()
      let captureDialog = false;
      let capturedDialog: any = null;

      // save original window.dialog() and install our hook
      let _dialog = window.dialog;
      window.dialog = function() {
          //console.log("INTERCEPT DIALOG", arguments);
          let dlg = _dialog.apply(window, arguments);
          if (captureDialog) capturedDialog = dlg;
          //console.log("DIALOG -> ", dlg);
          return dlg;
      }

      // save original window.plugin.missions.showMissionListDialog() and install our hook
      let _showMissionListDialog = window.plugin.missions.showMissionListDialog;
      let me = this;
      window.plugin.missions.showMissionListDialog = (missions: Array<MissionsPlugin.IMissionSummary>) => {
        (() => {

          console.log("INTERCEPT MISSIONS:", missions);

          let bounds = window.map.getBounds();
          let point = window.map.getCenter();
          let b: ILatLngBounds = {
            ne: { lat: bounds.getNorth(), lng: bounds.getEast() },
            sw: { lat: bounds.getSouth(), lng: bounds.getWest() }
          }
          let p: ILatLng = {
            lat: point.lat,
            lng: point.lng
          }

          // set up for capturing the dialog
          captureDialog = true;
          capturedDialog = null;

          // call the original window.plugin.missions.showMissionListDialog()
          let res = _showMissionListDialog.call(mp, missions);

          // stop capturing and check if we found the dialog
          captureDialog = false;
          if (capturedDialog != null) {
              // we found the dialog, so we can go ahead and patch it!

              console.log("DIALOG", capturedDialog);

              // submit full data and get results back
              let promise = this.store(missions.map(m => this.convertMissionSummary(m, undefined, b)));

              // // submit list of guids to backend to check which ones we know about
              // let checkUrl = `${this.baseUrl}missions/guids/`
              //   + missions.map(function(m) { return m.guid }).join(",")
              //   + `?apiKey=${encodeURIComponent(this.apiKey)}`
              //   + `&iu=${encodeURIComponent(this.iu)}`
              //   ;
              // console.debug(checkUrl);
              // let promise = $.ajax({
              //     type: 'GET',
              //     contentType: "application/json; charset=utf-8",
              //     dataType: "json",
              //     url: checkUrl
              // });
              
              promise.then((resp: ISearchResults<IMissionInfoDetailed>) => {

                  console.log("STORED MISSIONS << %o", resp);

                  this.currentDialog = [];

                  // for each entry in the dialog, add our status
                  capturedDialog.find(".plugin-mission-summary").each((index: string, value: HTMLElement) => {
                      let mid = value.getAttribute("data-mission_mid");
                      let el = $(value);
                      let knownMission = resp.results.find(m => m.guid == mid);

                      // let span = el.append(
                      //   `<span
                      //     class="plugin-mission-info help specopsquest-mission-status"
                      //     title="placeholder">placeholder</span>`).find("span.specopsquest-mission-status");

                      this.currentDialog.push({
                        elem: null!, //span,
                        guid: mid!,
                        status: '',
                        info: knownMission
                      })
                  });

                  this.updateDialog();

              }).catch(err => {

                  console.error("ERROR QUERYING BACKEND", err);

              });
          }

          // return the result of the original showMissionListDialog() call (not that there's suppsoed to be any)
          return res;
                    
        }).call(me);

      }

      let _renderMissionSummary: Function = window.plugin.missions.renderMissionSummary;
      window.plugin.missions.renderMissionSummary = (mission: MissionsPlugin.IMissionSummary) => {
        let el = _renderMissionSummary.call(window.plugin.missions, mission);
        console.log("RE-RENDER", mission, el);
        this.updateDialog(mission?.guid, el);
        return el;
      };
      
    });

  }

  updateDialog(singleGuid?: string, newEl?: HTMLElement) {

    console.log("UPDATE DIALOG ", singleGuid);

    this.currentDialog.forEach(m => {

      console.log("UPDATE DIALOG GUID ", m.guid);
      if (singleGuid != null && m.guid != singleGuid) return;

      m.status = m.info && (m.info.waypoints || m.info.detailsModified) ? "known" : "unknown";
      let statusText: any = this.statusTexts[m.status];

      let span = $(`<span
      class="plugin-mission-info help specopsquest-mission-status specopsquest-mission-status-${m.status}"
      title="SpecOps.quest: ${ statusText.title }">${ statusText.text }</span>`);

      if (newEl) {
        $(newEl).append(span);
        m.elem = span;
      } else {
        if (m.elem == null) {
          let el = $(".plugin-mission-summary [data-mission_mid='"+m.guid+"']");
          el.append(span);
          m.elem = span;
        } else {
          m.elem.replaceWith(span);
          m.elem = span;
        }
      }

      if (statusText.locked !== false) {
        span.click(ev => {
          m.status = "pending";
          this.updateDialog();

          console.log("loading mission..");
          this.missionsPlugin.loadMission(m.guid, ms => {
            console.log("loaded mission:", ms);
            m.info = this.convertMissionDetails(ms);
            this.store(m.info);
            m.status = "known";
            this.updateDialog();
          }, err => {
            console.error("ERROR UPDATING MiSSION: " + m.guid + ": ", err);
            m.status = "unknown";
            this.updateDialog();
          })
        })

      }

    })
  }

  loadMission(guid: string): Promise<IMissionInfoDetailed> {
    return new Promise<IMissionInfoDetailed>((resolve, reject) => {

      const mp = this.missionsPlugin;

      if (mp == null) {
        reject(new Error("missions plugin not loaded"));
        return;
      }

      this.suppressHooks = true;
      mp.loadMission(guid, mission => {

        this.endSuppressHooks();

        console.log("RAW MISSION", mission);
        let m = this.convertMissionDetails(mission);

        let cur = this.currentDialog.find(m => m.guid == guid);
        if (cur != null) {
          console.log("UPDATED CURRENT INFO", cur);
          cur.info = m;
          cur.status = "known";
          this.updateDialog();
        }

        resolve(m);

      }, err => {

        this.endSuppressHooks();
        reject(new Error(err));

      })
    });
  }

  private store(data: IMissionInfoDetailed | IMissionInfoSummary | IMissionInfoSummary[]): Promise<ISearchResults<IMissionInfoDetailed>> {
    return new Promise((resolve, reject) => {
      console.log("STORE MISSIONS >> %o", data);
      $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: `${this.baseUrl}missions?apiKey=${encodeURIComponent(this.apiKey)}&iu=${encodeURIComponent(this.iu)}`,
        data: JSON.stringify(data)
      }).then((resp: ISearchResults<IMissionInfoDetailed>) => { // highest form is detailed, dont need union
        console.log("STORED MISSIONS << %o", resp);
        resolve(resp);
      }).catch(err => {
        reject(err);
      })
    });
  }

  private convertWaypoint(w: MissionsPlugin.IMissionWaypoint): IMissionWaypointEx {
    let p = window.portals[w.guid];
    let data = w.portal ? w.portal : p?.options?.data;
    const w2: IMissionWaypointEx = {
      guid: w.guid,
      hidden: w.hidden,
      objective: w.objectiveNum,
      title: w.title,
      type: w.typeNum,
      point: null!
    }
    if (data) { // not available for removed portals ("Unavailable")
      w2.point = {
        latE6: data.latE6,
        lngE6: data.lngE6,
        lat: data.latE6 / 1E6,
        lng: data.lngE6 / 1E6
      }
    }
    return w2;
  }

  private convertMissionDetails(mission: MissionsPlugin.IMissionDetails, portalGuid?: string): IMissionInfoDetailed {
    const missionEx: IMissionInfoDetailed = {
      author: {
        nickname: mission.authorNickname,
        team: mission.authorTeam
      },
      description: mission.description,
      guid: mission.guid,
      image: mission.image,
      time: parseInt(<string>mission.medianCompletionTimeMs),
      numCompleted: parseInt(<string>mission.numUniqueCompletedPlayers),
      portalGuid: portalGuid == undefined ? mission.waypoints[0].guid : portalGuid,
      rating: parseInt(<string>mission.ratingE6) / 1E4,
      title: mission.title,
      type: mission.typeNum,
      waypoints: mission.waypoints.map(this.convertWaypoint),
      numWaypoints: mission.waypoints.length
    }
    return missionEx;
  }

  private convertMissionSummary(mission: MissionsPlugin.IMissionSummary, portalGuid?: string, location?: ILatLng | ILatLngBounds): IMissionInfoSummary {
    const missionEx: IMissionInfoSummary = {
      guid: mission.guid,
      image: mission.image,
      time: parseInt(<string>mission.medianCompletionTimeMs),
      portalGuid: portalGuid,
      rating: parseInt(<string>mission.ratingE6) / 1E4,
      title: mission.title
    }
    if (location != null) {
      if ((location as ILatLngBounds).ne != null) {
        missionEx.locationBounds = <ILatLngBounds>location;
        missionEx.locationPoint = {
          lat: (missionEx.locationBounds.ne.lat + missionEx.locationBounds.sw.lat) / 2,
          lng: (missionEx.locationBounds.ne.lng + missionEx.locationBounds.sw.lng) / 2
        };
      } else {
        missionEx.locationPoint = <ILatLng>location;
      }
    }
    return missionEx;
  }

  makeResults(missions: MissionInfo[]): ISearchResults<MissionInfo> {
    return {
      count: missions.length,
      page: 1,
      pageSize: missions.length,
      numPages: 1,
      results: missions
    }
  }

  async showDialog(results?: ISearchResults<MissionInfo>) {

    let app: any = null;
    let initialized = false;
    let plugin = this;

    let dlg = window.dialog({

      title: '::[specops.quest]::',
      id: 'ingress-banner-helper-dialog-outer',
      html: `<div id="ingress-banner-helper-dialog"></div>`,
      width: 550,
      height: Math.round(window.innerHeight * 0.75),

      focusCallback: () => {
        if (!initialized) {
          initialized = true;

          let v: any = Vue; // (<any>window).Vue;
          app = new v({

            el: '#ingress-banner-helper-dialog',

            template: missionsListHtml,

            data() {
              return {
                haveApiKey: plugin.apiKey != "" && plugin.apiKey != null,
                bounded: true,
                favorites: false,
                title: '',
                currentlyUpdatingGuid: null as string | null,
                results: results as ISearchResults<MissionInfo> | null,                
                prevQs: null as string[] | null
              }
            },

            computed: {
              filteredMissions() {
                let vm = <any>this;
                return vm.results;
                // let f = vm.titleFilter.toLowerCase().trim();
                // return vm.missions.filter((m: IMissionDetailsEx) => {
                //   let t = m.title.toLowerCase();
                //   return f == "" || t.indexOf(f) >= 0;
                // })
              }
            },

            methods: {

              toggleFav(m: MissionInfo, state: boolean) {
                let vm = <any>this;
                if (vm.haveApiKey) {
                  $.ajax({
                    type: state ? 'POST' : 'DELETE',
                    contentType: "application/json; charset=utf-8",
                    url: `${plugin.baseUrl}missions/${encodeURIComponent(m.guid)}/favorite?apiKey=${encodeURIComponent(plugin.apiKey)}&iu=${encodeURIComponent(plugin.iu)}`
                  }).then(() => {
                    console.log("updated favorite status");
                    m.userData = {
                      ...m.userData,
                      favoritedBy: state ? [ "dummy_id" ] : []
                    };
                  }).catch(err => {
                    alert("Error setting favorite: " + err);
                  })                  
                } else {
                  alert("You must set up your API key to use favorites!");
                }
              },

              processImage(url: string) {
                let u = stripGoogleImageParams(url, "=w40");
                return u;
              },

              show(guid: string) {
                plugin.missionsPlugin.openMission(guid);
              },

              checkKey(key: any) {
                if (key.code == 'Enter' || key.key == "Enter") this.search();
              },

              search() {

                let vm = <any>this;

                let qs = [];
                
                if (vm.bounded) {
                  let bounds = window.map.getBounds();
                  let nesw = [bounds.getNorth(), bounds.getEast(), bounds.getSouth(), bounds.getWest()];
                  qs.push("nesw=" + nesw.join(","));
                }

                if (vm.favorites) {
                  qs.push("requireFavorite=1");
                }

                if (vm.title.trim().length) {
                  qs.push("title=" + encodeURIComponent(vm.title.trim()));
                }

                qs.push("apiKey=" + encodeURIComponent(plugin.apiKey));
                qs.push("iu=" + encodeURIComponent(plugin.iu));

                vm.prevQs = qs;
                vm.nextSearch(1);
              },

              async nextSearch(page: number) {

                let vm = <any>this;
                let results: ISearchResults<MissionInfo> = await $.ajax({
                  method: 'GET',
                  url: `${plugin.baseUrl}missions/search?t=summary&page=${page || 1}&` + vm.prevQs.join("&")
                })
                vm.results = results;

              },

              go(rel: number) {
                let vm = <any>this;
                this.nextSearch(
                  Math.min(Math.max(vm.results.page + rel, 1), vm.results.numPages)
                );
              },

              async getMissionDetails(guid: string) {
                let vm = <any>this;
                try {
                  vm.currentlyUpdatingGuid = guid;
                  console.log("querying ingress API..");
                  let mission = await plugin.loadMission(guid);
                  console.log("posting to backend API..");
                  let missionInfo = (await plugin.store(mission)).results[0];
                  console.log("done!");
                  let replaced = false;
                  vm.results.results = vm.results.results.map((m: IMissionInfoSummary) => {
                    if (guid == m.guid) {
                      replaced = true;
                      return missionInfo;
                    }
                    else return m;
                  })
                  if (!replaced) vm.results.results.unshift(missionInfo);
                } finally {
                  vm.currentlyUpdatingGuid = null;
                }
              },

              setMissions(missions: IMissionInfoSummary[]) {
                (<any>this).missions = missions.sort((a, b) => a.title.localeCompare(b.title));
              },

              formatTime(t: number) {
                t /= 1000;
                let d = Math.floor(t / 86400);
                t %= 86400;
                let h = Math.floor(t / 3600);
                t %= 3600;
                let m = Math.floor(t / 60);
                t %= 60;
                let s = Math.round(t);
                return [
                  d ? d + 'd' : null,
                  h ? h + 'h' : null,
                  m ? m + 'm' : null,
                  s ? s + 's' : null
                ].filter(s => s).join(" ")
              }

            }
          });

        }
      },
      closeCallback: () => {
      }
    });

    dlg.dialog('option', 'buttons', [
      {      
        text: 'Edit API key',
        class: 'ingress-banner-helper-dialog-button',
        click: () => {
          let ak = prompt("Please enter your API key", plugin.apiKey || "");
          if (ak != null) {
            ak = ak.trim();
            if (ak != "") {
              // test API key and at the same time verify name via IITC
              $.get(`${plugin.baseUrl}profile/api-key/test?apiKey=`
                + encodeURIComponent(ak.trim())
                + "&iu=" + encodeURIComponent(this.iu)
              )
              .then(() => {
                plugin.apiKey = ak!;
                localStorage.setItem(SETTINGS_PREFIX + ".apikey", plugin.apiKey);
                app.haveApiKey = plugin.apiKey != "" && plugin.apiKey != null && plugin.apiKey != "null";
                alert("API key is ok!");
              })
              .catch(err => {
                console.log(err);
                alert("That doesn't appear to be working.....");
              });
            } else {
              plugin.apiKey = null!;
              localStorage.setItem(SETTINGS_PREFIX + ".apikey", plugin.apiKey);
              app.haveApiKey = false;
              alert("API key cleared!");
            }
          }
        },
      },
      {
        text: 'Open missions in view',
        class: 'ingress-banner-helper-dialog-button',
        click: () => {
          window.plugin.missions.openTopMissions();
        },
      },
      {
        text: 'Close',
        class: 'ingress-banner-helper-dialog-button',
        click: () => { dlg.dialog('close'); },
      }
    ]);
  }

}

Plugin.Register(new IngressBannerHelperPlugin(), "specopsQuestBannerHelper");
