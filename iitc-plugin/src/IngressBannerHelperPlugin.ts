declare const BUILD_TYPE: string;

import * as Plugin from "iitcpluginkit";

import { MissionsPlugin } from './missionsPlugin';

const Vue = require("vue/dist/vue");

const SETTINGS_PREFIX = "ingressbannerhelper";

import {
  IMissionWaypointEx,
  IMissionInfoSummary,
  ILatLngBounds,
  ILatLng,
  IMissionInfoDetailed,
  ISearchResults,
  MissionInfo,
  // WaypointObjective,
  // WaypointType,
  // MissionOrder,
  // WaypointObjectiveTexts
} from '../../shared/src/types';

type Funfun =  (arg: any, cb: Function, cberr: Function) => void;

import { stripGoogleImageParams } from '../../shared/src/googleImageParams';

const missionsListHtml: string = require('./missions-list.html').default;

// PROBLEM: window.plugin.missions does not store the lat/lng of fieldtrip waypoints in decodeWaypoint()

class IngressBannerHelperPlugin implements Plugin.Class {

  baseUrl: string = BUILD_TYPE == "dev" 
    ? "http://localhost:8420/api/"
    : "https://specops.quest/api/";

    apiKey: string = '';

  selectedPortalGuid: string | null = null;

  // suppressHooks: number = 0;
  suppressHooks: boolean = false;

  endSuppressHooks() {
    setTimeout(() => {
      this.suppressHooks = false;
    }, 500);
  }

  get missionsPlugin(): MissionsPlugin.IPlugin {

    // const addWrapper = (target: Funfun): Funfun => {
    //   return (arg: any, cb: Function, cberr: Function) => {
    //     this.suppressHooks++;
    //     target(
    //       arg,
    //       (res: any) => { this.suppressHooks--; cb(res); },
    //       (err: any) => { this.suppressHooks--; cberr(err); }
    //     );
    //   }
    // }

    let p: MissionsPlugin.IPlugin = window.plugin.missions;
    if (p != null) {
      p = {
        ...p,
        loadMission: MissionsPlugin.loadMission // override with patched decode
      }
    }
    return p;
  }

  private iu: string;

  async init() {
    console.log("ingressbannerhelper " + VERSION + " " + BUILD_TYPE);
    require("./styles.css");
    require("./missions-list.css");

    this.iu = btoa(PLAYER.nickname + ':' + PLAYER.team);

    this.apiKey = localStorage.getItem(SETTINGS_PREFIX + ".apikey") as string;
    if (this.apiKey == "null") this.apiKey = null!;

    //console.log("loading vue script..");
    //await $.getScript("https://unpkg.com/vue@2.6.12/dist/vue.js");

    //await $.getScript("https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.common.js");
    //await $.getScript("https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js");
    // await $.getScript("https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.min.js");
    //await $.getScript("https://cdn.jsdelivr.net/npm/vue-template-compiler@2.6.12/browser.js");

    $("#toolbox").append(
      $("<a>", {
        text: "🚩specops.quest",
        click: () => this.showMissionsDialog()
      })
    );
        
    // $("#toolbox").append(
    //   $("<a>", {
    //     text: "✅KnownMissions",
    //     click: () => this.queryMissionsInView()
    //   })
    // );

    // $("#toolbox").append(
    //   $("<a>", {
    //     text: "🚩TopMissionsHere",
    //     click: () => this.getMissionsInView()
    //   })
    // );

    window.addHook("portalDetailsUpdated", e => {
      if (e.portal && (e.portal.options.data.mission || e.portal.options.data.mission50plus)) {
        this.selectedPortalGuid = e.guid;
        console.log("---> add missions tool link", e.guid);
        // TODO: there should be a <aside> around this
        $("#portaldetails .linkdetails").append(
          $("<a>", {
            text: "🚩Missions",
            click: () => this.getMissionsForSelected()
          })
        );
      }
    });

    // window.addHook('iitcLoaded', () => {

    //   console.log("iitc loaded hook");
    
    let pollForPluginInterval: NodeJS.Timeout | null = null;
    pollForPluginInterval = setInterval(() => {
      if (window.plugin.missions) {

        clearInterval(pollForPluginInterval!);
        console.log("MISSIONS PLUGIN READY");

        // INJECT..
        const oldHandler = window.plugin.missions.loadMissionsInBounds;
        window.plugin.missions.loadMissionsInBounds = function(bounds: L.LatLngBounds, cb: Function, cbErr: Function) {
          oldHandler(bounds, (missions: MissionsPlugin.IMissionSummary[]) => {
            cb(missions);
            window.runHooks('plugin-missions-in-view-loaded', { missions, bounds });
          }, cbErr);
        }

        // HOOKS..

        window.addHook('plugin-missions-on-portal-loaded', (ev: { missions: MissionsPlugin.IMissionSummary[], portalguid: string }) => {
          if (!this.suppressHooks) {
            console.log("plugin-missions-on-portal-loaded -> ", ev);
            this.store(ev.missions.map(m => this.convertMissionSummary(m, ev.portalguid)));
          } else {
            console.log("IGNORE plugin-missions-on-portal-loaded -> ", ev);
          }
        });
    
        window.addHook('plugin-missions-loaded-mission', (ev: { mission: MissionsPlugin.IMissionDetails }) => {
          if (!this.suppressHooks) {
            console.log("plugin-missions-loaded-mission -> ", ev);
            this.store(this.convertMissionDetails(ev.mission));
          } else {
            console.log("IGNORE plugin-missions-loaded-mission -> ", ev);
          }
        });

        window.addHook('plugin-missions-in-view-loaded', (ev: { missions: MissionsPlugin.IMissionSummary[], bounds: L.LatLngBounds }) => {
          if (!this.suppressHooks) {
            console.log("plugin-missions-in-view-loaded -> ", ev);
            let b: ILatLngBounds = {
              ne: { lat: ev.bounds.getNorth(), lng: ev.bounds.getEast() },
              sw: { lat: ev.bounds.getSouth(), lng: ev.bounds.getWest() }
            }
            this.store(ev.missions.map(m => this.convertMissionSummary(m, undefined, b)));
          } else {
            console.log("IGNORE plugin-missions-in-view-loaded -> ", ev);
          }
        });

      } else {
        console.log("NOT READY");
      }
    }, 250);

  }

  async queryMissionsInView(page: number = 1) {
    // let bounds = window.map.getBounds();
    // let [n, e, s, w] = [bounds.getNorth(), bounds.getEast(), bounds.getSouth(), bounds.getWest()];
    // let results: ISearchResults<MissionInfo> = await $.ajax({
    //   method: 'GET',
    //   url: `${this.baseUrl}missions/bounds/${n}/${e}/${s}/${w}?t=summary&page=${page}`
    // })
    // this.handleMissionsResult(results, false);
  }

  loadMission(guid: string): Promise<IMissionInfoDetailed> {
    return new Promise<IMissionInfoDetailed>((resolve, reject) => {
      const mp = this.missionsPlugin;
      if (mp == null) {
        reject(new Error("missions plugin not loaded"));
        return;
      }
      this.suppressHooks = true;
      mp.loadMission(guid, mission => {
        this.endSuppressHooks();
        console.log("RAW MISSION", mission);
        let m = this.convertMissionDetails(mission);
        // $.ajax({
        //     type: 'POST',
        //     contentType: "application/json; charset=utf-8",
        //     dataType: "json",
        //     url: `${this.baseUrl}missions`,
        //     data: JSON.stringify(m)
        // });
        resolve(m);
      }, err => {
        this.endSuppressHooks();
        reject(new Error(err));
      })
    });
  }

  private store(data: IMissionInfoDetailed | IMissionInfoSummary | IMissionInfoSummary[]): Promise<IMissionInfoSummary[] | IMissionInfoDetailed[]> {
    return new Promise((resolve, reject) => {
      let isArray = data instanceof Array;
      console.log("STORE MISSIONS >> %o", data);
      $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: `${this.baseUrl}missions?apiKey=${encodeURIComponent(this.apiKey)}&iu=${encodeURIComponent(this.iu)}`,
        data: JSON.stringify(data)
      }).then((resp: ISearchResults<IMissionInfoDetailed>) => { // highest form is detailed, dont need union
        let missions = resp.results;
        missions.forEach(m => {
          if (m.detailsCreated == undefined) m.detailsCreated = undefined;
          if (m.detailsModified == undefined) m.detailsModified = undefined;
        })
        console.log("STORED MISSIONS << %o", missions);
        resolve(missions);
      }).catch(err => {
        reject(err);
      })
    });
  }

  private convertWaypoint(w: MissionsPlugin.IMissionWaypoint): IMissionWaypointEx {
    let p = window.portals[w.guid];
    let data = w.portal ? w.portal : p?.options?.data;
    const w2: IMissionWaypointEx = {
      guid: w.guid,
      hidden: w.hidden,
      objective: w.objectiveNum,
      title: w.title,
      type: w.typeNum,
      point: null!
    }
    if (data) { // not available for removed portals ("Unavailable")
      w2.point = {
        latE6: data.latE6,
        lngE6: data.lngE6,
        lat: data.latE6 / 1E6,
        lng: data.lngE6 / 1E6
      }
    }
    return w2;
  }

  private convertMissionDetails(mission: MissionsPlugin.IMissionDetails, portalGuid?: string): IMissionInfoDetailed {
    const missionEx: IMissionInfoDetailed = {
      author: {
        nickname: mission.authorNickname,
        team: mission.authorTeam
      },
      description: mission.description,
      guid: mission.guid,
      image: mission.image,
      time: parseInt(<string>mission.medianCompletionTimeMs),
      numCompleted: parseInt(<string>mission.numUniqueCompletedPlayers),
      portalGuid: portalGuid == undefined ? mission.waypoints[0].guid : portalGuid,
      rating: parseInt(<string>mission.ratingE6) / 1E4,
      title: mission.title,
      type: mission.typeNum,
      waypoints: mission.waypoints.map(this.convertWaypoint),
      numWaypoints: mission.waypoints.length
    }
    return missionEx;
  }

  private convertMissionSummary(mission: MissionsPlugin.IMissionSummary, portalGuid?: string, location?: ILatLng | ILatLngBounds): IMissionInfoSummary {
    const missionEx: IMissionInfoSummary = {
      guid: mission.guid,
      image: mission.image,
      time: parseInt(<string>mission.medianCompletionTimeMs),
      portalGuid: portalGuid,
      rating: parseInt(<string>mission.ratingE6) / 1E4,
      title: mission.title
    }
    if (location != null) {
      if ((location as ILatLngBounds).ne != null) {
        missionEx.locationBounds = <ILatLngBounds>location;
        missionEx.locationPoint = {
          lat: (missionEx.locationBounds.ne.lat + missionEx.locationBounds.sw.lat) / 2,
          lng: (missionEx.locationBounds.ne.lng + missionEx.locationBounds.sw.lng) / 2
        };
      } else {
        missionEx.locationPoint = <ILatLng>location;
      }
    }
    return missionEx;
  }

  loadTopMissionsInView(): Promise<Array<IMissionInfoSummary>> {
    return this.loadTopMissionsInBounds(window.map.getBounds());
  }

  loadTopMissionsInBounds(bounds: L.LatLngBounds): Promise<Array<IMissionInfoSummary>> {
    return new Promise<Array<IMissionInfoSummary>>((resolve, reject) => {
      const mp = this.missionsPlugin;
      if (mp == null) {
        reject(new Error("missions plugin not loaded"));
        return;
      }
      let b: ILatLngBounds = {
        ne: { lat: bounds.getNorth(), lng: bounds.getEast() },
        sw: { lat: bounds.getSouth(), lng: bounds.getWest() }
      }
      this.suppressHooks = true;
      mp.loadMissionsInBounds(bounds, missions => {
        this.endSuppressHooks();
        console.log("RAW MISSIONS", missions);
        let missionsEx = missions.map(m => this.convertMissionSummary(m, undefined, b));
        // $.ajax({
        //     type: 'POST',
        //     contentType: "application/json; charset=utf-8",
        //     dataType: "json",
        //     url: `${this.baseUrl}missions`,
        //     data: JSON.stringify(missionsEx)
        // });
        resolve(missionsEx);
      }, err => {
        this.endSuppressHooks();
        reject(new Error(err));
      })
    });
  }

  loadTopMissionsOnPortal(guid: string): Promise<Array<IMissionInfoSummary>> {
    return new Promise<Array<IMissionInfoSummary>>((resolve, reject) => {
      const mp = this.missionsPlugin;
      if (mp == null) {
        reject(new Error("missions plugin not loaded"));
        return;
      }
      this.suppressHooks = true;
      mp.loadPortalMissions(guid, missions => {
        this.endSuppressHooks();   
        console.log("RAW MISSIONS", missions);
        let l: ILatLng | ILatLngBounds | undefined;
        let p = window.portals[guid];
        if (p) {
          l = { lat: p.options.data.latE6 / 1E6, lng: p.options.data.lngE6 / 1E6 };
        } else {
          let bounds = window.map.getBounds();
          l = {
            ne: { lat: bounds.getNorth(), lng: bounds.getWest() },
            sw: { lat: bounds.getSouth(), lng: bounds.getEast() }
          }
        }
        let missionsEx = missions.map(m => this.convertMissionSummary(m, guid, l));
        // $.ajax({
        //     type: 'POST',
        //     contentType: "application/json; charset=utf-8",
        //     dataType: "json",
        //     url: `${this.baseUrl}missions`,
        //     data: JSON.stringify(missionsEx)
        // });
        resolve(missionsEx);
      }, (err: any) => {
        this.endSuppressHooks();
        reject(new Error(err));
      })
    });
  }


  async getMissionsInView() {
    try {
      let missions = await this.loadTopMissionsInView();
      console.log("MISSIONS", missions);
      //this.handleMissionsResult(this.makeResults(missions));
    } catch (err) {
      console.log("ERROR: " + err);
      alert(err);
    }
  }

  async getMissionsForSelected() {
    if (this.selectedPortalGuid) {
      try {
        let missions = await this.loadTopMissionsOnPortal(this.selectedPortalGuid);
        console.log("MISSIONS", missions);
        this.showMissionsDialog(this.makeResults(missions));
      } catch (err) {
        console.log("ERROR: " + err);
        alert(err);
      }
    }
  }

  makeResults(missions: MissionInfo[]): ISearchResults<MissionInfo> {
    return {
      count: missions.length,
      page: 1,
      pageSize: missions.length,
      numPages: 1,
      results: missions
    }
  }

  async showMissionsDialog(results?: ISearchResults<MissionInfo>) {

    let app: any = null;
    let initialized = false;
    let plugin = this;

    let dlg = window.dialog({

      title: '::[specops.quest]::',
      id: 'ingress-banner-helper-dialog-outer',
      html: `<div id="ingress-banner-helper-dialog"></div>`,
      width: 550,
      height: Math.round(window.innerHeight * 0.75),

      focusCallback: () => {
        if (!initialized) {
          initialized = true;

          let v: any = Vue; // (<any>window).Vue;
          app = new v({

            el: '#ingress-banner-helper-dialog',

            template: missionsListHtml,

            data() {
              return {
                haveApiKey: plugin.apiKey != "" && plugin.apiKey != null,
                bounded: true,
                favorites: false,
                title: '',
                currentlyUpdatingGuid: null as string | null,
                results: results as ISearchResults<MissionInfo> | null,                
                prevQs: null as string[] | null
              }
            },

            computed: {
              filteredMissions() {
                let vm = <any>this;
                return vm.results;
                // let f = vm.titleFilter.toLowerCase().trim();
                // return vm.missions.filter((m: IMissionDetailsEx) => {
                //   let t = m.title.toLowerCase();
                //   return f == "" || t.indexOf(f) >= 0;
                // })
              }
            },

            methods: {

              toggleFav(m: MissionInfo, state: boolean) {
                let vm = <any>this;
                if (vm.haveApiKey) {
                  $.ajax({
                    type: state ? 'POST' : 'DELETE',
                    contentType: "application/json; charset=utf-8",
                    url: `${plugin.baseUrl}missions/${encodeURIComponent(m.guid)}/favorite?apiKey=${encodeURIComponent(plugin.apiKey)}&iu=${encodeURIComponent(plugin.iu)}`
                  }).then(() => {
                    console.log("updated favorite status");
                    m.userData = {
                      ...m.userData,
                      favoritedBy: state ? [ "dummy_id" ] : []
                    };
                  }).catch(err => {
                    alert("Error setting favorite: " + err);
                  })                  
                } else {
                  alert("You must set up your API key to use favorites!");
                }
              },

              processImage(url: string) {
                let u = stripGoogleImageParams(url, "=w40");
                return u;
              },

              show(guid: string) {
                plugin.missionsPlugin.openMission(guid);
              },

              checkKey(key: any) {
                if (key.code == 'Enter' || key.key == "Enter") this.search();
              },

              search() {

                let vm = <any>this;

                let qs = [];
                
                if (vm.bounded) {
                  let bounds = window.map.getBounds();
                  let nesw = [bounds.getNorth(), bounds.getEast(), bounds.getSouth(), bounds.getWest()];
                  qs.push("nesw=" + nesw.join(","));
                }

                if (vm.favorites) {
                  qs.push("requireFavorite=1");
                }

                if (vm.title.trim().length) {
                  qs.push("title=" + encodeURIComponent(vm.title.trim()));
                }

                qs.push("apiKey=" + encodeURIComponent(plugin.apiKey));
                qs.push("iu=" + encodeURIComponent(plugin.iu));

                vm.prevQs = qs;
                vm.nextSearch(1);
              },

              async nextSearch(page: number) {

                let vm = <any>this;
                let results: ISearchResults<MissionInfo> = await $.ajax({
                  method: 'GET',
                  url: `${plugin.baseUrl}missions/search?t=summary&page=${page || 1}&` + vm.prevQs.join("&")
                })
                vm.results = results;

              },

              go(rel: number) {
                let vm = <any>this;
                this.nextSearch(
                  Math.min(Math.max(vm.results.page + rel, 1), vm.results.numPages)
                );
              },

              async getMissionDetails(guid: string) {
                let vm = <any>this;
                try {
                  vm.currentlyUpdatingGuid = guid;
                  console.log("querying ingress API..");
                  let mission = await plugin.loadMission(guid);
                  console.log("posting to backend API..");
                  let missionInfo = (await plugin.store(mission))[0];
                  console.log("done!");
                  let replaced = false;
                  vm.results.results = vm.results.results.map((m: IMissionInfoSummary) => {
                    if (guid == m.guid) {
                      replaced = true;
                      return missionInfo;
                    }
                    else return m;
                  })
                  if (!replaced) vm.results.results.unshift(missionInfo);
                } finally {
                  vm.currentlyUpdatingGuid = null;
                }
              },

              setMissions(missions: IMissionInfoSummary[]) {
                (<any>this).missions = missions.sort((a, b) => a.title.localeCompare(b.title));
              },

              formatTime(t: number) {
                t /= 1000;
                let d = Math.floor(t / 86400);
                t %= 86400;
                let h = Math.floor(t / 3600);
                t %= 3600;
                let m = Math.floor(t / 60);
                t %= 60;
                let s = Math.round(t);
                return [
                  d ? d + 'd' : null,
                  h ? h + 'h' : null,
                  m ? m + 'm' : null,
                  s ? s + 's' : null
                ].filter(s => s).join(" ")
              }

            }
          });

        }
      },
      closeCallback: () => {
      }
    });

    dlg.dialog('option', 'buttons', [
      {      
        text: 'Edit API key',
        class: 'ingress-banner-helper-dialog-button',
        click: () => {
          let ak = prompt("Please enter your API key", plugin.apiKey || "");
          if (ak != null) {
            ak = ak.trim();
            if (ak != "") {
              // test API key and at the same time verify name via IITC
              $.get(`${plugin.baseUrl}profile/api-key/test?apiKey=`
                + encodeURIComponent(ak.trim())
                + "&iu=" + encodeURIComponent(this.iu)
              )
              .then(() => {
                plugin.apiKey = ak!;
                localStorage.setItem(SETTINGS_PREFIX + ".apikey", plugin.apiKey);
                app.haveApiKey = plugin.apiKey != "" && plugin.apiKey != null && plugin.apiKey != "null";
                alert("API key is ok!");
              })
              .catch(err => {
                console.log(err);
                alert("That doesn't appear to be working.....");
              });
            } else {
              plugin.apiKey = null!;
              localStorage.setItem(SETTINGS_PREFIX + ".apikey", plugin.apiKey);
              app.haveApiKey = false;
              alert("API key cleared!");
            }
          }
        },
      },
      {
        text: 'Open missions in view',
        class: 'ingress-banner-helper-dialog-button',
        click: () => {
          window.plugin.missions.openTopMissions();
        },
      },
      {
        text: 'Close',
        class: 'ingress-banner-helper-dialog-button',
        click: () => { dlg.dialog('close'); },
      }
    ]);
  }

}

Plugin.Register(new IngressBannerHelperPlugin(), "specopsQuestBannerHelper");
