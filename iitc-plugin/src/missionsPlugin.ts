// this is because this lib is included from the frontend also, and it needs to resolve the IITC.PortalData type
/// <reference path="../node_modules/iitcpluginkit/src/types/iitc/types.d.ts" />

export module MissionsPlugin {

  export interface IMissionSummary {
    guid: string;
    title: string;
    image: string;
    ratingE6?: string;
    medianCompletionTimeMs: string;
  }

  export interface IMissionDetails extends IMissionSummary {
    description: string;
    authorNickname: string;
    authorTeam: 'E' | 'R' | 'N';
    numUniqueCompletedPlayers: number | string;
    typeNum: number;
    type: string | null;
    waypoints: Array<IMissionWaypoint>;
  }

  export interface IMissionWaypoint {
    hidden: boolean;
    lat?: number;
    lng?: number;
    guid: string;
    title: string;
    typeNum: number;
    type?: string | null;
    objectiveNum: number;
    objective?: string | null;
    portal?: IITC.PortalData;
  }
  
  export interface IPlugin {
    loadMissionsInBounds(bounds: L.LatLngBounds, callback: (missions: IMissionSummary[]) => void, errorCallback: (reason: any) => void): void; // getTopMissionsInBounds
    loadPortalMissions(portalGuid: string, callback: (mission: IMissionSummary[]) => void, errorCallback: (reason: any) => void): void; // getTopMissionsForPortal
    loadMission(missionGuid: string, callback: (mission: IMissionDetails) => void, errorCallback: (reason: any) => void): void; // getMissionDetails
    openMission(missionGuid: string): void;
    showMissionListDialog(missions: IMissionSummary[]): void;
  }

  function decodeWaypoint(data: any): IMissionWaypoint {
    var result: IMissionWaypoint = {
      hidden: data[0],
      guid: data[1],
      title: data[2],
      typeNum: data[3],
      type: [null, 'Portal', 'Field Trip'][data[3]],
      objectiveNum: data[4],
      objective: [null, 'Hack this Portal', 'Capture or Upgrade Portal', 'Create Link from Portal', 'Create Field from Portal', 'Install a Mod on this Portal', 'Take a Photo', 'View this Field Trip Waypoint', 'Enter the Passphrase'][data[4]],
      portal: undefined
    };
    if (result.typeNum === 1 && data[5]) {
      let win = <any>window;
      if (win.decodeArray.portal) result.portal = win.decodeArray.portal(data[5], 'summary'); // IITC 0.31.1 and after
      else result.portal = win.decodeArray.portalSummary(data[5]); // IITC 0.31.1 and below
      // Portal waypoints have the same guid as the respective portal.
      (<any>result).portal.guid = result.guid;
    } else if (result.typeNum == 2 && data[5]) {
      result.portal = <any>{
        // data[5] = [ "f", <latE6>, <lngE6> ]
        latE6: data[5][1],
        lngE6: data[5][2],
        title: result.title
      } // as IITC.PortalData;
    }
    return result;
  };

  function decodeMission(data: any): IMissionDetails {
    return {
      guid: data[0],
      title: data[1],
      description: data[2],
      authorNickname: data[3],
      authorTeam: data[4],
      // Notice: this format is weird(100%: 1.000.000)
      ratingE6: data[5],
      medianCompletionTimeMs: data[6],
      numUniqueCompletedPlayers: data[7],
      typeNum: data[8],
      type: [null, 'Sequential', 'Non Sequential', 'Hidden'][data[8]],
      waypoints: data[9].map(decodeWaypoint),
      image: data[10]
    };
  };

  export function loadMission(guid: string, callback: Function, errorcallback: Function) {
    var me = window.plugin.missions;
    // TODO: we need to refresh data often enough, portal data can quickly go stale
    if (me.cacheByMissionGuid[guid] && me.cacheByMissionGuid[guid].time > (Date.now() - me.missionCacheTime)) {
      // check if the cached data has "portal" info for fieldtrip waypoints
      let cached = me.getMissionCache(guid, true);
      for (let i = 0; cached.waypoints.length > i; i++) {
        let wp = cached.waypoints[i] as IMissionWaypoint;
        if (wp.typeNum == 2 && wp.portal == null) {
          console.log("INVALIDATE MISSION CACHE - MISSING WAYPOINT LATLNG INFO", { cached });
          cached = null;
          break;
        }
      }
      if (cached) {
        callback(cached);
        return;
      }
    }
    let win: any = window;
    win.postAjax('getMissionDetails', {
      guid: guid
    }, function (data: any) {
      var mission = decodeMission(data.result);
      if (!mission) {
        if (errorcallback) {
          errorcallback('Invalid data');
        }
        return;
      }

      window.runHooks('plugin-missions-loaded-mission', { mission: mission });

      me.cacheByMissionGuid[guid] = {
        time: Date.now(),
        data: mission
      };
      me.storeCache();

      callback(mission);
    }, function (error: any) {
      console.error('Error loading mission data: ' + guid + ', ' + Array.prototype.slice.call(arguments));

      if (errorcallback) {
        errorcallback(error);
      }
      // awww
    });
  }
}