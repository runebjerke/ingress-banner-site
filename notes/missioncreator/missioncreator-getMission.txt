https://missions.ingress.com/api/author/getMission

{
  "mission": {
    "created_ms": 1613413409439, 
    "definition": {
      "badge_url": "https://lh3.googleusercontent.com/hBuPLXFi9YIzA7eXgUFEz2FNHIL-dx9XnhikL30qLVXVROnUBqnzbar6R1IJCvIFBD1dzTO93aRsSnmE1aHX", 
      "description": "Visit Aker Brygge,  the fashionable pier area known for its restaurants, shops and many attractions. Enjoy the sea breeze, say hi to the seagulls and capture all the portals!", 
      "logo_url": "https://lh3.googleusercontent.com/ncyd5M0YbalU6w_h9zStaTckdOaiNq2kMtEvaowYe-Et9Cmu_mBBythUlDss2ZhFe927L3QqaxazHIktewE", 
      "mission_type": "SEQUENTIAL", 
      "name": "Aker Brygge (01/12)", 
      "waypoints": [
        {
          "custom_description": null, 
          "hidden": false, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "11ad1817a8294af7aeca0053ad59e024.11", 
          "poi_type": "PORTAL"
        }, 
        {
          "custom_description": null, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "71115e131a92467bb2b740105f2c47c1.16", 
          "poi_type": "PORTAL"
        }, 
        {
          "custom_description": null, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "ed47828310914f59b7bab1f4116a4de6.16", 
          "poi_type": "PORTAL"
        }, 
        {
          "custom_description": null, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "081547de56804d66929bbb00cd9c46a7.16", 
          "poi_type": "PORTAL"
        }, 
        {
          "custom_description": null, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "0b54a9c149e64f8d9824178d0161155f.16", 
          "poi_type": "PORTAL"
        }, 
        {
          "custom_description": null, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "633a29b7374d4d4c8daae6bd1f480660.16", 
          "poi_type": "PORTAL"
        }, 
        {
          "custom_description": null, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "e88310a4493a42ac9cf267a725bf6684.11", 
          "poi_type": "PORTAL"
        }, 
        {
          "custom_description": null, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "041bd1060be0424da5bd15d932db9996.16", 
          "poi_type": "PORTAL"
        }, 
        {
          "custom_description": null, 
          "hidden_location_clue": null, 
          "objective": {
            "passphrase_params": null, 
            "type": "CAPTURE_PORTAL"
          }, 
          "poi_guid": "c1a2c71ac5cb4fa38b67770ab5faf191.11", 
          "poi_type": "PORTAL"
        }
      ]
    }, 
    "geostore_token": null, 
    "mission_guid": "ba7fb6be438a4536b4c96c92a186f7a1.1c", 
    "mission_id": 5061693044948992, 
    "modified_ms": 1613413412231, 
    "state": "DRAFT", 
    "ui": {
      "view": "waypoints"
    }
  }, 
  "pois": [
    {
      "description": "Nobel Peace Center", 
      "guid": "11ad1817a8294af7aeca0053ad59e024.11", 
      "imageUrl": "http://lh3.googleusercontent.com/lQ5OSNvCh-KYLuVf8Ovwq67WtSzX9eM8NxTaD2zMeQVHM9-NowbzpFigRhyrbfz7DtcKgLEiFjkHToO5UXRdY1a6hRg", 
      "isOrnamented": true, 
      "isStartPoint": true, 
      "location": {
        "latitude": 59.911609, 
        "longitude": 10.730532
      }, 
      "title": "Nobels Fredssenter", 
      "type": "PORTAL"
    }, 
    {
      "description": "Informasjonpunkt ", 
      "guid": "71115e131a92467bb2b740105f2c47c1.16", 
      "imageUrl": "http://lh3.googleusercontent.com/0RjChaZ53EXiGnPiMQWMdNiza9GlYUDTw_0V4JBa_u6Jt28yzdS6oUku260Zw7tqVqkWA6lqUqtAGUQH08Dw-sBTgeRm", 
      "isOrnamented": true, 
      "isStartPoint": true, 
      "location": {
        "latitude": 59.911065, 
        "longitude": 10.730865
      }, 
      "title": "Havnepromenaden", 
      "type": "PORTAL"
    }, 
    {
      "description": "Trykk p\u00e5 en knapp s\u00e5 er det en dame som gir deg informasjon om hvor ting er p\u00e5 Aker Brygge.", 
      "guid": "ed47828310914f59b7bab1f4116a4de6.16", 
      "imageUrl": "http://lh3.googleusercontent.com/pcIa0wD4Ula8faF0SIb2G-AbqaoFFHqDwYCeAPQ4EdN04fHWT17MxBUVFDaCZOhN1ZUDZ8bOr5oPToYz4WqqjYa89Q", 
      "isOrnamented": false, 
      "isStartPoint": false, 
      "location": {
        "latitude": 59.910631, 
        "longitude": 10.729633
      }, 
      "title": "Snakkende informasjonskart nr 2", 
      "type": "PORTAL"
    }, 
    {
      "description": "Ved rundkj\u00f8ringen / entr\u00e9 til Aker Brygge ", 
      "guid": "081547de56804d66929bbb00cd9c46a7.16", 
      "imageUrl": "http://lh3.googleusercontent.com/BoCRsJXpxysOenlgai36xX-7JHrTBM1kqBuHTPjxUPQGa6VWiXmX14dIqTTEvvkKqQfO92mce14VWFTNNeQT_NFHJp32", 
      "isOrnamented": true, 
      "isStartPoint": true, 
      "location": {
        "latitude": 59.910582, 
        "longitude": 10.728966
      }, 
      "title": "Lang stang I rundkj\u00f8ringen", 
      "type": "PORTAL"
    }, 
    {
      "description": "Trykk p\u00e5 en knapp s\u00e5 er det en dame som gir deg informasjon om hvor ting er p\u00e5 Aker brygge.", 
      "guid": "0b54a9c149e64f8d9824178d0161155f.16", 
      "imageUrl": "http://lh3.googleusercontent.com/KeDfuiksCarFQKtJlR0F_GoS0As9kc1MWf--Y_obFuz8E_uCCE25fsPsKys1dtSSNOhCssllddtLZEh6Uvmb", 
      "isOrnamented": false, 
      "isStartPoint": false, 
      "location": {
        "latitude": 59.910335, 
        "longitude": 10.72903
      }, 
      "title": "Snakkende Informasjonskart", 
      "type": "PORTAL"
    }, 
    {
      "description": "Gatekunst p\u00e5 Aker Brygge", 
      "guid": "633a29b7374d4d4c8daae6bd1f480660.16", 
      "imageUrl": "http://lh3.googleusercontent.com/pv51UdpcI8E4xEowGvtK9deKf00P193m8mgHnzyAxqrEvYKo0ccmpE7cHpQ9mh3Btt28AAacAmiJr_s9Uekp_SeqO8QK", 
      "isOrnamented": false, 
      "isStartPoint": false, 
      "location": {
        "latitude": 59.910388, 
        "longitude": 10.728208
      }, 
      "title": "Pipen", 
      "type": "PORTAL"
    }, 
    {
      "description": "", 
      "guid": "e88310a4493a42ac9cf267a725bf6684.11", 
      "imageUrl": "http://lh3.googleusercontent.com/R__f74KAFKlV6dQ6R2Nkaubq2YryXr0siw3OqsdJiiOyeqH67ynMc72hSjRN2ByAqalb_8kBqTYJuHvsKsAMEHTw5g", 
      "isOrnamented": true, 
      "isStartPoint": true, 
      "location": {
        "latitude": 59.910191, 
        "longitude": 10.728472
      }, 
      "title": "Aasta Hansteen - av Nina Sundbye", 
      "type": "PORTAL"
    }, 
    {
      "description": "\n", 
      "guid": "041bd1060be0424da5bd15d932db9996.16", 
      "imageUrl": "http://lh3.googleusercontent.com/Xa59_tOB7WqkvT3mGK2QN8Iprh554dmOsYRqD1WWCvz9kEJNj4C7IavlK-6PI0QERkDQHTQXVT0EpEF76gp8x3YofjtO", 
      "isOrnamented": true, 
      "isStartPoint": true, 
      "location": {
        "latitude": 59.910076, 
        "longitude": 10.728423
      }, 
      "title": "Pelle-gruppa minnestatue", 
      "type": "PORTAL"
    }, 
    {
      "description": "Denne er en verksted klokke som kalles \u201cTre minutter\u201d. Denne klokka stod p\u00e5 toppen av Verkstedhallene. Den var en viktig del av hverdagen p\u00e5 Aker mekaniske Verksted. Lyden av klokkas fl\u00f8ytesignaler varslet arbeidsdagens start, slutt og spisepauser. En tunnel under Munkedamsveien forbandt verkstedsomr\u00e5det med velferdsbyggets kantine og garderober. Fra klokka ringte hadde arbeiderne tre minutter p\u00e5 seg til \u00e5 stemple inn. Kom de ett minutt for sent, ble de trukket et bel\u00f8p tilsvarende femten minutters arbeid i l\u00f8nn.", 
      "guid": "c1a2c71ac5cb4fa38b67770ab5faf191.11", 
      "imageUrl": "http://lh3.googleusercontent.com/fZg2q1HAj4wZ5WF24pMPOTrjLocIMG2kQnlKI7XPtnwXSk_avA2wxVUqcRHUINejCKFlyfVTNovgcRRjy-DC-1b-xw", 
      "isOrnamented": true, 
      "isStartPoint": true, 
      "location": {
        "latitude": 59.90981, 
        "longitude": 10.728274
      }, 
      "title": "Tre minutter", 
      "type": "PORTAL"
    }
  ]
}