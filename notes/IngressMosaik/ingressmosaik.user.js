// ==UserScript==
// @id             iitc-plugin-ingressmosaik@ohnenot
// @name           IITC plugin: IngressMosaik
// @category       Info
// @author         OhneNot
// @version        0.1.0.20190418.00
// @description    IM | Helper. Upload Mission to IngressMosaik
// @namespace      https://ingressmosaik.com
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @include        https://intel.ingress.com/*
// @match          https://intel.ingress.com/*
// @downloadURL    https://ingressmosaik.com/iitc/iitc-ingressmosaik.user.js
// @updateURL      https://ingressmosaik.com/iitc/iitc-ingressmosaik.user.js
// @supportURL     https://plus.google.com/communities/114699326997766567901
// @version     1
// @grant       none
// ==/UserScript==

function wrapper(i) {
    window.plugin.ingressmosaik = {
        v: "0.1.0.20190418.00",
        run: 0,
        dno: 518400,
        xiw: 3600,
        pwn: 200,
        znd: null,
        ven: !1,
        vot: !1,
        oxh: {},
        vds: [],
        ovg: [],
        ppo: [],
        vgt: [],
        fre: "im-plugin-mission-fav",
        mki: "im-plugin-mission-ref",
        oro: "im-plugin-mission-userSetup",
        nju: function () {
            function t(){document.querySelectorAll("table.sortable").forEach(function(t){sorttable.makeSortable(t)})}if(null===document.getElementById("sortable_is_on")){var e=document.createElement("script");if(e.type="text/javascript",e.id="sortable_is_on",e.src="https://ingressmosaik.com/iitc/js/sorttable.js",document.getElementsByTagName("head")[0].appendChild(e),"number"!=typeof window.sorttableWaitId){var a=0;window.sorttableWaitId=setInterval(function(){a++,window.sorttable&&(t(),clearInterval(window.sorttableWaitId)),20<a&&(clearInterval(window.sorttableWaitId),window.sorttableWaitId=null)},100)}}else window.sorttable&&t();
        },
        cde: function (i) {
            i || (i = [[], [], []]);
            var t = plugin.ingressmosaik, s = Math.ceil(Date.now() / 1e3);
            $(t.vgt).each(function (e, n) {
                var o = t.nhz.IM_Quest, a = !1, r = 0;
                -1 !== i[0].indexOf(n) ? (o = t.nhz.IM_Refresh, r = 200, Number.isInteger(i[2][i[0].indexOf(n)]) && (r = 310, o = t.nhz.IM_Refresh_u, a = i[2][i[0].indexOf(n)], t.ppo[n] = {status: r, timestamp: s - a})) : -1 !== i[1].indexOf(n) ? (o = t.nhz.IM_Upload, r = 100) : t.iiv(n, "have") && (o = t.iiv(n, "img")), t.iiv(n, "have") || (t.ppo[n] = {status: r, timestamp: 0}), null != t.znd && "dialog-m-im-dialog" == t.znd.attr("id") && ($("#dialog-m-im-dialog").find('tr[data-mission_mid="' + n + '"]').find('img[name="im-check"]').attr("src", o), $("#dialog-m-im-dialog").find('tr[data-mission_mid="' + n + '"]').find('img[name="im-check"]').parent().attr("title", "Send to IngressMosaik\nNext Refresh you can send " + plugin.ingressmosaik.iiv(n, "time")))
            }), $('img[name="im-akt-load"]').removeClass("load_a").parent().parent().animate({backgroundColor: "rgb( 00, 102, 00 )"}).animate({backgroundColor: "rgba( 00, 102, 00, 0.0 )"}), t.tzu(t.mki, t.ppo), null != t.znd && "dialog-im-lite-box" == t.znd.attr("id") && t.znd.html(t.ama(t.vds)), t.xsw(i[1])
        },
        difVersion: function (i) {
            dialog({title: "IngressMosaik.com", html: "<span>You are not using the most recent version of the<br>IITC plugin „IngressMosaik“ which could produce unexpected results.<br>Please update this plugin and reload the page or restart the browser.</span><br><br>" + i, height: "auto", width: "auto", collapseCallback: this.collapseFix, expandCallback: this.collapseFix}).dialog("option", "buttons", {
                " Step 1 - Refresh IITC-IngressMosaik-Plugin ": function () {
                    open("//ingressmosaik.com/iitc/iitc-ingressmosaik.user.js")
                }, " Step 2 - Refresh this Page ": function () {
                    location.reload()
                }, " OK ": function () {
                    $(this).dialog("close")
                }
            })
        },
        asd: function () {
            dialog({title: "IngressMosaic Plugin has been updated", html: '<span>Thank you for updating the IngressMosaik plugin.<br>There are many new features.<br>To explain what\'s new, I created a tutorial.<br><br>You can find them here<br><br><a href="https://ingressmosaik.com/iitc/doc/IngressMosaik_IITC-Plugin_Anleitung_DE.pdf" target="_blank">German version</a><br><a href="https://ingressmosaik.com/iitc/doc/IngressMosaik_IITC-Plugin_tutorial_EN.pdf" target="_blank">English version</a><br></span>', height: "auto", width: "auto"}).dialog("option", "buttons", {
                " OK ": function () {
                    $(this).dialog("close")
                }
            })
        },
        ggi: function (i) {
            return JSON.parse(localStorage[i] || "null") || null
        },
        iiv: function (i, t) {
            var s = plugin.ingressmosaik, e = Date.now() / 1e3;
            if (s.ppo[i])switch (300 == s.ppo[i].status && e - s.ppo[i].timestamp > s.dno && (s.ppo[i] = {status: 200, timestamp: 0}), t) {
                case"img":
                    switch (s.ppo[i].status) {
                        case 0:
                            return s.nhz.IM_Quest;
                        case 100:
                            return s.nhz.IM_Upload;
                        case 200:
                            return delete plugin.missions.cacheByMissionGuid[i], s.nhz.IM_Refresh;
                        case 300:
                            return s.nhz.IM_Refresh_done;
                        case 310:
                            return s.nhz.IM_Refresh_u;
                        default:
                            return s.nhz.IM_Quest
                    }
                case"load":
                    return -1 == [300, 310].indexOf(s.ppo[i].status);
                case"time":
                    return 300 == s.ppo[i].status ? s.sed(s.dno + s.ppo[i].timestamp - e) : 310 == s.ppo[i].status ? s.sed(s.dno + s.ppo[i].timestamp - e) + "\nAnother user has already updated this mission!" : e - s.ppo[i].timestamp > s.dno ? "now" : s.sed(s.dno + s.ppo[i].timestamp - e);
                case"status":
                    return s.ppo[i].status;
                case"have":
                    return !!s.ppo[i]
            }
            return "img" == t ? s.nhz.IM_Quest : "have" != t || !!s.ppo[i]
        },
        xum: function (i, t, s) {
            var e = plugin.ingressmosaik, n = e.nhz.IM_Refresh_done, o = ["rgb( 00, 102, 00 )", "rgba( 00, 102, 00, 0.0 )"];
            if (s) e.ppo[i] = {status: 300, timestamp: Date.now() / 1e3}, e.tzu(e.mki, e.ppo); else {
                switch (t) {
                    case"update":
                        n = e.nhz.IM_Refresh_error;
                        break;
                    case"insert":
                        n = e.nhz.IM_Upload_error
                }
                setTimeout(function () {
                    delete plugin.missions.cacheByMissionGuid[i], plugin.missions.storeCache()
                }, 100), o = ["rgb( 102, 00, 00 )", "rgba( 102, 00, 00, 0.0 )"]
            }
            if (null != e.znd && "dialog-m-im-dialog" == e.znd.attr("id")) {
                var a = $('tr[data-mission_mid="' + i + '"]');
                a.html() && (a.find('td img[name="im-check"]').attr("src", n).removeClass("load"), $('img[name="im-akt-load"]').removeClass("load_a"), a.find("td").animate({backgroundColor: o[0]}).animate({backgroundColor: o[1]}))
            }
            null != e.znd && "dialog-im-lite-box" == e.znd.attr("id") && setTimeout(function () {
                e.znd.html(e.ama(!1)), $('img[name="im-akt-load"]').removeClass("load_a")
            }, 100)
        },
        cdr: function (i) {
            var t = {"&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#039;"};
            return i.replace(/[&<>"']/g, function (i) {
                return t[i]
            })
        },
        nhz: {
            a: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAQMAAABsABwUAAAABlBMVEUAAACy+/gnk9HpAAAAAXRSTlMAQObYZgAAABVJREFUCNdjYEADB9Dg//8QjA7RAAB2VBF9TkATUAAAAABJRU5ErkJggg==",
            b: "https://commondatastorage.googleapis.com/ingress.com/img/tm_icons/time.png",
            c: "https://commondatastorage.googleapis.com/ingress.com/img/tm_icons/like.png",
            d: "https://commondatastorage.googleapis.com/ingress.com/img/tm_icons/players.png",
            e: "",
            IM_Upload: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABApAAAQKQH1eOIPAAAAB3RJTUUH4QocDAwxtcIkUwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACGklEQVQ4y62Uv2tTURTHP/fkJSHQvnaxpWQyirRLp/4BHWo1BBy0W60idlVc66BZpKtTp4q02K04CCEaon9Bpw5a/IEohJC6pK+FkCY91+W8+KwOgj1w4d5z7/nec873e6/jlH19FYaZNCVgThzT6hkFEEdbPbtA/bhH5fy1KErGueSiVQuL/ROWxVECsvzduuqpBCnWx+ej6h9AjWp4WxwPgUvm2lfPB6Bp6wlxTAFjAOr5BDzJF6ONAVCrFhZVeRqDiLAObKUDtwN0DCjX6/sZYFGVZfN9FOHB+HxUddaT5+K4bpvlgyO/OrlweAzQfB1eAZi4Gr0B2NsezowMuRWgbJm9zGXdHcmkKVlPUM+zJEirFi6pZw1Ya9XCJYDJhcPjgyO/alkjjlKn60sCzAFZ9fwIUrz4DUQpi6MAFFQpN6rhrRgM2AL2jZQ5Ece0lfTeejIAAQoJtgrieBxnlg7cjpGBOKYl1omx02lUwwuq3FCPAO0EUFs9ospCoxpeNBKa1pJROS2SfDH6IsL9TMAssBH71bOZCZgV4V6+GH0+HReIG9w6AeSAo/H56LuV2Fa1gyna5y5H3xKxOYtBHG0x2SOOKdPJP1mv72dMoKhnV4A60AXG+ifc3NsezgzKUTSe909+ze3Moqm8C9Qll3UV9VQsq7sjQ24lBlPPO+CRjbdJQcbqVk8ll3WVs3siZ/poz/Qb+d+P7Se4ZBaJQyGIdAAAAABJRU5ErkJggg==",
            IM_Upload_error: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABApAAAQKQH1eOIPAAAAB3RJTUUH4QoZEDcTfvIgqQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAB7klEQVQ4y62UP2gTcRTHP/e43BEC5ZZL6RpFyA1d4nRk6FAQyaZu9Q+iq+Kqi1mkq1OnilTs6haKELSUcJNdMqTgn0EdSnrLIZaQFl5c3sVLdBDMm36P977f+95739/PYS5OGo0lp1RqIbIOrAKBlTKgj2p3cn7eqR4e/ijinGKSNptXUb0PtACfv8cY6CCyHfZ6e38QpXF8B3gCXMrFAUfAseUrQB2oWv4JeBYmyc6UyJQ8n5KIbAO7ju9/AEYGLE/G48vAhqkG+IjIo7DX23NOGo0lx/dfAtes2NYs21weDM5M6RWAMEneAgyjyJMgeAy0rf+NUy7fFadUatlMAF7MkDSbt4AtYMvOLA8GZ5plm6YaoDU5PW2JbccHUkRez5CotoEaUEO1ncbx7ZwM2LU5+oisi60YYGAzmSfJowY8zZVZ75HVVqXgk2NglMbxBVSvA2LeoeAjQfVGGscXbQn5RgOZN0mYJF9w3Yd43hqwUyi9wvPWcN0HYZJ8nse5ha+uAGXgZ3hw8M1+MUMVs0QW7u9/LWDLhgHIBOhbUjef/FNYb93SvqDaNdtXUb05jCJv2q25nNmz9WyYy8eodl2nUulMRqOOGfKeBMH3YRTlXnoHTAz/fsaQv93dcSqVzuKuyEIv7UKfkf992H4B7n/zrhpsfskAAAAASUVORK5CYII=",
            IM_Setup: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAztAAAM7QFl1QBJAAAAB3RJTUUH4QofCgcTlW5vIAAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAC5UlEQVQ4y62UTWicZRSFn/N+00yqGdvJN0UJaaK1gdiNCGqRCPVnoWDAUshQaHRRbJuFoGJAwZWgKCga0YWIbtRoM1NDVqL4Q10UpXYl1TaYpplprJZkEmqqNsnMe1zYBAlVKXhWl3u5hwPn3AtXiBMfpG2VD6+5EWDy4Mabf36XqwF0JSQ/DbcU6pns65byAX9mtFd4LJmvPR8utzAxXOiaGC50re235S8sIHJC90U0ANoWzVVteRZXFU2P5tL2XQu1qVJ6j8ybFk7Egc19tcPTo7mUxWxaDzwt0WvzdkNxLEFPCt1E5NMMQLVU2NFYZl+llD0GbjW6AVCMvrcyUrilXudWBX8nwp22c1I8uaVv7ttKOZ3GPOjAeU2P5tLGUnaIoH7bllgGNf2l00srdYx+H/sdJXoKs1kwjuixGVNceim071qomXgU3JAkw3kTRxxjCZgHMP4D+Uj0xRPg9cAmcBPoWuSLkyd/rWTOlNO7Gg2uszHyjIiDHcwNE+AM6W6bVyTyiPZ1mfXd4Lca4M5i7WClnPabOHP3s9Q1NVI4LtENSsCHOpjdrSINAJdIqhQOgXbariOmMri3vVgbX+toQEQb/2eI9O+ZC6gxgHnOdt2wo0rrHpdIXCKpurUf6AEvg18M8Egdbq+UWh+20VQpfahayt+/muzKSDqA9CqoGTxjOHJp2APahF234hON+mI5SZpLkrZhf4NCr4mvnf6+NpipDG/IG90h1HzJ/g1CO1ftt+tIGcWwPck0/yCH34x/AS0JnxO0dHUXtmglkDb7EMewNxo9g5DtFySfUwzbkY+D9hvapHigo2/uvUo5fVnoUcPRANBRnP0qWbf4WGdxdqhhfYk4BT6VCXxxfXHujaRp8fGQaNT2YYkFO3RPlltvA9qBccHX+qejBdi6Z/bHv/f9MdnqhfQjCA8YTwhtteNQp2qDmcsRrSVYwdn5lhwZFpA/cfTnCuwN4vez82T5vx7bn4EnZ/KENiB9AAAAAElFTkSuQmCC",
            IM_Refresh: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABApAAAQKQH1eOIPAAAAB3RJTUUH4QoTDTsXrml7+wAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACdklEQVQ4y82TTWiUVxiFn3O/b5IoTNGF+EcMU4o4RhAkyyKIbTHJWmgXYiGkpeAiUaQLXfiDihWaUaRgS6FLwYULaYMltKmo7WKQKiSRcREnQdEKgj9o4uS7x82oYzLZ9yzfn4dz3/e+Yp5OVG8tX6pcD/Z2480i5I0Feiz418SRtM3De1Zueg5wrHJjxZKWtnY1Qs5Mj/dG04/VI5GjiYxnMJeAn9KcK3NzOoTZ+BZUqo7vBg5K+qih6SnmmQBDXtIHDcQ7UX4Y0Me27wpgaHKsWyGUJNbXy6awz9v+PQst1USvFDO3JyH51PCFoOM9l/bd9OSjiTwv3PcGYnvUIXw3uK44PO9Vd4A/SlMTFdvHJa1qTIbWF3GHRG8dco/oU4PtG4abzefs/bEO8Nb5EICAtF2oDQDpwkCh87dmkK/K5VytRp/QlwuSwikwanSPmM0S1BQC8GNXV21ocuzvSDyoJAkNC3FQqPK/k05P394V7Q6ht3/KWRaBG4OFzuHFGktT45uMuiG24nArtePhgArgd/QkEB2PniuXR77u6qot4qEvwAAEonw8YNTkDH7J5fh5MUhpcqwHe2e99kkwf4UFEPNAcG3Pms6mmxiavt1N0H5Ja+uhi4/jwyvpAsNildGR0tREMdqXQ/Bk5hYn8VWHpM+I/hxpXd1NOcs4d7iwbaYB5Ks2ayR9KFgN7A3Qr6inCTUIysO7o7V90+L7fYWN/wCkwH/Go2nqQ9lcUrSzfqReoVZJeSCv9+c3i7mkqB8GCsU/38TTaL55OTszfWD9lkdA9XSlct25uR4Cn1jejFkmZMtPZN1EjMwu1a/frig+axzJaxRDHilHQlnCAAAAAElFTkSuQmCC",
            IM_Refresh_a: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABApAAAQKQH1eOIPAAAAB3RJTUUH4QodCRMOcImifgAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACJUlEQVQ4y82US2hTURCGv7nemCik1KRJQ6W2ahEqguDax8IHJlkZIiioqYbu3bjroioU1I0rQaSoCx+gmEWxwRI09YEbERXcNJSkvqg5tFhTMCH0Hhc9lGu42TurmTMzH3P4zxyhxc6UQpssmwRwENgNBAEBFoEPQGGdX/LjPQvLAKc+hyL2RnrFDTlbCSe11sNAAvDhbXVgArhl+WTGaepRYOcaaKgcygAjwICr6TdQM34Q6HDlSsBPYC9QEYDMbCguFteBHaboC/AQmALmRES01r3AYeAk0NcyZcXO/ggHVxo664IUReTq7f6FfEtxCXg+VA7NAGNAzJ20Vhr6KJA08XftcM0DAsC5b+E+YH8rBMAy6gRM/Oju9sVJL0j6VafPaeosMOSR1jZQBCpGjck2SvF4369mZjb0FhgRC8sNEZE5/juTtFKnnVU5196U1toB3uei0Xy7xpRSu4A44Ac+2Q5cBLb+QxcBuHxgfr4wHYs127CywHnjj1nuSVx2JwDj7SDHqtUEcNyES8C07VE3D7y5H4l4KpFWKu7ABWCzOcr90fqlFygGXEopNQg8s6DsgDZrccSBE8AWU/tuReub+Wi0LimlykA/8BroAba5oDWzuF5L+xG48iQSeQBgA1WgGIDROgwCw2Zl/KY52DJxA5jQWt/IRaMv1gRKKbVn2XG+TnV3K4BktdqxHhIicsh8bJ2sXm3JTFHYIPL0XldXzU3/C+tFr82k66mAAAAAAElFTkSuQmCC",
            IM_Refresh_u: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABApAAAQKQH1eOIPAAAAB3RJTUUH4QofDzoKnSZNtQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAB8UlEQVQ4y83UTUiUURTG8d+8vo4hKAYGZqgUEhhB0Dra9EHqOqhFFIiLdiGEYAlTg0IF0iqICFoGLVpESSFl0dciJFtqkBpJH0OYxaDTOLboKtPwuu+s7rnnuX/Oveeek1Jh8xmb01W6cAB7UIcUvuMtxmrTRmv7/YK5QVtq01pS5ZCFId3Fkl50oVqyLeEebqSrTBVWZLBrHZTLOonzaC87tIifYV2H+rLYNL5gH2ZSAdKJq9gZRHO4jUeYjSOpYkkLDuE42iqynInzl9TlC3rKIONx5HLDOaMV4mk8zmVNYRhN5cEoX3AE3cH/hCsJkL/3HNaG/ZUQiEJ1NgX/TuOgB0mQiT7VhRU9OJUQXo0xHjJZJhkCe0f8zmW9CgWJ/oFEZv13lloYcqJY0hZ+75qVMNE4mPzo4cvsRidq8C4ullzA9gRt9v2AsfZhvzdg9eBMWA9HFZms2a10lZsbQXJZXTga3B94GiXoPuNF/UByJRaGdOIstoWtu/mCZ3GCtgkXc1kdeBhHPhRLVkNbHC6WHENr0L4prLjemrVUDnqOZuzAVvSht1iyuEHTTmKkOeM1xPiK8XSVTGFFB3pDy9SEw3UVGS+HMXKtcdCTtc0Yp/MFHxuzvmF2PuNlGGwHw2BrwGp41Mkw2O7X9q+PF/AHhmKVcUEonlUAAAAASUVORK5CYII=",
            IM_Refresh_done: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABApAAAQKQH1eOIPAAAAB3RJTUUH4QoYFgYV7f2Z+QAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACJ0lEQVQ4y82US2hTURCGvzneUBEi7Q1CjdaoVSEqCK7VjQ9MulWwoE1q6MKdFNx1URW6UBBXgkjVqqCg4KLYoBStbxdSrODClpBUrfggUVuRhDR3XPQo13Czd1ZnZv75GM6cOUKddU25LcYhCewEtgBhQIAS8AoYXdQk2cFo8SfAwTfuMmcJbeKHdBciHaraAySBEMFWBoaBCyYkk15V+4GNf0HpvJsC+oB1vqJZYM6ew8BSX24K+AxsAwoCkMq5CTGcBTZY0TvgBnAPmBYRUdU2YDfQCcTquiw4mY+RcK2iGR9kTEROXVpdzNaJp4D76bw7CQwArf6kqVV0L9Bh/Rn1OB0AAeDwh0gM2FEPATB2Ooutf3OovTQSBNn3uDnkVTUDpAPS6gBjwAxQAUYaTIpb279XUzn3OdAnBuOHiMg0/51JdyFySFVj9vUu9OrhAeND7aVso8J03t0MJIAm4LWjqseBNf/QF27gZOd4y+j1rd+qDVgZ4Kg9Dxh/Jz67bEIy2AiSyrlJYL91fwAPTYDuE/D04spi4CS6C5GEGI4BK2zodq3MIydA2wqcSOfdOHBXRPKqqnYt9qjqAWCV1b705jl/NV4q+0FPgCiwFlgO9AI9qjrbYGkngDNX1pdeADjAF2DMhKTfq2oc6LEr02SLw3UdV4Bh9Tg31F568CfoAEfmf/H+2qbiV2C66637TBySYthlP7ZmQO2lTtiP7c5gtDjnp/8Gyjy1LrfmuBsAAAAASUVORK5CYII=",
            IM_Refresh_error: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABApAAAQKQH1eOIPAAAAB3RJTUUH4QoYFgkMDg4t9gAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACIElEQVQ4y83UT2gVVxTH8c+dvkfqIqKLQqwkoyKieS90EYQiQRfaYl62CrooFcRFd6XgLouo4MJCcVUoReiyoNCFGLFEm0iMAf/gv4gki76XEGgrCJqChgdzu3hTmYbJvj8YuOeeM1/OzO+eG6zRVK1vczWEBg7iE3Qj4BUeYaIrhOuDT5t/w83+vo82JKE3FCGz9XQk4xQaqCrXO1zFj9UQ5tsxjqH/PWimnn6JUewsvPQGK/m6GxsLuQX8iSE0A0zX0uEkuIhdedEifsavaCWEjF58huNI13TZrDwY2Na9GuPJAmQy4cKnz1rX1xQv4NZMPZ3HefQUk8lqjIcxksfLWfRtCQTcG9iWYv9aCCS5Ox/m8eWhudZ4GeTKrq3VdqfzEyXpWMEklrGK8XWccmR+uT1dS+9iNAmSIiSh5X+nMFtPv8g6dr4/U1mU4eHQXPlPz89dHcPowpNKxhls/48DHeS58d29E40XS+11WCfxdb4+nxQ7KeinagiX1oNM19IGjubha0xVSur+wJ29T5ulTszW0+GM09iab/3yNou3y0A9ODtTT/fgRsLvGTEfi88zjqEvr73fjvGHg88X3xVB0/gYO7AF3+BU1hncsqF9jO8OzC3OQgV/YbIawlg7xj0618hI7kZ3/hS1iqtZ9P3QXOu3fzcr+OptFpf2PW+9RGuq1jfzgdBIgkP5xbZJ59Ne511MdIVwbfBZc6VI/wfeI6pkFoBLVgAAAABJRU5ErkJggg==",
            IM_No_Fav: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7rAAAO6wFxzYGVAAAAB3RJTUUH4QoTDgEZbj423AAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACcklEQVQ4y52Uz2ucVRSGn/e7X2KT+qNKsSSVScYu2qRVSukfkNJFN2IKgiVdKEWlmyymrbhxYaRVUbSTgssKdtNAQLDgrou6dlUkzhSLODOJRaWY0tBO03z3vi6SSIMzDXrhLu45h4eXc857YZNTbdWPVFv1I5vV6XHJz3+7sT2PngEogibe3bnndrfa7HGgPDKOPYY9lq2ko/9L0RetuecCYQa4sxbaFokTZ0r7/vpPigJhXOagnC6vXg4Gwvimij64di3vKz21NahvW8i8Kyh7z3B7Mf3xNsCz2Y6Lgu3R6bOY9Et0+067tXTvw0OHCgBVm7U3gJeFyyLbafw8Uh9m2VLlVGnPt6vTu3FU9jTiCey20J/IC8k0gB91oVm/iHgLu2WYTaaRhfCrtXLr1Av7rm9YhYW5/XLPYIqxnIlhwetIJcxXeehJZ4tCAekw9tzp4dFL3fqwBr4OMN2svWnpGPjrvMdnBfDlrZ+GikJTmDFgqjLUHbYOAaYQ3+e5pyYH9zb/afYGmPR+pTRyuSOkVT+O/dGjkA3jnxzc25Tzc8CS7QNdx7yaW5Lzc+uQf+3RSlh+iOgXnu8GSrCA6F8Jyw83uODRR2/qHUikrWThJsD5Rm1A0qsAPVu2XJncUf49ZNyMiSd7U+8AMN8RFF2UhWIk3qvO195RYgL8EkDxoH2s2qjN2PwsEaOLMvBDRxDWbqRnAv4YawT5CuaTNQ8cl/jUpo70NGZ3R6/N2kF4F5BhNWWdWLybTlaGRq9WhkavLt5NJ+XsBKiJybJML87aoaP7pxv115CyqP7vzpRK7c6/Qqsv+P4r2KkyPPLNevxvMAkUwpXp0hYAAAAASUVORK5CYII=",
            IM_Fav_o: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7rAAAO6wFxzYGVAAAAB3RJTUUH4QoTDzgoMKbRawAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAABf0lEQVQ4y53UTWsTURgF4GcmmUJrkdaKIC4kZtGF6EIQJxXFrtqFED/QogsR/COF/g6hK8WFXyDYrYhmoOBCcVmw4EoKBkELmXbi5rYEm2liDrxw38Ph3Pdyz70MQN5KF/JWujBIFw0wOY6nob2XNLKtMm08YKMmroW6cZgwPmSaY7iLaqg7gfvviZqY7+nnA9cX1b1F5/3ZalQZP0I8JYrrWELSo02wlLfSTd1ig6Ld3d3+PXbl6w5EeSt9gPOo4RROYArTJZv/RBs/8B3f8LmKq3hkeEyHquFS4B7HongFq0bHqiheiSDP5k7rFst4OILJcpJ+3NwP5Ahm+yYHkp230jpe4twAky+4mTSyjbIcdTAxxDQTQVsayJOYGcJoJmhLjWqY/IfbCtWLyaA9mOyA2R7uF17gSejv4xaOBs1sX6N8fbFip13HH7zGM0XnbXL5UwfyDxfeicdehafTxJl8fbGSXFzb7Xdrt8Nx3ySNbLvkVxjHdRRJI3u+x/8FwVZo4lhEwcoAAAAASUVORK5CYII=",
            IM_Fav: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7rAAAO6wFxzYGVAAAAB3RJTUUH4QoTDgAgKCCPlQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAB3UlEQVQ4y52UvWuTURTGn+fe20BildaKUAuJMWC1RVc3aaYuQvxAQYcgOHQRDCiutnQTJRVchU4GCuriIh36P/gVEQpNUAcJpERMaPO+93HoB0bfJMaz3ecefvecc885QB8rVsuzxWp5tp8fe10++vrpiAtVAoDA8vq9iVO1br6mF8iFyEGagTRj2v5iL9+uoMfV94cBXSPpSDpjeHVHGxBkYXOQsvuClLWwub41erC25uLJgwcs4yPWKGNp7oPsLLL0JpR/GHquh2pttqo/fi5kswEAsFj5mAdwllCaMBOijgIcITAa9bKAOqBNit9BffHCBoC3zoDnQdzaC469PxI7D3AURBrgOUMAwjNjh/yioGX8pwlatkN+kQDw9NuHVBBwnuDNQSHOaf72senKfh6Dwn6H/NXZTyqfM0LwiuSZnhDpHeEu3UmdXI/so7bd2gaRQP/BSrTt1nbXhoz52DiBsX8Y0LGYj413BYUK0hKG/0ijJqnWqWE4VJDumMvO5DlJQ7dbzAaAlwCe755vALhM8BBJJ4/JyIhWJEsoI6EpoUSZfL3h5wqpqdVCamq13vBzlMlLKEloGsMTK5KN3EdLG+UrIE3IxOu7yWQreitU41bNC5B84fjpF3v6L5K3x3cAgLuiAAAAAElFTkSuQmCC",
            IM_Checket_o: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7tAAAO7QHxzsUOAAAAB3RJTUUH4QoTDy0nl6wq7gAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAByUlEQVQ4y62UvWtTURjGf+/bc4+aUB2khHiHgLonV3TUTdA/QUH0n3DQKUUUkdbJSRwUQcShi+BsJxc1CeoiOtlQW6HFj2g4NznHoSW511xJi77j+fid53k57yNkKv14XphS0ZFHoWhdAFyrehr8KdAKeP0rRUwg+K/gn9pkbdl1amWG34b22GZfXKt6DtFrBP8D2ADCFFF94C7wBrgKpKi9aYCLBN+BwRXEfppmDTGKdxWgiegFAHzfGESPEgZ3bPLlPTso16oezkEA0ENK8ALaGx18Wze7ggT/HNFbuca6Tq1Gun7DtePLu4A0baO7bDKQg3jXRPQSgGvHJdvozu8EAjC24V0NOJMR0XTtGIJ/OA0CMLJmk9XXiC5sf4MRDFgqghAGn92r2dkJEIBtdBeB+RxMtD4B8f0V0NvM7D9RCNpWtrD1ov+e28jaEVsBjmfvF47DhLKCnvxZBtFAGJQLlC26dpwSfILo/RxEdAYIhMFwDAr+A+hJ15p7lvY3V3Jp8GvtXlSKy+nPbq/3wpQAoj0HDMGfRTQFXR+D4MHW0Jon0b7KBsHnh9Y7or1z2ZUyUAeu22T13b/FiOhL2+g+nsij/xFsvwG+Pt8FEBqwVAAAAABJRU5ErkJggg==",
            IM_Checket: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7tAAAO7QHxzsUOAAAAB3RJTUUH4QoTDTkcC43A8QAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAB/UlEQVQ4y63UP2tTYRQG8Od5702gCe2QWgId0rS6NEWKoKOCg6AfwaH4DYwUO+iSlKJoTUuJncRBcREHF8FVC45BFE1EUUw1VBFNaZtqzP3zODW9JqmJ4hnf4cdzDu85RKAkEV2KpDq+A8DSaukEfRyDURw+zZ6IkSRswOeD9Fhq5dqnZ9Fafd2bHT1eZ/79y9OgmRNRk1QloT9GEuugbniwX1jwLkJwrLCu2DLmDKDnjssLDPV/7NaaHw4Zq7EZN/CyBKZAwP0J26Z0QPKXz+8/+AY91OLq6zEDL2uAqcDcho1I+rS2dx4zemT/DSLpMQzmfxvs8lpxJPYhfjlfLs70jjCbTqRWmtD1SmnQayBLYAbkfL5cyvSKAECzDcfFiEWeDHyMbL5cgkvrTjcEAJqJppOppyRzEmpBzIJ3vxPiu/x89dWT/jYIAM4mxhcAzQYxApOtCOpexVha7Ovbd6QjBADp5ESOhllBWy3rs9tOGHFIh0HP7AntJKNvmsk6zaS1bEoyYrQNGx1fyJeLjsRDMLwVREhjAZR8z2tCIt+COpp7V3q4tf69EsS+ff1xMzY8EK2ubW5nCoUIAMQGBmxfzilDOJ4xX3YT+f5t0MyFbNwbHIpUhZaldVzEhiKBYTWipJmEdGk6OVH89zMCbogonEuk7rbdo/9x2H4BFHkMauEVkmkAAAAASUVORK5CYII=",
            IM_Check: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABGhAAARoQFTdAd6AAAAB3RJTUUH4QoTDToezq7yHgAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAA50lEQVQ4y+2UMWrDQBBF31+pkE+R0rFBKQKGFMK3kN3pAOnS5Qjp3OUA6aK9hXBnSCOQrdKnsJrdSREISSeBS79+HsPAPAHszqetI1Ym5hgJYxBBRh9xHy9395/anU9bp/hmMAAt6DJKhM2AXJBFc6+pI1YGg0J43h+OzbIsbYym817FarG2JHl3xCo1MQe+9odj4zebwBTquimeHlpTfHQ/N9Fl7CZ/WZalmdmAkTiuxE10E11HJALYrPNeU4c77yUpQ4RURm8iL1aLNXU9/WmJuYz+X0aEa81sGJUjKTPib0Z0rbB9A4/ychOyQ0NbAAAAAElFTkSuQmCC",
            IM_Quest: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABNkAAATZAEwx24xAAAAB3RJTUUH4QoTEhQVRQHG9wAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAABhElEQVQ4y5XSvWsUcRDG8c/cLb6AKRRBRNBebBRR1OKIiFiktrGyEc2ZXrAI2ARBLOS4xIhpBHsrm8Q/IBqQNGKXCL5FCCKoSLxdC+/wx7q7kIGFnWdmv/OyEyrsBuPBBM4E+4fyh2BpwLNZVsvfRFnochvd4GBVkYLV4EGPx6neTp1JplvcCcbUWHAAJ0+xuczr/0BdJoJ7wc5SB5vYKuljBYdOs7jMV2iNIgMGwXqSvF5wK+cSLhc8KTV3POfsyMlGL3M8v8mvgqngQs5Mn4ej+HU22nSCw8MRM8kes7REjxfXeJNxtM9SGss4gX2lrr5VgmCej/4+o06OtblScDXYk+zufc5KLSi1KTo508F46Qf8LpibTUBRB5nkSDAfXCxBVoKFHv3S6LX3cg7nS/LTnLtVl9002t5I4gUbeFQFaQQVvAruF+wY+u9287IuvxaU873FF+wajvrjZ8NOa0FtOpiJfx1+ylnE26r8VsOOGk9jO6BtWVPVzwVryfLXgq265D+AxWKguKMWWQAAAABJRU5ErkJggg==",
            IM_viewIn: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABApAAAQKQH1eOIPAAAAB3RJTUUH4QofDjIGXYvhoQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAABhElEQVQ4y62UsUtbURTGf+fmlAQK4pg6NG8Q0q2bTh0ES6kIFhrcBFdXbXH0rf4FdRQcImIxgohT0alztwpSnqWgYxyqSN87p4MiycsLDTYX7nK/w3fvd873XRjSkn7A2FarXik/vfwx//oKINree+Mm0yIogMENeGKl8OXX/NxZIVGtuRc50gzCd1TjpDF7XtveXxH3D511DiM4X80tLiSKdlp1Mo4RqYJvohoHK7eN39XOOrPwStzWwE90APWLpCmmxEnj3WkOPK019yeAKX2+9flZKJdGuuBMXoD3kEW7B3HSmD3PdbktoCpPSp/ImOy+yLmTxb/JzCAIQeAlItWe3V9mHO0e1PJIeIRjFi1NV8cPD8v/SVS8HkHkm0F1/Wxm5rbzVB2+iXulV0FRn+481TM5QP1PtiRF43ff6CbrQxIC4OjPhfcXwEXO2ZAN9hKcUYdUB+kJqvGVXbejnVY9HxHc3vaNiGd+60gS8KOH0DZby8DHblPbfWh9Y2jfyLBsxF+yrrcLVUKk2QAAAABJRU5ErkJggg==",
            IM_viewOut: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA/UAAAP1AF7in9SAAAAB3RJTUUH4QofDjEKfxD+SQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAABl0lEQVQ4y62UMWuTURSGn/fmtoi2UxVEh8RBWkdRcJOCuCc4xaF0EHGwu+gQQvEHuImI6GJAoQluTopD/Qda6WCCIHSwUMWi9ct9HUJimsSi8B24cOE95+G995x7IacQwIkna3OFac5JcXZYdEpvP10tfwaYe9iaBfhyrfxtEiiefNq6EKXb4Es47Q5zCLoJPKdWi0cOcwvp+EyjudqpVtpjoIJ0HXFe5k6msK6U3Bezr3rX2y0i7cxLXAGFYqNZH4VF4YvYzXa1cu8fb2NZglFYlIjJ6gzyag6n5l8cGy7d625PoUI8CBYBAuz1U4pnWjdsrQyDpmIBTKnXmsmwOGbcOo208L/HjDlM0LJRCnkNZA6O/Fh4dQxkeVNmY0JFCenQKMR2vVOttANAgum+1Hlfvi9rcXj9yrqXLV7+DQIQbTLh4kCvK32ErX01tVexuLCT/Wn/fkgPhN5IqpQarc1MYT2EkPrizx/bG1tLS98PcjIAde0HER013C047dIdcDwdZ1aAZ/Aa++wHwyPhiY82t28kt/gNvarAws03VtkAAAAASUVORK5CYII="
        },
        okl: function (i) {
            var t = plugin.ingressmosaik;
            if (null != t.znd && plugin.ingressmosaik.iiv(i, "load")) {
                var s = $('tr[data-mission_mid="' + i + '"]');
                s && s.find('td span img[name="im-check"]').addClass("load"), $('img[name="im-akt-load"]').addClass("load_a")
            }
            null !== plugin.missions.getMissionCache(i) && (delete plugin.missions.cacheByMissionGuid[i], delete t.ppo[i]), plugin.missions.loadMission(i, function (i) {
                plugin.missions.showMissionDialog(i);
                var t = $("#dialog-plugin-mission-details-" + i.guid.replace(".", "_")).dialog(), s = t.dialog("option", "buttons");
                $.extend(s, {
                    " open on IM ": function () {
                        open("//ingressmosaik.com/mission/" + i.guid)
                    }
                }), t.dialog("option", "buttons", s), $("div .ui-dialog-buttonset").find('button span:contains("open on IM")').parent().css({float: "left", "margin-right": "2px"})
            })
        },
        bhn: function () {
            var i = plugin.ingressmosaik;
            plugin.ingressmosaik.znd = dialog({
                id: "im-lite-box", title: "IM Short View", html: i.ama(i.vds), height: "auto", width: "229px", dragStop: function () {
                    i.mju("shortViewPos", [$(this).parent().css("left"), $(this).parent().css("top")])
                }, close: function () {
                    i.znd = null
                }, create: function (i, t) {
                    var s = plugin.ingressmosaik;
                    $(this).prev().prepend(s.oen(s.content.viewToggle, {img: s.nhz.IM_viewOut, func: "plugin.ingressmosaik.mju('shortView', false);plugin.ingressmosaik.znd.dialog('close');plugin.ingressmosaik.znd = null;plugin.missions.showMissionListDialog(plugin.ingressmosaik.vds);"})), $(this).prev().prepend(s.content.loadCircle)
                }
            }).dialog("option", "buttons", {}), i.znd.parent().css({left: i.oxh.shortViewPos[0], top: i.oxh.shortViewPos[1]})
        },
        swa: function () {
            var i = plugin.ingressmosaik;
            dialog({id: "im-setup-dialog", title: "IM | Setup", html: i.oen(i.content.setupDialog, {img1: plugin.ingressmosaik.oxh.autoR ? i.nhz.IM_Checket : i.nhz.IM_Check, img2: plugin.ingressmosaik.oxh.autoU ? i.nhz.IM_Checket : i.nhz.IM_Check, img3: plugin.ingressmosaik.oxh.autoInBound ? i.nhz.IM_Checket : i.nhz.IM_Check, disabled: i.cuw ? " disabled-button" : ""}), height: "auto", width: "229px"}).css({margin: "5px", padding: "0"})
        },
        cuw: function () {
            var i = plugin.ingressmosaik, t = Math.ceil(Date.now() / 1e3);
            if (i.oxh.requestCount > i.pwn - 1 && i.oxh.startRequest + i.xiw > t) {
                if (i.oxh.lastRequest + Math.ceil(i.xiw / 2) < t)return i.mju("startRequest", !1), i.mju("requestCount", 0), i.mju("lastRequest", t), i.mju("security", !0), !1;
                var s = new Date(1e3 * plugin.ingressmosaik.oxh.startRequest), e = s.getHours(), n = "0" + s.getMinutes(), o = "0" + s.getSeconds(), a = e + ":" + n.substr(-2) + ":" + o.substr(-2);
                e = (s = new Date(1e3 * (plugin.ingressmosaik.oxh.lastRequest + Math.ceil(i.xiw / 2)))).getHours(), n = "0" + s.getMinutes(), o = "0" + s.getSeconds();
                var r = e + ":" + n.substr(-2) + ":" + o.substr(-2);
                return i.oxh.cuw ? (i.mju("autoInBound", !1), i.mju("autoR", !1), i.mju("autoU", !1), dialog({id: "thisshitneedsanidsotheresnoduplicatesooohman", title: "IM | Safety information", html: "Security Look is acive!<br>Please try again at " + r + "h"}), $('img[name="im-akt-load"]').removeClass("load_a")) : (i.mju("autoInBound", !1), i.mju("autoR", !1), i.mju("autoU", !1), dialog({
                    title: "IM | Safety information",
                    html: "<span>You've executed " + i.oxh.requestCount + " requests since " + a + "h.<br>Each request is an additional burden on the Niantic servers.<br>To protect the servers of Niantic and your account, this lock has been installed.<br>From " + r + 'h you can update or submit missions again.<br>Thank you for supporting IngressMosaik so actively.<br>However, you can use the missions plugin as usual.</span><br><br><span style="color:red">You can get around the lockdown. But please don\'t exaggerate with the requests.<br>IM accepts no liability</span>',
                    height: "auto",
                    width: "auto",
                    buttons: [{
                        text: " Unlocking the lock ", css: {float: "left", "margin-right": "2px"}, click: function () {
                            i.mju("startRequest", 0), i.mju("requestCount", 0), i.mju("lastRequest", t), $(this).dialog("close"), alert("The blockage has been removed!")
                        }
                    }, {
                        text: " OK ", click: function () {
                            $(this).dialog("close"), i.mju("security", !0)
                        }
                    }]
                })), !0
            }
            return i.oxh.startRequest + i.xiw < t && (i.mju("startRequest", 0), i.mju("requestCount", Math.ceil(i.oxh.requestCount / 4))), i.oxh.startRequest || i.mju("startRequest", t), i.mju("requestCount", ++i.oxh.requestCount), i.mju("lastRequest", t), !1
        },
        mcu: function (i) {
            var t = plugin.ingressmosaik, s = $(i).parent().parent().data("mission_mid") || i;
            if (t.iiv(s, "load")) {
                if ($(i).find("img").addClass("load"), $('img[name="im-akt-load"]').addClass("load_a"), plugin.missions.cacheByMissionGuid[s] && plugin.missions.cacheByMissionGuid[s].time > Date.now() - plugin.missions.missionCacheTime)return void t.xum(s, "update", !0);
                if (t.cuw())return $(i).find("img").removeClass("load"), !0;
                plugin.missions.loadMission(s, function (i) {
                    if ("dialog-m-im-dialog" == t.znd.attr("id")) {
                        var s = $('tr[data-mission_mid="' + i.guid + '"] td'), e = t.lxd(i);
                        s.eq(2).html(e.missionOwner), s.eq(3).html(e.infoLength), s.eq(6).html(e.infoPlayers), s.eq(7).html(e.infoWaypoints)
                    }
                    "dialog-im-lite-box" == t.znd.attr("id") && t.znd.html(t.ama(!1))
                }, function (s) {
                    "dialog-m-im-dialog" == t.znd.attr("id") && ($(i).find("img").removeClass("load"), $(i).find("img").attr("src", t.nhz.IM_Refresh_error)), console.error("Error loading missions in bounds", s)
                })
            }
        },
        oen: function (i, t) {
            for (var s in t) {
                var e = new RegExp("{" + s + "}", "g");
                i = i.replace(e, t[s])
            }
            return i
        },
        lxd: function (i) {
            var t = plugin.ingressmosaik, s = plugin.missions.getMissionCache(i.guid), e = s.waypoints.filter(function (i) {
                return !!i.portal
            }).map(function (i) {
                return L.latLng(i.portal.latE6 / 1e6, i.portal.lngE6 / 1e6)
            }).map(function (i, t, s) {
                if (0 === t)return 0;
                var e = s[t - 1];
                return i.distanceTo(e)
            }).reduce(function (i, t) {
                return i + t
            }, 0);
            return e > 0 && (e = e > 1e3 ? Math.round(e / 100) / 10 + "km" : Math.round(10 * e) / 10 + "m"), {missionOwner: t.oen(t.content.missionOwner, {css: "R" === s.authorTeam ? "res" : "enl", name: s.authorNickname}), infoLength: t.oen(t.content.missionLength, {len: e}), infoPlayers: t.oen(t.content.missionCompleted, {completed: s.numUniqueCompletedPlayers}), infoWaypoints: t.oen(t.content.missionWaypoints, {missionType: s.type ? s.type + " mission" : "Unknown mission type", count: s.waypoints.length})}
        },
        ama: function (i) {
            var t = plugin.ingressmosaik, s = [0, 0, 0, 0];
            return i || (i = t.vds), $(i).each(function (i, e) {
                var n = !1;
                if ($(t.ovg).each(function (i, t) {
                    t.guid != e.guid || (n = !0)
                }), !n)switch (t.iiv(e.guid, "status")) {
                    case 100:
                        s[1]++;
                        break;
                    case 200:
                        s[0]++
                }
                s[2]++
            }), s[3] = s[2] - s[1], t.oen(t.content.shortTable, {img: t.oxh.autoU && t.oxh.autoInBound ? t.nhz.IM_Checket : t.nhz.IM_Check, canRef: s[0], newM: s[1], inBound: s[2], onIm: s[3]})
        },
        tzu: function (i, t) {
            localStorage[i] = JSON.stringify(t)
        },
        mju: function (i, t) {
            plugin.ingressmosaik.oxh[i] = t, plugin.ingressmosaik.tzu(plugin.ingressmosaik.oro, plugin.ingressmosaik.oxh)
        },
        xsw: function (i) {
            var t = plugin.ingressmosaik;
            if (t.oxh.autoR || t.oxh.autoU)for (var s = 0; s < t.vds.length; s++) {
                var e = !1, n = t.iiv(t.vds[s].guid, "status");
                if (t.oxh.autoR && 200 == n && (e = !0), t.oxh.autoU && 100 == n && (e = !0), e) {
                    if (t.cuw())return !0;
                    plugin.missions.loadMission(t.vds[s].guid, function (i) {
                        var t = $("#dialog-m-im-dialog").find('tr[data-mission_mid="' + i.guid + '"] td'), s = plugin.ingressmosaik.lxd(i);
                        t.eq(2).html(s.missionOwner), t.eq(3).html(s.infoLength), t.eq(6).html(s.infoPlayers), t.eq(7).html(s.infoWaypoints)
                    }, function () {
                    })
                }
            }
        },
        wow: function () {
            for (var i = plugin.ingressmosaik, t = 0; t < i.vds.length; t++) {
                var s = i.vds[t].guid;
                i.mcu(s)
            }
        },
        auh: function (i) {
            $.ajax({
                dataType: "json", contentType: "application/json", type: "POST", data: JSON.stringify(i), crossDomain: !0, url: "https://ingressmosaik.com/api/addMissions.php", success: function (i) {
                    if (i[0])switch (i[1][0]) {
                        case"update":
                        case"insert":
                            plugin.ingressmosaik.xum(i[1][1], "update", !0);
                            break;
                        case"inBound":
                            window.plugin.ingressmosaik.cde(i[1][1])
                    } else switch (i[1][0]) {
                        case"update":
                            plugin.ingressmosaik.xum(i[1][1], "update", !1);
                            break;
                        case"insert":
                            plugin.ingressmosaik.xum(i[1][1], "insert", !1)
                    }
                    i[1][3] || plugin.ingressmosaik.difVersion(i[1][4])
                }, error: function (i) {
                    console.warn(i)
                }
            })
        },
        sed: function (i) {
            var t = parseInt(i / 86400) + "d " + new Date(i % 86400 * 1e3).toUTCString().replace(/.*(\d{2}):(\d{2}):(\d{2}).*/, "$1h $2m $3s");
            return (t = (t = (t = t.replace("0d", "")).replace("00h", "")).replace("00m", "")).trim()
        },
        vor: function (i) {
            var t = i.parent().parent().data("mission_mid"), s = [], e = !0, n = $('tr[data-mission_mid="' + t + '"]');
            $(plugin.ingressmosaik.ovg).each(function (i, n) {
                n.guid == t ? e = !1 : s.push(n)
            });
            var o = plugin.ingressmosaik.nhz.IM_No_Fav, a = "#im-m-l";
            e && (o = plugin.ingressmosaik.nhz.IM_Fav_o, a = "#im-m-f-l", $(plugin.ingressmosaik.vds).each(function (i, e) {
                if (e.guid == t)return e.imCheck = plugin.ingressmosaik.iiv(t), s.push(e), !0
            })), n.find("td:eq(8) span:eq(1) img").attr("src", o), $(a).find("tbody").append(n.clone()), n.remove(), $("#im-m-f-l").prev().find("span").html(s.length + " Missions"), plugin.ingressmosaik.ovg = s, plugin.ingressmosaik.tzu(plugin.ingressmosaik.fre, s)
        },
        qov: function (i) {
            var t = i.parent().parent().data("mission_mid");
            plugin.missions.toggleMission(t), i.find("img").attr("src") == plugin.ingressmosaik.nhz.IM_Check ? i.find("img").attr("src", plugin.ingressmosaik.nhz.IM_Checket_o) : i.find("img").attr("src", plugin.ingressmosaik.nhz.IM_Check)
        },
        ycx: function () {
            $('#toolbox a[onclick="plugin.missions.openTopMissions();"]').html('<span style="color:rgba(0,197,255,0.5)">I</span><span style="color:rgba(3,254,3,0.5)">M</span>issions in view');
            var i = plugin.ingressmosaik;
            i.ovg = i.ggi(i.fre) || [], i.ppo = i.ggi(i.mki) || {}, i.oxh = i.ggi(i.oro) || {autoInBound: !1, autoR: !1, autoU: !1, favHiden: !0, shortViewPos: ["102px", "217px"], shortView: !1, requestCount: 0, lastRequest: 0, startRequest: !1, security: !1, v: 0};
            var t = Date.now() / 1e3;
            for (guid in i.ppo)t - ob.timestamp > i.dno && (delete i.ppo[guid], delete plugin.missions.cacheByMissionGuid[guid]);
            plugin.missions.storeCache(), i.tzu(i.mki, i.ppo), window.map.on("moveend", function () {
                plugin.ingressmosaik.oxh.autoInBound && null != plugin.ingressmosaik.znd && plugin.missions.openTopMissions()
            }), (i.oxh.shortView || i.oxh.autoInBound) && i.bhn(), i.oxh.v != i.v && (i.mju("v", i.v), i.asd())
        }
    }, window.plugin.ingressmosaik.content = {
        table: '<div style="padding: 5px;" onclick="$(this).next().toggle();"><span style="float:right">{count} Missions</span></div><table id="im-m-l" class="sortable im-mission-table"><thead><tr><th class="sorttable_nosort">img</th><th style="cursor:pointer;">name</th><th style="cursor:pointer;">owner</th><th style="cursor:pointer;"><img src="' + plugin.ingressmosaik.nhz.a + '" /></th><th style="cursor:pointer;width: 60px;"><img src="' + plugin.ingressmosaik.nhz.b + '" /></th><th style="cursor:pointer;"><img src="' + plugin.ingressmosaik.nhz.c + '" /></th><th style="cursor:pointer;"><img src="' + plugin.ingressmosaik.nhz.d + '" /></th><th style="cursor:pointer;">WP</th><th class="sorttable_nosort" style="width: 80px;"></th></tr></thead><tbody>{body}</tbody></table></div>',
        tableFav: '<div style="background-color: rgba(255,153,0,0.5);padding: 5px;cursor:pointer;" onclick="$(this).next().toggle();plugin.ingressmosaik.vot = !plugin.ingressmosaik.vot">Favorits - click Toggle<span style="float:right">{count} Missions</span></div><table id="im-m-f-l" style="display:' + (plugin.ingressmosaik.vot ? "table" : "none") + ';" class="sortable im-mission-table"><thead><tr><th class="sorttable_nosort">img</th><th style="cursor:pointer;">name</th><th style="cursor:pointer;">owner</th><th style="cursor:pointer;"><img src="' + plugin.ingressmosaik.nhz.a + '" /></th><th style="cursor:pointer;width: 60px;"><img src="' + plugin.ingressmosaik.nhz.b + '" /></th><th style="cursor:pointer;"><img src="' + plugin.ingressmosaik.nhz.c + '" /></th><th style="cursor:pointer;"><img src="' + plugin.ingressmosaik.nhz.d + '" /></th><th style="cursor:pointer;">WP</th><th class="sorttable_nosort" style="width: 80px;"></th></tr></thead><tbody>{body}</tbody></table><br>',
        tableBody: '<tr class="plugin-mission-summary" data-mission_mid="{guid}"><td>{img}</td><td>{title}</td><td>{owner}</td><td style="text-align: right;">{distance}</td><td style="text-align: right;">{time}</td><td style="text-align: right;">{rating}</td><td style="text-align: right;">{playerFinish}</td><td style="text-align: right;">{count}</td><td style="vertical-align: middle;text-align: right;"><span title="Send to IngressMosaik\nNext Refresh you can send {statusTitle}" onclick="plugin.ingressmosaik.mcu(this)" style="margin-right: 10px; cursor: pointer"><img style="height: 15px;" name="im-check" src="{status}" /></span><span title="send to favorites" onclick="plugin.ingressmosaik.vor($(this))" style="margin-right: 10px; cursor: pointer"><img class="crossRotate" style="height: 15px;" src="{fav}" /></span><span title="Make it as absolved" onclick="plugin.ingressmosaik.qov($(this))" style="margin-right: 10px; cursor: pointer"><img style="height: 15px;" src="{checked}" /></span></td></tr>',
        missionImg: '<img src="{img}=s{size}" onclick="plugin.missions.toggleMission(\'{guid}\');" />',
        missionTitle: '<a href="/mission/{guid}" style="cursor:pointer;" onclick="plugin.ingressmosaik.okl(\'{guid}\');return false">{title}</a>',
        missionOwner: '<span class="nickname {css}">{name}</span>',
        missionLength: '<span title="Length of this mission.\n\nNOTE: The actual distance required to cover may vary depending on several factors!">{len}</span>',
        missionTime: '<span title="Typical duration">{time}</span>',
        missionRating: '<span title="Average rating">{rating}%</span>',
        missionCompleted: '<span title="Unique players who have completed this mission">{completed}</span>',
        missionWaypoints: '<span title="{missionType} with {count} waypoints">{count}</span>',
        missionDescription: '<p class="description">{desc}</p>',
        missionView: '<div><table class="im-mission-table"><tbody><tr><td style="width:{size}px;" rowspan="4">{img}</td><td></td><td style="font-weight: bold;font-size: 1.3em;color: #FFCE00;" colspan="2">{title}</td></tr><tr><td></td><td>{owner}</td><td><img src="' + plugin.ingressmosaik.nhz.a + '" /> <span style="float: right;margin-right: 20px;">{len} </span></td></tr><tr><td></td><td><img src="' + plugin.ingressmosaik.nhz.b + '" /> <span style="float: right;margin-right: 20px;">{time} </span></td><td><img src="' + plugin.ingressmosaik.nhz.c + '" /> <span style="float: right;margin-right: 20px;">{rating} </span></td></tr><tr><td></td><td><img src="' + plugin.ingressmosaik.nhz.d + '" /> <span style="float: right;margin-right: 20px;">{completed} </span></td><td><img src="{orderImg}" /> <span style="float: right;margin-right: 20px;">{count} </span></td></tr><tr><td colspan="4">{description}</td></tr></tbody></table><ol>{list}</ol></div>',
        loadCircle: '<span style="position: absolute;left: 3px;top: 3px;"><img name="im-akt-load" src="' + plugin.ingressmosaik.nhz.IM_Refresh_a + '" style="height: 15px;"></span>',
        viewToggle: '<span onclick="{func}" style="position: absolute;right: 46px;border: 1px solid rgb(32, 168, 177);height: 15px;top: 3px;width: 15px;cursor:pointer;"><img src="{img}" style="height: 13px;margin: 1px;"></span>',
        shortTable: '<div style="padding: 0 2px;text-align: center;"><table style="width: 100%;border-collapse: collapse;"><tbody><tr><td title="Missions in range" style="background-color: rgba(0,0,0,0.2);color: orange;">in range</td><td title="is already known on IM" style="background-color: rgba(0,0,0,0.4);color: orange;">known</td><td title="Missions not on IngressMosaik" style="background-color: rgba(0,0,0,0.2);color: orange;">new</td><td title="Missions you can refresh" style="background-color: rgba(0,0,0,0.4);color: orange;">to Ref</td></tr><tr><td style="text-align: center;width: 25%;background-color: rgba(0,0,0,0.2);" name="inBound">{inBound}</td><td style="text-align: center;width: 25%;background-color: rgba(0,0,0,0.4);" name="onIm">{onIm}</td><td style="text-align: center;width: 25%;background-color: rgba(0,0,0,0.2);" name="new">{newM}</td><td style="text-align: center;width: 25%;background-color: rgba(0,0,0,0.4);" name="canRef">{canRef}</td></tr></tbody></table></div><div class="ui-dialog-buttonset" style="padding: 5px 5px 0px 5px;"><button onclick="plugin.ingressmosaik.wow();" name="im-refresh" style="width: 100%" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">Refresh and Upload all</span></button></div><div class="ui-dialog-buttonset" style="padding: 5px;"><button name="im-auto-load" style="width: 100%" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text"><img style="vertical-align: middle;padding-right: 5px;" src="{img}" /> Auto Upload all new Missions</span></button></div><script>$("button[name=im-auto-load]").click(function(){  var a = (plugin.ingressmosaik.oxh.autoU && plugin.ingressmosaik.oxh.autoInBound ? false : true ); plugin.ingressmosaik.mju("autoInBound", a); plugin.ingressmosaik.mju("autoR", false); plugin.ingressmosaik.mju("autoU", a); $(this).find("img").attr("src",(a ? plugin.ingressmosaik.nhz.IM_Checket : plugin.ingressmosaik.nhz.IM_Check )); })<\/script>',
        setupDialog: '<div class="ui-dialog-buttonset" style="padding: 5px;"><button name="im-auto-refresh" style="width: 100%" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><img style="vertical-align: middle;padding-left: 5px;float:left;" src="{img1}" /><span class="ui-button-text"> Auto Refresh all Missions</span></button></div><div class="ui-dialog-buttonset" style="padding: 5px;"><button name="im-auto-upload" style="width: 100%" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><img style="vertical-align: middle;padding-left: 5px;float:left;" src="{img2}" /><span class="ui-button-text"> Auto Upload all new Missions</span></button></div><div class="ui-dialog-buttonset" style="padding: 5px;"><button name="im-auto-bound" style="width: 100%" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><img style="vertical-align: middle;padding-left: 5px;float:left;" src="{img3}" /><span class="ui-button-text"> Auto refresh mission list</span></button></div><div class="ui-dialog-buttonset" style="padding: 5px 5px 0px 5px;"><button name="im-unlook" style="width: 100%" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only{disabled}" role="button"><span class="ui-button-text">Unlook security lock</span></button></div><script>var ch=plugin.ingressmosaik.nhz.IM_Check,cht=plugin.ingressmosaik.nhz.IM_Checket;$("button[name=im-auto-refresh]").click(function(){plugin.ingressmosaik.mju("autoR",!plugin.ingressmosaik.oxh.autoR),$(this).find("img").attr("src",plugin.ingressmosaik.oxh.autoR?cht:ch)}),$("button[name=im-auto-upload]").click(function(){plugin.ingressmosaik.mju("autoU",!plugin.ingressmosaik.oxh.autoU),$(this).find("img").attr("src",plugin.ingressmosaik.oxh.autoU?cht:ch)}),$("button[name=im-auto-bound]").click(function(){plugin.ingressmosaik.mju("autoInBound",!plugin.ingressmosaik.oxh.autoInBound),$(this).find("img").attr("src",plugin.ingressmosaik.oxh.autoInBound?cht:ch)}),$("button[name=im-unlook]").click(function(){if($(this).hasClass("disabled-button"))return!0;plugin.ingressmosaik.mju("startRequest",0),plugin.ingressmosaik.mju("requestCount",0),plugin.ingressmosaik.mju("lastRequest",0),alert("The blockage has been removed!")});<\/script>'
    };
    var t, s = window.plugin.ingressmosaik.ycx;
    plugin.missions.onIITCLoaded = function () {
        var i = location.pathname.match(/\/mission\/([0-9a-z.]+)/);
        if (i && i[1]) {
            var t = i[1];
            this.loadMission(t, function (i) {
                plugin.ingressmosaik.okl(t), this.zoomToMission(i)
            }.bind(this))
        }
        window.plugin.sync && (window.plugin.sync.registerMapForSync("missions", "checkedMissions", this.syncCallback.bind(this), this.syncInitialed.bind(this)), window.plugin.sync.registerMapForSync("missions", "checkedWaypoints", this.syncCallback.bind(this), this.syncInitialed.bind(this)))
    }, plugin.missions.openPortalMissions = function () {
        this.loadPortalMissions(window.selectedPortal, function (i) {
            i.length && (1 === i.length ? this.loadMission(i[0].guid, function (i) {
                plugin.missions.showMissionDialog(i);
                var t = $("#dialog-plugin-mission-details-" + i.guid.replace(".", "_")).dialog(), s = t.dialog("option", "buttons");
                $.extend(s, {
                    " open on IM ": function () {
                        open("//ingressmosaik.com/mission/" + i.guid)
                    }
                }), t.dialog("option", "buttons", s), $("div .ui-dialog-buttonset").find('button span:contains("open on IM")').parent().css({float: "left", "margin-right": "2px"})
            }) : this.showMissionListDialog(i))
        }.bind(this))
    }, plugin.missions.openTopMissions = function (i) {
        $('img[name="im-akt-load"]').addClass("load_a"), i = i || window.map.getBounds(), this.loadMissionsInBounds(i, this.showMissionListDialog.bind(this))
    }, plugin.missions.showMissionListDialog = function (i) {
        var t = plugin.ingressmosaik;
        t.vds = i, null != t.znd ? (null != t.znd && "dialog-m-im-dialog" == t.znd.attr("id") && (t.znd.html(this.renderMissionList(i)), t.nju()), null != t.znd && "dialog-im-lite-box" == t.znd.attr("id") && t.znd.html(t.ama(i))) : t.oxh.shortView ? t.bhn() : (t.znd = dialog({
            id: "m-im-dialog", title: "Missions with IM support | thank's for your help!", html: this.renderMissionList(i), height: "auto", width: "auto", collapseCallback: this.collapseFix, expandCallback: this.collapseFix, close: function () {
                t.znd = null
            }, buttons: [{
                text: " SETUP ", css: {float: "left", "margin-right": "2px"}, click: function () {
                    t.swa()
                }
            }, {
                text: " Refresh/Upload all ", css: {float: "left", "margin-right": "2px"}, click: function () {
                    t.wow()
                }
            }, {
                text: " Go to IM ", css: {float: "left", "margin-right": "2px"}, click: function () {
                    open("//ingressmosaik.com")
                }
            }, {
                css: {"margin-right": "2px"}, text: " Create new mission ", click: function () {
                    open("//mission-author-dot-betaspike.appspot.com")
                }
            }, {
                text: " OK ", click: function () {
                    $(this).dialog("close")
                }
            }], create: function (i, t) {
                var s = plugin.ingressmosaik;
                $(this).prev().prepend(s.oen(s.content.viewToggle, {img: s.nhz.IM_viewIn, func: "plugin.ingressmosaik.mju('shortView', true);plugin.ingressmosaik.znd.dialog('close');plugin.ingressmosaik.znd = null;plugin.ingressmosaik.bhn();"})), $(this).parent().find('button span:contains("SETUP")').html('<img src="' + s.nhz.IM_Setup + '" />').parent().css({padding: "0", "min-width": "30px", height: "21px"}), $(this).prev().prepend(s.content.loadCircle)
            }
        }).css("min-width", "400px"), t.nju())
    }, plugin.missions.renderMissionList = function (i) {
        var t = plugin.ingressmosaik, s = "", e = "", n = 1, o = 1;
        return i.forEach(function (i) {
            var e = !1;
            $(t.ovg).each(function (t, s) {
                s.guid != i.guid || (e = !0)
            }), e || (s += plugin.missions.renderMissionSummary(i, o++, !1))
        }), $(t.ovg).each(function (i, t) {
            e += plugin.missions.renderMissionSummary(t, n++, !0)
        }), t.oen(t.content.tableFav, {body: e, count: t.ovg.length}) + t.oen(t.content.table, {count: i.length, body: s})
    }, plugin.missions.renderMissionSummary = function (i, t, s) {
        var e = plugin.ingressmosaik, n = this.getMissionCache(i.guid), o = (this.checkedMissions[i.guid], e.oen(e.content.missionImg, {img: i.image, size: 30, guid: i.guid})), a = e.oen(e.content.missionTitle, {title: e.cdr(i.title), guid: i.guid}), r = e.oen(e.content.missionTime, {time: plugin.ingressmosaik.sed(i.medianCompletionTimeMs / 1e3 | 0)}), l = e.oen(e.content.missionRating, {rating: (i.ratingE6 / 100 | 0) / 100}), g = {missionOwner: "", infoLength: "?", infoPlayers: "", infoWaypoints: ""};
        if (n) g = plugin.ingressmosaik.lxd(i);
        return e.oen(e.content.tableBody, {guid: i.guid, nr: t || "", img: o, title: a, owner: g.missionOwner, distance: g.infoLength, time: r, rating: l, playerFinish: g.infoPlayers, count: g.infoWaypoints, status: plugin.ingressmosaik.iiv(i.guid, "img"), statusTitle: plugin.ingressmosaik.iiv(i.guid, "time"), fav: s ? plugin.ingressmosaik.nhz.IM_Fav_o : plugin.ingressmosaik.nhz.IM_No_Fav, checked: this.checkedMissions[i.guid] ? plugin.ingressmosaik.nhz.IM_Checket_o : plugin.ingressmosaik.nhz.IM_Check})
    }, plugin.missions.renderMission = function (i) {
        var t = plugin.ingressmosaik, s = this.getMissionCache(i.guid);
        t.ppo[i.guid] && $('img[name="im-akt-load"]').removeClass("load_a").parent().parent().animate({backgroundColor: "rgb( 00, 102, 00 )"}).animate({backgroundColor: "rgba( 00, 102, 00, 0.0 )"});
        var e = t.oen(t.content.missionImg, {img: i.image, size: 100, guid: i.guid}), n = t.oen(t.content.missionTitle, {title: t.cdr(i.title), guid: i.guid}), o = t.oen(t.content.missionTime, {time: plugin.ingressmosaik.sed(i.medianCompletionTimeMs / 1e3 | 0)}), a = t.oen(t.content.missionRating, {rating: (i.ratingE6 / 100 | 0) / 100}), r = t.oen(t.content.missionDescription, {desc: i.description}), l = "";
        i.waypoints.forEach(function (t, s) {
            var e = this.renderMissionWaypoint(t, s, i).outerHTML;
            if (t.portal) {
                var n = t.portal.latE6 / 1e6, o = t.portal.lngE6 / 1e6;
                e = e.replace("<a", '<a onclick="selectPortalByLatLng(' + n + ", " + o + ');return false;" ondblclick="zoomToAndShowPortal(\'' + t.portal.guid + "', [" + n + ", " + o + ']);return false;"')
            } else e = e.replace("<a", '<a onclick="return false;"');
            l += e
        }, this);
        var g = $('tr[data-mission_mid="' + i.guid + '"] td'), u = plugin.ingressmosaik.lxd(i);
        return g.eq(2).html(u.missionOwner), g.eq(3).html(u.infoLength), g.eq(6).html(u.infoPlayers), g.eq(7).html(u.infoWaypoints), t.oen(t.content.missionView, {img: e, size: 100, title: n, owner: u.missionOwner, len: u.infoLength, time: o, rating: a, completed: u.infoPlayers, count: u.infoWaypoints, orderImg: this.missionTypeImages[s.typeNum] || this.missionTypeImages[0], list: l, description: r})
    }, t = XMLHttpRequest.prototype.open, XMLHttpRequest.prototype.open = function (i, s, e, n, o) {
        this.addEventListener("readystatechange", function (i) {
            if (4 == this.readyState) {
                if ("/r/getTopMissionsInBounds" == s) {
                    plugin.ingressmosaik.vgt = [];
                    for (var t = [], e = JSON.parse(this.responseText), n = 0; n < e.result.length; n++) {
                        var o = e.result[n][0];
                        plugin.ingressmosaik.vgt.push(o), plugin.ingressmosaik.iiv(o, "have") || t.push(o)
                    }
                    0 != t.length ? window.plugin.ingressmosaik.auh({inBound: t, v: plugin.ingressmosaik.v}) : ($('img[name="im-akt-load"]').removeClass("load_a").parent().parent().animate({backgroundColor: "rgb( 00, 102, 00 )"}).animate({backgroundColor: "rgba( 00, 102, 00, 0.0 )"}), window.plugin.ingressmosaik.cde(!1))
                }
                if ("/r/getTopMissionsForPortal" == s) {
                    for (t = [], e = JSON.parse(this.responseText), n = 0; n < e.result.length; n++)o = e.result[n][0], plugin.ingressmosaik.vgt.push(o), plugin.ingressmosaik.iiv(o, "load") && t.push(o);
                    0 != t.length ? window.plugin.ingressmosaik.auh({inBound: t, v: plugin.ingressmosaik.v}) : $('img[name="im-akt-load"]').removeClass("load_a").parent().parent().animate({backgroundColor: "rgb( 00, 102, 00 )"}).animate({backgroundColor: "rgba( 00, 102, 00, 0.0 )"})
                }
                "/r/getMissionDetails" == s && (e = JSON.parse(this.responseText), plugin.ingressmosaik.iiv(e.result[0], "load") && window.plugin.ingressmosaik.auh({detail: e.result, user: $("#name span").text(), fra: $("#name span").attr("class"), v: plugin.ingressmosaik.v}))
            }
        }, !1), t.apply(this, arguments)
    }, s.info = i, window.bootPlugins || (window.bootPlugins = []), window.bootPlugins.push(s), window.iitcLoaded && "function" == typeof s && s()
}

var timeOut = 0, int = setInterval(function () {
    if (window.plugin.missions) {
        clearInterval(int);
        var i = document.createElement("script"), t = {};
        "undefined" != typeof GM_info && GM_info && GM_info.script && (t.script = {version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description}), i.appendChild(document.createTextNode("(" + wrapper + ")(" + JSON.stringify(t) + ");")), (document.body || document.head || document.documentElement).appendChild(i);
        var s = ".im-mission-table{border-collapse:collapse;width: 100%;}.im-mission-table tbody tr{ border : none !important; background-color: rgba(0,0,0,0.2) !important; }.im-mission-table tr:nth-child(even){ background-color: rgba(0,0,0,0.4) !important; }.im-mission-table td{ vertical-align: middle; padding: 0 1px 0 3px; }.plugin-mission-summary.checked::after{ content: none !important; }.load {-webkit-animation:spin 2s linear infinite;-moz-animation:spin 2s linear infinite;animation:spin 2s linear infinite;}.load_a {-webkit-animation:spin 1s linear infinite;-moz-animation:spin 1s linear infinite;animation:spin 1s linear infinite;}@-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }@-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }@keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }.ui-dialog-titlebar{ min-width:200px !important;}#dialog-im-lite-box { padding: 0 !important; min-height: 90px !important; }.disabled-button{ color: #8C8465 !important; border: 1px solid #8C8465 !important; }",
            e = document.head || document.getElementsByTagName("head")[0], n = document.createElement("style");
        n.type = "text/css", n.styleSheet ? n.styleSheet.cssText = s : n.appendChild(document.createTextNode(s)), e.appendChild(n)
    } else timeOut++;
    timeOut > 5 && (clearInterval(int), dialog({
        title: "IngressMosaik.com",
        html: '<span>A mandatory plugin is not installed! Please install IITC “Missions” first,<br>because the “IngressMosaik” plugin extends its functionality to send marked missions to IM.</span><span style="color:darkorange"><strong><br>Please follow these steps for installation:</strong></span><ol style="margin-top: 0px;"><li>Install plugin “Missions” which you can find here <a href="https://iitc.me" target="_blank">https://iitc.me</a><br>(Desktop -> Info -> Missions)</li><li>Reload the page or restart the browser.</li></ol><span style="color:darkorange"><strong>If this does not solve the issues with the plugin, check to following:</strong></span><ul style="margin-top: 0px;"><li>If the “Missions” plugin is already installed, is it deactivated? If yes, please activate.</li><li>You mixed up the order of plugins, so that “Missions” will be loaded after “IngressMosaik”<br>was loaded? This does not work either, please ensure that “IngressMosaik”is loaded after “Missions”.</li></ul><span>If both plugins are properly installed, configured, placed in a proper order but you are still not able to see how the “Missions” dialog box offers you to send certain missions to IM, then call the folks at IngressMosaik.<br>There is a support <a href="https://plus.google.com/communities/114699326997766567901/stream/c44cc99f-25f0-473f-8f8b-6495600d26cd" target="_blank">community in G+</a>.</span>',
        height: "auto",
        width: "auto"
    }).dialog("option", "buttons", {
        " Go to IITC ": function () {
            open("//iitc.me")
        }, " OK ": function () {
            $(this).dialog("close")
        }
    }))
}, 100);
